const validator = {
    validateForm: (fieldName, formFields, formErrors) => {
        let fields = formFields;
        let errors = formErrors;
            errors["formIsValid"] = true;

        if((fieldName == 'first_name' || fieldName == null) && ("first_name" in fields)){
            if (!fields["first_name"]) {
                errors["first_name"] = "Please enter first name.";
                errors["formIsValid"] = false;
            }else{
                if (!fields["first_name"].match(/^[a-zA-Z ]*$/)) {
                    errors["first_name"] = "Please enter alphabet characters only.";
                    errors["formIsValid"] = false;
                }
                else
                    errors["first_name"] = "";
            }
        } 
        if((fieldName == 'last_name' || fieldName == null) && ("last_name" in fields)){
            if (!fields["last_name"]) {
                errors["last_name"] = "Please enter your last name.";
                errors["formIsValid"] = false;
            }else{
                if (!fields["last_name"].match(/^[a-zA-Z ]*$/)) {
                    errors["last_name"] = "Please enter alphabet characters only.";
                    errors["formIsValid"] = false;
                }
                else
                    errors["last_name"] = "";
            }
        } 

        if((fieldName == 'name' || fieldName == null) && ("name" in fields)){
            if (!fields["name"]) {
                errors["name"] = "Please enter name.";
                errors["formIsValid"] = false;
            }else{
                if (!fields["name"].match(/^[a-zA-Z ]*$/)) {
                    errors["name"] = "Please enter alphabet characters only.";
                    errors["formIsValid"] = false;
                }
                else
                    errors["name"] = "";
            }
        } 

        if((fieldName == 'email' || fieldName == null) && ("email" in fields)){
            if (!fields["email"]) {
                errors["email"] = "Please enter your email address.";
                errors["formIsValid"] = false;
             }else {
                //regular expression for email validation
              //let pattern = new RegExp(/^([a-z\d\.-]+)@([a-z\d-]+)+.ac.uk$/,);     
            let pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
              if (!pattern.test(fields["email"])) {
                    errors["email"] = "Please enter valid email address.";
                    errors["formIsValid"] = false;
                }
                else 
                    errors["email"] = "";
            }
        }
        //Validating phone
        if((fieldName == 'phome' || fieldName == null) && ('phone' in fields)){
            if(!fields['phone']){
                errors['phone'] = 'Please enter your phone number!';
                errors["formIsValid"] = false;
            }
            // else{
            //     if(fields['phone'].length !== 11){
            //         errors['phone'] = 'Please enter valid phone number!';
            //         errors["formIsValid"] = false;
            //     }else{
            //         errors['phone'] = "";
            //     }
            // }
       }
       if((fieldName == 'dateOfBirth' || fieldName == null) && ('dateOfBirth' in fields)){
        if(!fields['dateOfBirth']){
            errors['dateOfBirth'] = 'Please enter your date of birth!';
            errors["formIsValid"] = false;
        }else{
            errors['dateOfBirth'] = "";
        }
   }
        if((fieldName == 'password' || fieldName == null) && ("password" in fields)){
            if (!fields["password"]) {
                errors["password"] = "Please enter your password.";
                errors["formIsValid"] = false;
            }else{
                if(fields['password'].length < 7){
                    errors["password"] = "*Password length must be 8 characters!'";
                    errors["formIsValid"] = false;
                }else{
                    errors["password"] = "";
                }
              
            }
        }

        //Validating account_number
        if((fieldName == 'accountNumber' || fieldName == null) && ('accountNumber' in fields)){
            if(!fields['accountNumber']){
                errors['accountNumber'] = 'Please enter your account number!';
                errors["formIsValid"] = false;
            }else{
                    errors['accountNumber'] = "";
            }
       }

        //Validating card number
        if((fieldName == 'cardNumber' || fieldName == null) && ('cardNumber' in fields)){
            if(!fields['cardNumber']){
                errors['cardNumber'] = 'Please enter your Card number!';
                errors["formIsValid"] = false;
            }else{
                    errors['cardNumber'] = "";
            }
       }
  
       //Validating expMonth
        if ((fieldName == 'expMonth' || fieldName == null) && ('expMonth' in fields)) {
            if (!fields['expMonth']) {
                errors['expMonth'] = 'Please enter expiry month!';
                errors["formIsValid"] = false;
            } else {
                if(fields['expMonth'] > 12 || fields['expMonth'] <= 0){
                    errors['expMonth'] = 'Please enter valid month!';
                    errors["formIsValid"] = false;
                }else{
                    errors['expMonth'] = "";
                }
               
            }
        }

        //Validating expYear
        if ((fieldName == 'expYear' || fieldName == null) && ('expYear' in fields)) {
            if (!fields['expYear']) {
                errors['expYear'] = 'Please enter expiry year!';
                errors["formIsValid"] = false;
            } else {
                errors['expYear'] = "";
            }
        }

         //Validating cvv
         if ((fieldName == 'cvv' || fieldName == null) && ('cvv' in fields)) {
            if (!fields['cvv']) {
                errors['cvv'] = 'Please enter cvv!';
                errors["formIsValid"] = false;
            } else {
                errors['cvv'] = "";
            }
        }
        // if((fieldName == 'password' || fieldName == null) && ("password" in fields)){
        //     if (!fields["password"]) {
        //         errors["password"] = "Please enter your password.";
        //         errors["formIsValid"] = false;
        //     }else{
        //         var pattern = new RegExp("^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})");
        //         if (!pattern.test(fields["password"])) {
        //             errors["password"] = "Passwords must have minimum 8 characters and contain at least one upper case character , one lower case character, one numeric character and one special character.";
        //             errors["formIsValid"] = false;
        //         }
        //         else {
        //             if("oldPassword" in fields && "password" in fields){
        //                 if (fields["oldPassword"] == fields["password"]) {
        //                     errors["password"] = "Old password and new password should not be same.";
        //                     errors["formIsValid"] = false;
        //                 }
        //                 else
        //                     errors["password"] = "";
        //             }else{
        //                 errors["password"] = "";
        //             }
        //         }
        //     }
        // }
        // if((fieldName == 'oldPassword' || fieldName == null) && ("oldPassword" in fields)){
        //     if (!fields["oldPassword"]) {
        //         errors["oldPassword"] = "Please enter your password.";
        //         errors["formIsValid"] = false;
        //     }
        //     else
        //         errors["oldPassword"] = "";
        // }
        //Validating address
        if((fieldName == 'address' || fieldName == null) && ('address' in fields)){
            if(!fields['address']){
                errors['address'] = 'Please enter your address!';
                errors["formIsValid"] = false;
            }else{
                    errors['address'] = "";
            }
       }
  
       //Validating city
       if((fieldName == 'city' || fieldName == null) && ('city' in fields)){
        if(!fields['city']){
            errors['city'] = 'Please enter your city!';
            errors["formIsValid"] = false;
        }else{
                errors['city'] = "";
        }
   }

     //Validating state
     if((fieldName == 'state' || fieldName == null) && ('state' in fields)){
        if(!fields['state']){
            errors['state'] = 'Please enter your state!';
            errors["formIsValid"] = false;
        }else{
                errors['state'] = "";
        }
   }

   //Validating sortCode
   if((fieldName == 'sortCode' || fieldName == null) && ('sortCode' in fields)){
    if(!fields['sortCode']){
        errors['sortCode'] = 'Please enter sortCode!';
        errors["formIsValid"] = false;
    }else{
            errors['sortCode'] = "";
    }
}

   //Validating postal
   if((fieldName == 'postalCode' || fieldName == null) && ('postalCode' in fields)){
    if(!fields['postalCode']){
        errors['postalCode'] = 'Please enter postal code!';
        errors["formIsValid"] = false;
    }else{
            errors['postalCode'] = "";
    }
}

        if((fieldName == 'con_password' || fieldName == null) && ("con_password" in fields)){
            if (!fields["con_password"]) {
                errors["con_password"] = "Please confirm your password.";
                errors["formIsValid"] = false;
            }else if(fields["con_password"] !== fields["password"]){
                errors["con_password"] = "Password does not match.";
                errors["formIsValid"] = false;
            }
            else
                errors["con_password"] = "";
        }
        if((fieldName == 'verifyOTP' || fieldName == null) && ("verifyOTP" in fields)){
            if (!fields["verifyOTP"]) {
                errors["verifyOTP"] = "Please enter OTP.";
                errors["formIsValid"] = false;
            }
            else
                errors["verifyOTP"] = "";
        }
   
        return errors;
    }
}
module.exports = validator