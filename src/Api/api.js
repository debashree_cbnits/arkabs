import AsyncStorage from '@react-native-community/async-storage';
import config from './config'
let headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
}

const resolver = () => AsyncStorage.getItem('UserDetails', (err, result) => {
  if (result) {
    result = JSON.parse(result);
    // headers.Authorization = 'Bearer '+result.token
  }
})

class api {
  static post(endpoint, data) {
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.base_api + endpoint, {
          method: 'POST',
          headers: headers,
          body: JSON.stringify(data)
        }).then(response => {
          console.log("Api-response", response);
          response.json().then(function (parsedJson) {
            parsedJson.status = response.status
            resolve(parsedJson);
          }).catch(reject)
        }).catch(error => error)
      }).catch(err => err)
    })
    post
  }


  static postRide(endpoint, data, token) {
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.trip_url + endpoint, {
          method: 'POST',
          headers:
          {
            "Content-Type": "application/json",
            "Authorization": 'Bearer ' + token
          },
          body: JSON.stringify(data)
        }).then(response => {
          response.json().then(function (parsedJson) {
            parsedJson.status = response.status
            resolve(parsedJson);
          }).catch(reject)
        }).catch(error => error)
      }).catch(err => err)
    })

  }

  static POST(endpoint, data, token) {
     return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.App_url + endpoint, {
          method: 'POST',
          headers:
          {
            "Content-Type": "application/json",
            "Authorization": 'Bearer ' + token
          },
          body: JSON.stringify(data)
        }).then(response => {
         // console.log("Api-response", response);
          response.json().then(function (parsedJson) {
            parsedJson.status = response.status
            resolve(parsedJson);
          }).catch(reject)
        }).catch(error => error)
      }).catch(err => err)
    })

  }
  // ********************************************POST****************************************************** 

  static put(endpoint, data, token) {
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.base_api + endpoint, {
          method: 'PUT',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
          },
        })                
          .then(response => {            
            if (response.status === 200) {
              resolve(response.json());
            } else if (response.status === 201) {
              resolve(response.json());
            } else {              
              if (response.status === 400) {
                resolve();
              }
              if (response.status === 500) {
                resolve(response.json());
              } else {
                reject({ "err": "401 found" })
              }
            }
          }).catch(error => {
            console.log("api post catch block with error msg" + error)
          })
      }).catch(error => {
        console.log("api-catch-error", error)

      })
    })
  }

  static PUT(endpoint,token,data) {
    // console.log('url',config.App_url + endpoint)
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.App_url + endpoint, {
          method: 'PUT',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
          },
        })
          .then(response => {
            //  console.log("Api-response", response);
            if (response.status === 200) {
              resolve(response.json());
            } else {
              if (response.status === 400) {
                resolve();
              }
              if (response.status === 500) {
                resolve(response.json());
              } else {
                reject({ "err": "401 found" })
              }
            }
          }).catch(error => {
            console.log("api post catch block with error msg" + error)
          })
      }).catch(error => {
        console.log("api-catch-error", error)

      })
    })
  }

  static logOut(endpoint, token) {
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.base_api + endpoint, {
          method: 'PUT',
          // body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + token,
          },
        })
          .then(response => {
            // console.log("Api-response", response);
            if (response.status === 200) {
              resolve(response.json());
            } else {
              if (response.status === 204) {
                resolve();
              }
              if (response.status === 401) {
                resolve(response.json());
              } else {
                reject({ "err": "401 found" })
              }
            }
          }).catch(error => {
            console.log("api post catch block with error msg" + error)
          })
      }).catch(error => {
        console.log("api-catch-error", error)

      })
    })
  }


  //   static putImage ( endpoint, data) {
  //     console.log('url',config.base_api+endpoint)
  //     console.log('data',data)
  //     return new Promise((resolve, reject) => {
  //         resolver().then(() => {
  //             fetch(config.base_api+endpoint, {
  //                 method: 'PUT',
  //                 body:data,
  //                 headers: {
  //                     'Content-Type': 'multipart/form-data',
  //                     //'Accept':'application/json',
  //                     // 'Authorization': 'app.com ' + token
  //                 },
  //             })
  //                 .then(response => {

  //                     console.log("Api-response", response);

  //                     if (response.status === 200) {
  //                         resolve(response.json());
  //                     } else {
  //                         if (response.status === 400) {
  //                             resolve();
  //                         }
  //                         if (response.status === 500) {
  //                             resolve(response.json());
  //                         } else {
  //                             reject({ "err": "401 found" })
  //                         }
  //                     }
  //                 }).catch(error => {
  //                     console.log("api post catch block with error msg" + error)
  //                 })
  //         }).catch(error => {
  //             console.log("api-catch-error", error)

  //         })
  //     })
  // };

  //***************************************************************PUT*********************************************************
  static postImage(endpoint, data) {
    // console.log('url', config.base_api + endpoint)
    // console.log('data', data)
    return new Promise((resolve, reject) => {
      // resolver().then(() => {
      fetch(config.base_api + endpoint, {
        method: 'POST',
        body: data,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
        .then(response => {

          console.log("Api-response", response);

          if (response.status === 200) {
            resolve(response.json());
          } else {
            if (response.status === 400) {
              resolve(response.json());
            }
            if (response.status === 500) {
              resolve(response.json());
            } else {
              reject({ "err": "401 found" })
            }
          }
        }).catch(error => {
          console.log("api post catch block with error msg" + error)
        })
      // }).catch(error => {
      //     console.log("api-catch-error", error)

      // })
    })
  };


  static get(endpoint) {
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.base_api + endpoint, {
          method: 'GET',
          headers: headers
        }).then(response => {
          // console.log("Api-response", response);
          response.json().then(function (parsedJson) {
            parsedJson.status = response.status
            resolve(parsedJson);
          }).catch(reject)
        }).catch(reject)
      }).catch(reject)
    })
  }

  static getUserDetails(endpoint, token) {
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.base_api + endpoint, {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + token,
          },
        }).then(response => {
          response.json().then(function (parsedJson) {
            parsedJson.status = response.status
            resolve(parsedJson);
          }).catch(reject)
        }).catch(reject)
      }).catch(reject)
    })
  }

  static getRides(endpoint, token) {
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.trip_url + endpoint, {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + token,
          },
        }).then(response => {
          response.json().then(function (parsedJson) {
            parsedJson.status = response.status
            resolve(parsedJson);
          }).catch(reject)
        }).catch(reject)
      }).catch(reject)
    })
  }

  static GET(endpoint, token) {
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.App_url + endpoint, {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + token,
          },
        }).then(response => {
          response.json().then(function (parsedJson) {
            parsedJson.status = response.status
            resolve(parsedJson);
          }).catch(reject)
        }).catch(reject)
      }).catch(reject)
    })
  }

  // *******************************************************GET******************************************************


  static delete(endpoint) {
    return new Promise((resolve, reject) => {
      fetch(config.base_api + endpoint, {
        method: 'DELETE',
        headers: headers
      }).then(response => {
        response.json().then(function (parsedJson) {
          parsedJson.status = response.status
          resolve(parsedJson);
        }).catch(reject)
      }).catch(reject)
    })

  }
}



export default api
