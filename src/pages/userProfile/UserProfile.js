import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Image, Alert } from 'react-native'
import ActionSheet from 'react-native-actionsheet'
import AsyncStorage from '@react-native-community/async-storage';
import { Avatar, Icon, } from 'react-native-elements'
import styles from './style';
// import ImagePicker from 'react-native-image-picker';
import ImagePicker from "react-native-image-crop-picker";
import api from '../../Api/api';
import config from '../../Api/config';
import imageUrl from '../../Api/config';

var BUTTONS = [
    { text: 'camera', icon: "ios-camera", iconColor: "#2c8ef4" },
    { text: 'file', icon: "ios-images", iconColor: "#f42ced" }
];



export default class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userDetails: '',
            userName: '',
            fileName: '',
            userImage: '',
            profileDetails: [],
            tripRating: '',
            UniversityDetails: [],
            profilePicture: '',
            loggedUserId: '',
            token: '',
            userProfileId: this.props.navigation.state.params.userProfile._id

        }

    }

    componentDidMount() {
        AsyncStorage.getItem('UserDetails', (err, result) => {
            let data = JSON.parse(result);
            this.setState({
                token: data.token,
                loggedUserId: data.data._id
            });
            api.getUserDetails('user/' + this.state.userProfileId, this.state.token).then((result) => {
                if (result.status == 200) {
                    this.setState({
                        tripRating: result.data,
                        profileDetails: result.data.user,
                        UniversityDetails: result.data.user.universityId,
                        profile_picture: result.data.user.profilePicture,
                    });

                }

            }).catch((err) => {

            });

        });
    }

    render() {
       // console.log('prof', this.state.profileDetails)
        return (

            <View style={styles.container}>
                <View style={styles.logoDiv}>
                    <Image source={require("../../assets/imsges/logo.png")} style={styles.logo} />
                </View>
                <View style={styles.profile}>
                    <TouchableOpacity disabled={true} >
                        {this.state.profile_picture ?
                            <Avatar size="xlarge" rounded source={{ uri:  this.state.profile_picture.includes('/userImage')? imageUrl.img_url+this.state.profile_picture : this.state.profile_picture}} />
                            :
                            <Avatar size="xlarge" rounded source={require("../../assets/images/user.png")} />

                        }
                    </TouchableOpacity>
                    <Text style={styles.username}>{this.state.profileDetails.name}</Text>
                    <Text style={styles.mtext}>Rating:{this.state.tripRating.averageRating ? this.state.tripRating.averageRating : 0}</Text>
                    <Text style={styles.mtext}>Trips:{this.state.tripRating.totalTrip}</Text>
                    <TouchableOpacity style={styles.proButton} onPress={() => this.props.navigation.navigate('ChatDetails',{receiverDetails:this.state.profileDetails})} >
                        <Text style={styles.proButtonTxt}>Message</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    <View style={styles.info}>
                        <View style={styles.row}>
                            <Text style={styles.label2}>University</Text>
                            <Text>{this.state.UniversityDetails.universityName ? this.state.UniversityDetails.universityName : '.................'}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label2}>Bio</Text>
                            <Text>{this.state.profileDetails.bio ? this.state.profileDetails.bio : '.................'}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label2}>About</Text>
                            <Text>{this.state.profileDetails.aboutMe ? this.state.profileDetails.aboutMe : '.................'}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label2}>Info</Text>
                            <Text>{this.state.profileDetails.info ? this.state.profileDetails.info : '.................'}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label2}>Car Details</Text>
                            <Text>{this.state.profileDetails.model ? this.state.profileDetails.model : '.................'}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label2}>Model</Text>
                            <Text>{this.state.profileDetails.carDetails ? this.state.profileDetails.carDetails : '.................'}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label2}>Colour</Text>
                            <Text>{this.state.profileDetails.color ? this.state.profileDetails.color : '.................'}</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
