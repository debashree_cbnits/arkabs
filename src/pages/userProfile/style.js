import { Right } from "native-base";

export default {
    container: {
        paddingLeft:10,
        paddingRight:10,
        flex:1,
        backgroundColor:'#fff'
    },
    logoDiv:{      
        width:'100%',
        justifyContent:'center',
        alignItems:'center', 
    },
    logo:{
      width:180,
      height:60,
      resizeMode:'cover',
      marginTop:20
    },
    profile:{
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'column',
        paddingTop:30,
        paddingBottom:20
    },
    username: {
        fontSize:22,
        color:'#222',
        fontFamily: 'Oswald-Bold',
        paddingTop:10
    },
    menulink:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingTop:18,
        paddingBottom:18,
        borderTopWidth:1,
        borderColor:'#EEE'
    },
    linktext:{
        fontSize:18,
        fontFamily: 'Oswald-Bold',
        color:'#444'
    },
    prolist: {
        padding:15
    },
    mtext:{
        fontSize:16,
        fontFamily: 'Oswald-Bold',
        color:'#666',
        textAlign:'center'
    },
    row:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingBottom:6,
        paddingTop:6
    },
    chk:{
        backgroundColor:'transparent',
        borderWidth:0,        
    },
    label:{
        fontSize:16,
        color:'#666',
        fontFamily: 'Oswald-Regular'
    },
    label2:{
        fontSize:18,
        color:'#666',
        fontFamily: 'Oswald-Bold'
    },
    labeltext:{
        fontSize:20,
        color:'#111',
        fontFamily: 'Oswald-Bold',
        marginBottom:10
    },
    info: {
        paddingLeft:15,
        paddingRight:15
    },
    editButton: {
        backgroundColor: "#3498db",
        width: "70%",
        marginVertical: 50,
        borderRadius: 10,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        elevation: 3
    },
    editButtonTxt:{
        fontSize:18,
        color:'#3498db',
        fontFamily: 'Oswald-Bold',
        textAlign:'right'
    },
    uploadButtonTxt:{
        fontSize:18,
        color:'#fff',
        fontFamily: 'Oswald-Bold',
        textAlign:'right'
    },
    uploadButton: {
        backgroundColor: "#3498db",
        width: "100%",
        marginVertical: 50,
        borderRadius: 10,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        elevation: 3
    },
    headerBackBtn: {
        width: 40,
        justifyContent: 'center'
    },
    headerBackIcon: {
        color: '#3498db',
        fontSize: 30
    },
    button: {
        backgroundColor: "#3498db",
        width: "70%",
        marginVertical: 50,
        borderRadius: 10,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        elevation: 3
    },
    buttonTxt: {
        color: 'white',
        fontFamily: 'Oswald-Bold',
        fontSize: 24
    },
    proButton: {
        padding:10,
        backgroundColor: '#3498db',
        borderRadius: 10,
        alignItems: 'center',
        width: "40%",
        elevation: 3
    },
    proButtonTxt: {
        color: 'white',
        fontFamily: 'Oswald-Bold',
        fontSize: 15
    },
    connectWarp: { 
        width: '46%', 
        minWidth:140,
        borderWidth: 1, 
        borderColor: '#e0e0e0', 
        margin: 4, 
        flexDirection: 'row', 
        alignItems: 'center', 
        padding: 8, 
        borderRadius: 4, 
        paddingTop: 10,
        paddingBottom: 10, 
        backgroundColor: '#fff'
    },
    connectImage:{ 
        height: 50, 
        width: 50,
        marginRight:5
    },

}