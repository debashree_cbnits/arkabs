export default {
    container: {
        // paddingLeft:10,
        // paddingRight:10,
        paddingHorizontal: '4%',
        flex:1,
        backgroundColor:'#fff'
    },
    logoDiv:{      
        width:'100%',
        justifyContent:'center',
        alignItems:'center', 
        backgroundColor:'#fff'
    },
    logo:{
        // width: '60%',
        // height: 120
        width:180,
        height:80,
        resizeMode:'cover',
        marginTop:20,
        marginBottom: 15  
    },
    mainheading: {
        color:'#444',
        fontSize:24,
        // fontFamily: 'Oswald-Bold',
        fontFamily: 'Oswald-Regular',

        textAlign:'center',
        marginBootom:20
    },
    btntext:{
        color:'#fff',
        textAlign:'center',
        fontWeight:'900',
        fontSize:24,
        fontFamily: 'Oswald-Regular'
    },
    disabledBtntext:{
        color:'#fff',
        textAlign:'center',
        fontWeight:'900',
        fontSize:24,
        fontFamily: 'Oswald-Regular'
    },
    orangebtn:{       
        marginTop:10,
        paddingTop:5,
        paddingBottom:10,
        backgroundColor:'#fca600',
        borderRadius:10, 
        marginBottom:10
    },
    disabledOrangebtn:{       
        marginTop:10,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#808080',
        borderRadius:10, 
        marginBottom:10
    },
    blutext: {    
        width: '15%',   
        color:'#377de1',
        fontSize:20,
        fontFamily: 'Oswald-Bold',
        marginLeft:10
               
    },
    orangetext: {   
        width: '15%',    
        color:'#fca600',
        fontSize:20,
        fontWeight:'bold',
        marginLeft:10
               
    },
    pickerin: {
        width:"80%",
        color:'#555',
        fontSize:18,
        fontFamily: 'Oswald-Regular'
               
    },
    pickera: {
        width:"100%",
        color:'#555'
               
    },
    pickerviewcard: {
         height:60,       
        borderRadius:5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 4,
        backgroundColor:'#fff',
        marginTop:6,
        marginBottom:6,
        marginLeft:6,
        marginRight:6,
        overflow:'hidden',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
      
    },
    pickerview: {
        width:"100%",
        height:50,
        borderWidth:2,
        borderColor:'#ddd',
        borderRadius:5,
        marginTop:30      
    },
    infotext: {
        color:'#888',
        fontSize:20,
        lineHeight:28,
        fontWeight:'500',
        marginBottom:20,
        textAlign:'center',
        fontFamily: 'Oswald-Regular'
    },
    seats: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        padding:15
    },
    bprice: {
        fontSize:30,
        color:'#377de1',
        marginLeft:15,
        marginRight:15,
        fontFamily: 'Oswald-Bold'
    },
    seatHeader :{
        paddingTop:10,
        width:'100%',  
        textAlign:'center',
        backgroundColor: '#fff',
        fontFamily: 'Oswald-Regular',
        fontSize : 16,
        color : '#888'
    },
    tabBoldText : {
        borderColor:'red',
        borderWidth:1,
        width: '80%',
        height: 20,
        // padding: 10
    }
    
}