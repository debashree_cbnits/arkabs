import React, { useState, Component } from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, Picker, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { Avatar, Icon, Card } from 'react-native-elements'
import Input from "../../Components/floatingInput"
import styles from './style'
import DatePicker from "react-native-datepicker";
import RNDateTimePicker from '@react-native-community/datetimepicker';
import TimePicker from "react-native-24h-timepicker";
import { GlobalStyle } from "../../assets/StylesGlobal/GlobalStyle";
import { colors } from "../../assets/StylesGlobal/globalColors";
import api from "../../Api/api"
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import * as firebase from 'firebase';

// var firebaseConfig = {
//   apiKey: "AIzaSyBPn_p6JBWkIUSDzG0ke-0IGMNwrnfLpaY",
//   authDomain: "arkabs.firebaseapp.com",
//   databaseURL: "https://arkabs.firebaseio.com/",
//   projectId: "arkabs",
//   storageBucket: "arkabs.appspot.com",
//   messagingSenderId: "769927113864",
// }

var firebaseConfig = {
    apiKey: "AIzaSyBT6pCnWjwSOoVknx-no1HlpuSg4JRkJkQ",
    authDomain: "arkabs-280713.firebaseapp.com",
    databaseURL: "https://arkabs-280713.firebaseio.com",
    projectId: "arkabs-280713",
    storageBucket: "arkabs-280713.appspot.com",
    messagingSenderId: "1031996217292",
    appId: "1:1031996217292:web:6a5415afdc702f2a356f69",
    measurementId: "G-MMZT0R9B7E"
};

export default class DriveScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      userName:'',
      token: '',
      date: '',
      time: '',
      fromLocationLat: 0.00,
      fromLocationLong: 0.00,
      fromLocationName: '',
      fromLocation: { type: 'Point', coordinates: [] },
      toLocationLat: 0.00,
      toLocationLong: 0.00,
      toLocationName: '',
      toLocation: { type: 'Point', coordinates: [] },
      amount: 0,
      availableSeat: 0,
      totalSeat: 3,
      loader: false,
      show: false,
    };
    this.onChange = this.onChange.bind(this);

    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
      this.state.chatRef = firebase.database().ref().child('chatMessages');
    } else {
      this.state.chatRef = firebase.database().ref().child('chatMessages');
    }

  }

  componentDidMount() {
    this.props.navigation.addListener('didFocus', payload => {
      // console.log("did focus")
      this.setState({
        date: '',
        time: '',
        fromLocationLat: 0.00,
        fromLocationLong: 0.00,
        fromLocationName: '',
        fromLocation: { type: 'Point', coordinates: [] },
        toLocationLat: 0.00,
        toLocationLong: 0.00,
        toLocationName: '',
        toLocation: { type: 'Point', coordinates: [] },
        amount: 0,
        availableSeat: 0,
        totalSeat: 3,
        loader: false,
        show: false
      })
    });
    AsyncStorage.getItem('UserDetails', (err, result) => {
      let parseData = JSON.parse(result)
      this.setState({
        userId: parseData.data._id,
        token: parseData.token,
        userName:parseData.data.name
      })
    });
  }

  handleSit(text) {
    if (text > this.state.totalSeat) {
      Alert.alert('You can select only 3 seats')
      this.setState({ availableSeat: '' })
    } else {
      this.setState({ availableSeat: text })
    }

  }

  checkFormValidation = (data) => {
    if (data.toLocationName == "" || data.toLocationName == "" || data.amount == "" || data.tripDate == "" || data.tripTime == "") {
      return false;
    }
  }

  onChange = (event) => {
    let time = moment(event.nativeEvent.timestamp).format("HH:mm")
    this.setState({ time: time, show: false })
  };
  showTimepicker = (e) => {
    this.setState({ show: true })
  }

  createRide = () => {
    if (this.state.fromLocationName == "" || this.state.toLocationName == "" || this.state.date == "" || this.state.time == "") {
      Alert.alert('Please fill up all the details')
    } else {
    const nowDate = new Date().toLocaleDateString();
    let data = {
      toLocationName: this.state.toLocationName,
      toLocation: {
        type: 'Point',
        coordinates: [this.state.toLocationLong, this.state.toLocationLat]
      }, // [latitude, longitude]
      fromLocationName: this.state.fromLocationName,
      fromLocation: { type: 'Point', coordinates: [this.state.fromLocationLong, this.state.fromLocationLat] }, // [latitude, longitude]
      userId: this.state.userId,
      amount: this.state.amount,
      availableSeat: this.state.availableSeat,
      tripDate: this.state.date,
      tripTime: this.state.time
    }

    this.setState({ isDisabled: true, loader: true })
    api.postRide('trip', data, this.state.token).then((result) => {
     this.setState({ loader: false })
      if (result.status == 200) {
        this.state.chatRef.push({
          "group":true,
          "groupName": this.state.fromLocationName+"-"+this.state.toLocationName,
          "adminId": this.state.userId,
          "adminName": this.state.userName,
          "senderId": this.state.userId,
          "senderName": this.state.userName,
          //"senderImage": this.state.senderImage,
          //"Message": this.state.typeMessage,
          //"userId": this.state.userId,
          //"username": this.state.username,
          //"userImage": this.state.userImage,
          "chatRoomId": result.data.tripNumber,
          "memberId":this.state.userId,
          "memberName":this.state.userName,
          "date": nowDate,
      });
        Alert.alert(result.msg)
        this.props.navigation.navigate('Trips')
      } else {
        Alert.alert(result.msg)
      }
    }).catch((err) => {
      this.setState({ loader: false })
      console.log('error', err)
    });
    }
  }

  render() {    
    let seatArray=[1,2,3]
    return (
      <>
        <View style={styles.logoDiv}>
          <Image source={require("../../assets/imsges/logo.png")} style={styles.logo} />
        </View>

        <ScrollView style={{backgroundColor:'#fff'}} keyboardShouldPersistTaps={'always'}>
          <View style={styles.container}>
            <Spinner visible={this.state.loader}
              textContent={'Loading...'}
              textStyle={{ color: '#FFF' }} />


            <Text style={{...styles.mainheading, marginBottom: 20}}>Where Are You Driving?</Text>

            <View style={{backgroundColor:'#fff'}}>

              <View style={{backgroundColor:'#fff'}}>
                <GooglePlacesAutocomplete                  
                  placeholder={this.state.fromLocationName ? this.state.fromLocationName : 'Select your location'}
                  minLength={2} // minimum length of text to search
                  autoFocus={false}
                  returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                  listViewDisplayed={false}    // true/false/undefined
                  fetchDetails={true}
                  renderLeftButton={() => <Text style={styles.blutext}>From</Text>}
                  renderDescription={(row) => row.structured_formatting.main_text}
                  onPress={(data, details = null) => {                    
                    if (details.geometry.location) {
                      this.setState({
                        fromLocationLat: details.geometry.location.lat,
                        fromLocationLong: details.geometry.location.lng,

                      });

                    }
                    if (data) {
                      this.setState({
                        fromLocationName: data.structured_formatting.main_text
                      });
                    }
                  }}

                  getDefaultValue={() => ''}
                  query={{
                    // available options: https://developers.google.com/places/web-service/autocomplete
                    // key: 'AIzaSyCaZ7DeAIwVCOzQjPHyOs6ZRWMBB3R-F3A',
                    key: 'AIzaSyDdzB09qeDT4xMloFsz0KFVOaivyWMSZDg',
                    language: 'en', // language of the results
                    components : 'country:uk',
                    
                  }}
                  styles={{
                    textInputContainer: {
                      height: 60,
                      borderRadius: 5,
                      shadowColor: '#000',
                      shadowOffset: { width: 0, height: 2 },
                      shadowOpacity: 0.5,
                      shadowRadius: 2,
                      elevation: 4,
                      backgroundColor: '#fff',
                      marginTop: 6,
                      marginBottom: 6,
                      marginLeft: 6,
                      marginRight: 6,
                      overflow: 'hidden',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center'
                    },
                    textInput: {
                      fontSize: 15,
                      color: `${colors.textColor}`,
                      fontFamily: 'Oswald-Regular',
                      textAlign: 'center',
                      paddingRight: '20%'
                    }

                  }}
                  currentLocation={false}
                  GooglePlacesSearchQuery={{
                    // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                    rankby: 'distance',
                    types: 'food'
                  }}

                  debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                />
              </View>

              <View>
                <GooglePlacesAutocomplete
                  placeholder={this.state.toLocationName ? this.state.toLocationName : 'Select your destination'}
                  minLength={2} // minimum length of text to search
                  autoFocus={false}
                  returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                  listViewDisplayed={false}    // true/false/undefined
                  fetchDetails={true}
                  renderLeftButton={() => <Text style={styles.blutext}>To</Text>}
                  renderDescription={(row) => row.structured_formatting.main_text}
                  onPress={(data, details = null) => {
                    if (details.geometry.location) {
                      this.setState({
                        toLocationLat: details.geometry.location.lat,
                        toLocationLong: details.geometry.location.lng,

                      });

                    }
                    if (data) {
                      this.setState({
                        toLocationName: data.structured_formatting.main_text
                      });
                    }
                  }}
                  getDefaultValue={() => {
                    return ''; // text input default value
                  }}
                  query={{
                    // available options: https://developers.google.com/places/web-service/autocomplete
                    // key: 'AIzaSyCaZ7DeAIwVCOzQjPHyOs6ZRWMBB3R-F3A',
                    key: 'AIzaSyDdzB09qeDT4xMloFsz0KFVOaivyWMSZDg',
                    language: 'en', // language of the results,
                    components : 'country:uk',
                  }}
                  styles={{
                    textInputContainer: {
                      height: 60,
                      borderRadius: 5,
                      shadowColor: '#000',
                      shadowOffset: { width: 0, height: 2 },
                      shadowOpacity: 0.5,
                      shadowRadius: 2,
                      elevation: 4,
                      backgroundColor: '#fff',
                      marginTop: 6,
                      marginBottom: 6,
                      marginLeft: 6,
                      marginRight: 6,
                      overflow: 'hidden',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    },
                    textInput: {
                      fontSize: 15,
                      color: `${colors.textColor}`,
                      fontFamily: 'Oswald-Regular',
                      textAlign: 'center',
                      paddingRight: '20%'
                    }
                  }}

                  currentLocation={false}

                  GooglePlacesSearchQuery={{
                    // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                    rankby: 'distance',
                    types: 'food'
                  }}

                  filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                  debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                />
              </View>

              <DatePicker
                ref={(ref) => this.datePickerRef = ref}
                style={{ width: 0, height: 0 }}
                date={this.state.datetime}
                mode="date"
                format="YYYY-MM-DD"
                minDate={new Date()}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={false}
                hideText={true}
                onDateChange={(date) => {
                  this.setState({ date: date })
                }}
              />
              <TouchableOpacity onPress={() => this.datePickerRef.onPressDate()} style={styles.pickerviewcard}>
                <Text style={styles.blutext}>Date</Text>
                <View style={{ width: "100%", alignItems: "flex-start", borderWidth: 0, marginStart: 38, paddingHorizontal: 20 }}>
                  <Text style={{ fontSize: 20, color: `${colors.textColor}`, fontFamily: 'Oswald-Regular' }}>{this.state.date}</Text>
                </View>
              </TouchableOpacity>
              {this.state.show &&
                (<RNDateTimePicker
                  mode="time"
                  value={new Date()}
                  display="spinner"
                  is24Hour={false}
                  onChange={this.onChange} />)
              }
              <TouchableOpacity onPress={this.showTimepicker} style={styles.pickerviewcard}>
                <Text style={styles.blutext}>Time</Text>
                <View style={{ width: "100%", alignItems: "flex-start", borderWidth: 0, marginStart: 38, paddingHorizontal: 20 }}>
                  <Text style={{ fontSize: 20, color: `${colors.textColor}`, fontFamily: 'Oswald-Regular' }}>{this.state.time}</Text>
                </View>
              </TouchableOpacity>

              {/* <View style={{ width: "60%", alignSelf: "center", }}>
                <Input
                  label="Select seat"
                  name="Select seat"
                  keyboardType="number-pad"
                  maxLength={1}
                  value={this.state.availableSeat}
                  onChangeText={text => this.handleSit(text, 'phone')}
                  ref={ref => (this.customInput4 = ref)}
                  refInner="innerTextInput4"
                  onSubmitEditing={() =>
                    this.customInput5.refs.innerTextInput5.focus()
                  }
                  returnKeyType="next"
                  blurOnSubmit={false}
                />

              </View> */}
              
            <View style={{width: '80%', marginVertical: 10, alignItems: 'center', backgroundColor: '#fff', paddingBottom: 10, marginLeft: 'auto', marginRight: 'auto'}}>
              <Text style={styles.seatHeader}>How many seats are available?</Text>
              </View>

              <View style={{width: '48%', marginVertical: 6, alignItems: 'center', borderRadius: 5, elevation: 4, backgroundColor: '#fff', marginLeft: 'auto', marginRight: 'auto'}}>
              <Picker
                selectedValue={this.state.availableSeat}
                style={{width: '80%'}}
                onValueChange={(itemValue) => this.setState({availableSeat:itemValue})}
              >
                <Picker.Item label="1" value={1} />
                <Picker.Item label="2" value={2} />
                <Picker.Item label="3" value={3}/>

              </Picker>
              </View>

              {/* <View style={styles.seats}>
              <Text style={styles.bprice}>£{this.state.amount}</Text>
            </View> */}
              <Text style={styles.infotext}>Tip: Use popular locations to make it easier for riders to join you</Text>

            </View>
          </View>
        </ScrollView>
        <View style={{ paddingHorizontal: '4%', backgroundColor:'#fff' }}>
          <TouchableOpacity onPress={() => this.createRide()} style={styles.orangebtn}>
            <Text style={styles.btntext}>Post</Text>
          </TouchableOpacity>
        </View>

      </>
    );
  }
}
