import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Platform, TouchableNativeFeedback, SafeAreaView, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import Input from "../../Components/floatingInput"
import style from './style';
import { ScrollView } from 'react-native-gesture-handler';
import Validator from '../../helper/validator';
import api from '../../Api/api'
import PushNotification from "react-native-push-notification";

export class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      errorLoginMsg: '',
      alreadyLoggedIn: 'false',
      fields: { email: '', password: '' },
      errors: {},
      loading: 0,
      userType: "",
      isDisabled: false,
      deviceToken:'',
      deviceType: '',
    }
    this.handleSubmit = this.handleSubmit;
    this.validateForm = this.validateForm;
  }

  componentDidMount(){
    this.configurePushNotifications()
  }

  configurePushNotifications = () => {
    const that = this;
    PushNotification.configure({
       onRegister: function(token) {         
         that.setState({deviceToken : token.token, deviceType: token.os})         
       }
    });
 }

  // Field validation --start
  handleChange(value, name) {
    let fields = this.state.fields;
    fields[name] = value;
    this.setState({ fields });
    // this.setState({ errors: Validator.validateForm(name, this.state.fields, this.state.errors) });
  }
  // Field validation --end

  // Login function ---start
  handleSubmit = (e) => {

    e.preventDefault();
      this.setState({ isDisabled: true });
      let inputData = {
        email: this.state.fields.email,
        password: this.state.fields.password,
        deviceToken: this.state.deviceToken,
        deviceType: this.state.deviceType
      }
      // console.log('inputData----', inputData)
      api.post('userLogin', inputData).then(Result => {
        if (Result.status == 200) {
          AsyncStorage.setItem("UserDetails", JSON.stringify(Result));
          Alert.alert(Result.msg)
          this.props.navigation.navigate('Trips')
        } else {
          Alert.alert(Result.msg)
          let fields = {},
            errors = {};
          this.setState({ fields: fields, errors: errors, isDisabled: false });
        }
      }).catch(err => {
      })
    // }
  }
  //  Login function ---end

  render() {
    let TouchableCmp = TouchableOpacity;
    if (Platform.OS === 'android' && Platform.Version >= 21)
      TouchableCmp = TouchableNativeFeedback;
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
        <ScrollView>
          <View style={style.container}>
            <View style={style.subContainer1}>
              <View style={style.subContainer2}>
                <Text style={style.containerTxt}>Sign In</Text>
              </View>

              <Input
                label="Email"
                name="username"
                keyboardType="email-address"
                autoCapitalize="none"
                value={this.state.fields.email}
                onChangeText={text => this.handleChange(text, 'email')}
                errorMessage={this.state.errors.email}
                onSubmitEditing={() =>
                  this.customInput2.refs.innerTextInput2.focus()
                }
                returnKeyType="next"
                blurOnSubmit={false}
              />
              <Input
                label="Password"
                name="password"
                secureTextEntry={true}
                value={this.state.fields.password}
                onChangeText={text => this.handleChange(text, 'password')}
                errorMessage={this.state.errors.password}
                returnKeyType="done"
              />
            </View>
            <View style={{ alignItems: 'center' }}>
              <TouchableCmp disabled={this.state.isDisabled}
                onPress={this.handleSubmit}>
                <View
                  style={[this.state.isDisabled == false ? style.button : style.disabledButton]}>
                  <Text style={style.buttonTxt}>
                    Login
                    </Text>
                </View>
              </TouchableCmp>
              <TouchableOpacity style={style.forgotPassword} onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                <Text style={style.forgotPasswordTxt}>Forgot Password?</Text>
              </TouchableOpacity>

              <View style={{ flexDirection: 'row' }}>
                <Text style={style.signUpBtnTxt}>New here? </Text>
                <TouchableOpacity style={style.signUpBtn} onPress={() => this.props.navigation.navigate('Signup')}>
                  <Text style={style.signUpBtnTxt1}>Sign up</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}

export default Login
