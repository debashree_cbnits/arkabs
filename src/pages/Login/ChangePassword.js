import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableNativeFeedback,
  TouchableOpacity,
  SafeAreaView,
  Platform,
  ScrollView,
  TextInput,
  Alert,
  RefreshControlBase,
} from 'react-native';
import style from './style';
import Input from '../../Components/floatingInput';
import api from "../../Api/api";
import AsyncStorage from "@react-native-community/async-storage";


export default class CardDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPassword: '',
      newPassword:'',
      confirmPassword :''
    };
  }

  changePassSubmit = () => {
    AsyncStorage.getItem('UserDetails', (err, result) => {
      let userdata = JSON.parse(result);      
    if (this.state.currentPassword == "" && this.state.newPassword == ""  && this.state.confirmPassword == "" ){
      Alert.alert("Please enter the details")
    } else if (this.state.currentPassword == ""){
      Alert.alert("Please enter current password")
    } else if (this.state.newPassword == ""){
      Alert.alert("Please enter new password")
    }  else if (this.state.currentPassword === this.state.newPassword){
      Alert.alert("Current and new password shouldn't be same")
    } else if (this.state.confirmPassword == ""){
      Alert.alert("Please enter confirm password")
    }  else if (this.state.newPassword != this.state.confirmPassword){
      Alert.alert("New and Confirm password doesn't matched")
    } else {
      let data = {
        currentPassword: this.state.currentPassword,
        newPassword: this.state.newPassword,
        confirmPassword : this.state.confirmPassword
      }      
      api.put('user/changePassword/'+userdata.data._id, data,  userdata.token ).then((result) => {                    
        if (result.status == 200) {
          this.props.navigation.navigate('Login')
        } else {
          Alert.alert(result.msg)
        }
      }).catch((err) => {        
      console.log('error', err)
      });
    }
  })

  }
  render() {
    let TouchableCmp = TouchableOpacity;
    if (Platform.OS === 'android' && Platform.Version >= 21)
      TouchableCmp = TouchableNativeFeedback;
   
    return (
      <ScrollView style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={{...style.subContainer2, marginTop: 50}}>
          <Text style={style.containerTxt}>Change Password</Text>
        </View>

        <View style={{width: '100%', paddingHorizontal: '9%'}}>
          <TextInput
            placeholder="Current Password"
            placeholderTextColor="#aaa"
            returnKeyType="next"
            secureTextEntry={true}
            style={style.chngPassInput}
            value = {this.state.currentPassword}
            onChangeText ={(text)=> this.setState({currentPassword: text})}
          />

          <TextInput
            placeholder="New Password"
            placeholderTextColor="#aaa"
            returnKeyType="next"
            secureTextEntry={true}
            style={style.chngPassInput}
            value = {this.state.newPassword}
            onChangeText ={(text)=> this.setState({newPassword: text})}
          />

          <TextInput
            placeholder="Confirm Password"
            placeholderTextColor="#aaa"
            returnKeyType="next"
            autoCapitalize="none"
            returnKeyType="next"
            style={style.chngPassInput}
            value = {this.state.confirmPassword}
            onChangeText ={(text)=> this.setState({confirmPassword: text})}
          />
        </View>

        <View style={{alignItems: 'center', width: '100%'}}>
          <TouchableCmp
            // disabled={this.state.isDisabled}
            onPress={this.changePassSubmit}>
            <View
              style={style.button}>
              <Text style={style.buttonTxt}>Submit</Text>
            </View>
          </TouchableCmp>
        </View>
      </ScrollView>
    );
  }
}
