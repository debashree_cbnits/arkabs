export default {
  container: {
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  subContainer1: {
    paddingHorizontal: 20,
    marginTop: '15%',
  },
  subContainer2: {
    marginBottom: '15%',
    alignItems: 'center',
  },
  containerTxt: {
    fontFamily: 'Oswald-Medium',
    fontSize: 40,
    color: '#3498db',
  },
  button: {
    backgroundColor: '#0081E2',
    width: '80%',
    marginVertical: 50,
    borderRadius: 10,
    paddingTop: 5,
    paddingBottom: 10,
    alignItems: 'center',
    elevation: 3,
  },
  buttonTxt: {
    color: 'white',
    fontFamily: 'Oswald-Bold',
    fontSize: 22,
  },
  disabledButton: {
    backgroundColor: '#808080',
    width: '80%',
    marginVertical: 50,
    borderRadius: 10,
    paddingTop: 5,
    paddingBottom: 10,
    alignItems: 'center',
    elevation: 3,
  },
  forgotPassword: {
    alignItem: 'center',
    marginBottom: 10,
  },
  forgotPasswordTxt: {
    color: '#f00',
    fontSize: 18,
    fontFamily: 'Oswald-Light',
  },
  signUpBtn: {
    alignItem: 'center',
    flexDirection: 'row',
  },
  signUpBtnTxt: {
    color: '#3498db',
    fontSize: 18,
    fontFamily: 'Oswald-Regular',
  },
  signUpBtnTxt1: {
    color: '#3498db',
    fontSize: 18,
    fontFamily: 'Oswald-SemiBold',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },

  //new style for change password

  chngPassInput: {
    paddingHorizontal: 10,
    marginBottom: 20,
    borderBottomColor: '#aaa',
    borderBottomWidth: 1,
  },
};
