import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Image } from 'react-native'
import { Avatar, Icon, } from 'react-native-elements'
import styles from './style';
import * as firebase from 'firebase';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../Api/api';
import {NavigationEvents} from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';
import logoAvatar from '../../assets/imsges/logo.png';

// var firebaseConfig = {
//   apiKey: "AIzaSyBPn_p6JBWkIUSDzG0ke-0IGMNwrnfLpaY",
//   authDomain: "arkabs.firebaseapp.com",
//   databaseURL: "https://arkabs.firebaseio.com/",
//   projectId: "arkabs",
//   storageBucket: "arkabs.appspot.com",
//   messagingSenderId: "769927113864",
// }

var firebaseConfig = {
  apiKey: "AIzaSyBT6pCnWjwSOoVknx-no1HlpuSg4JRkJkQ",
  authDomain: "arkabs-280713.firebaseapp.com",
  databaseURL: "https://arkabs-280713.firebaseio.com",
  projectId: "arkabs-280713",
  storageBucket: "arkabs-280713.appspot.com",
  messagingSenderId: "1031996217292",
  appId: "1:1031996217292:web:6a5415afdc702f2a356f69",
  measurementId: "G-MMZT0R9B7E"
};

export default class messageScreen extends Component {
  constructor() {
    super()
    this.state = {
      loggedUserId: '',
      showLoader: false,
      senderIdList: [],
      chatRef: '',
      chatList: [],
      token: '',
      userId:'',
      profileDetails: [],
      loader: false
    }

    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
      this.state.chatRef = firebase.database().ref().child('chatMessages');
      // console.warn('chatRef:', this.state.chatRef)
    } else {
      this.state.chatRef = firebase.database().ref().child('chatMessages');
      //console.warn('chatRef:', this.state.chatRef)
    }

  }

  componentDidMount() {
    this.setState({
      loader: true
    })
    AsyncStorage.getItem('UserDetails', (err, result) => {
      if (result) {
        let UserDetails = JSON.parse(result);
        this.setState({userId:UserDetails.data._id})
        this.setState({ token: UserDetails.token })
        this.state.chatRef.orderByChild('chatRoomId').once('value').then((snapShot) => {
          if (snapShot.val()) {
            let chatList = [];
            let senderIdList = [];
            for (let key in snapShot.val()) {
              let IsPresent = false;
              if (snapShot.val()[key].senderId == UserDetails.data._id || snapShot.val()[key].userId == UserDetails.data._id || snapShot.val()[key].memberId == UserDetails.data._id) {
                for (let i = 0; i < chatList.length; i++) {
                  if (chatList[i].chatRoomId == snapShot.val()[key].chatRoomId) {
                    IsPresent = true;
                    break;
                  }
                }
                if (!IsPresent) {
                  chatList.push(snapShot.val()[key]);
                  senderIdList.push({ id: snapShot.val()[key].senderId });
                }
              }
            }
            this.setState({
              chatList: chatList,
              senderIdList: senderIdList,
              loader: false
            })

          }
          else {
            this.setState({
              loader: false
            })
          }
        }).catch((err) => {
          this.setState({
            loader: false
          })
        })
      }
      else {
        this.setState({
          loader: false
        })
      }
    })
  }


  goToChat(userId, chatRoomId, group, chatDetails) {    
    api.getUserDetails('user/' + userId, this.state.token).then((result) => {
      if (result.status == 200) {
        this.setState({
          profileDetails: result.data.user,
        });
      }
    }).catch((err) => {
    });
    if (group == true) {
      this.props.navigation.navigate('GroupChatDetails', { chatRoomId: chatRoomId, group: group, chatDetails: chatDetails });
    } else {
      this.props.navigation.navigate('ChatDetails', { receiverDetails: this.state.profileDetails, chatRoomId: chatRoomId, group: group });
    }
  }

  render() {    
  
    return (
      <>
        <View style={styles.logoDiv}>
          <NavigationEvents onDidFocus={() =>
           this.state.chatRef.orderByChild('chatRoomId').once('value').then((snapShot) => {
            if (snapShot.val()) {
              let chatList = [];
              let senderIdList = [];
              for (let key in snapShot.val()) {
                let IsPresent = false;
                if (snapShot.val()[key].senderId == this.state.userId || snapShot.val()[key].userId == this.state.userId  || snapShot.val()[key].memberId == this.state.userId ) {
                  for (let i = 0; i < chatList.length; i++) {
                    if (chatList[i].chatRoomId == snapShot.val()[key].chatRoomId) {
                      IsPresent = true;
                      break;
                    }
                  }
                  if (!IsPresent) {
                    chatList.push(snapShot.val()[key]);
                    senderIdList.push({ id: snapShot.val()[key].senderId });
                  }
                }
              }
              this.setState({
                chatList: chatList,
                senderIdList: senderIdList,
                loader: false
              })
             }
            else {
              this.setState({
                loader: false
              })
            }
          }).catch((err) => {
            this.setState({
              loader: false
            })
          })
          } />
          {/* <Image source={require("../../assets/imsges/logo1.png")} style={styles.logo} /> */}
          <Image source={logoAvatar} style={styles.logo} />
        </View>
        <ScrollView style={{backgroundColor:'#fff'}}>
          <View style={styles.container}>
            <View style={styles.messagelist}>
              <Spinner visible={this.state.loader}
                textContent={'Loading...'}
                textStyle={{ color: '#FFF' }} />
              {this.state.chatList.length > 0 ?
                this.state.chatList.map((item, key) => {
                  return (
                    <TouchableOpacity style={styles.msghead} onPress={() => this.goToChat(item.userId, item.chatRoomId, item.group, item)}>
                      {item.group ? <Avatar size="medium" rounded source={require("../../assets/imsges/logo.png")} /> :
                        <Avatar size="medium" rounded source={{ uri: item.userImage }} />}
                      <View style={styles.tcontent}>
                        <View style={styles.flextop}>
                          <Text numberOfLines={1} style={styles.namemsg}>{item.group ? item.groupName : item.username}</Text>
                          <Text style={styles.timemuted}>{item.date}</Text>
                        </View>
                        <Text numberOfLines={1} style={styles.chatext}>{item.Message}</Text>
                      </View>
                    </TouchableOpacity>
                  )
                })
                : <View style={{backgroundColor:'#fff'}}>
                  <Text style={{ textAlign: 'center', padding: 15, fontSize: 15, backgroundColor: '#fff' }}>No records found.</Text>
                </View>}
            </View>
          </View>
        </ScrollView>
      </>
    )
  }
}
