import React from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    SafeAreaView,
    ActivityIndicator
  } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import api from "../../Api/api"
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from 'react-native-loading-spinner-overlay';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import moment from 'moment';
import imageUrl from '../../Api/config';



class GroupInfoDetails extends React.Component{

    constructor (props){
        super(props);
        this.state ={
            chatDetails:this.props.navigation.state.params.chatDetails,
            chatRoomId: this.props.navigation.state.params.chatRoomId ? this.props.navigation.state.params.chatRoomId : '',
            memberArray : [],
            loader: true,
            tripDetails:{},            
            toLocationLatitude:0,
            toLocationLongitude:0,
            fromLocationLatitude:0,
            fromLocationLongitude:0,

        }
    }
    componentDidMount(){
        AsyncStorage.getItem('UserDetails', (err, result) => {
            let userdata = JSON.parse(result);
            this.setState({token: userdata.token }, ()=>{
                this.getMemberInfo();
            })
        })        
    }

    getMemberInfo () {
        this.setState({loader: true})
        api.GET('trip-api/trip/groupChat/list/'+this.state.chatRoomId, this.state.token).then((result) => {            
            this.setState({ memberArray:result.data, loader: false, tripDetails : result.trip}, ()=>{                
                this.setState({
                    toLocationLatitude : this.state.tripDetails.fromLocation.coordinates[1],
                    toLocationLongitude : this.state.tripDetails.fromLocation.coordinates[0],
                    fromLocationLatitude : this.state.tripDetails.toLocation.coordinates[1],
                    fromLocationLongitude : this.state.tripDetails.toLocation.coordinates[0],
                    loader: false }) 
            })
        }).catch((err) => {
        this.setState({ loader: false })
        console.log('error', err)
        });
    }
    render() {  
        if (this.state.loader) {
            return (
                <ActivityIndicator size="large" style={{flex:1}}/>
                // <Loader isLoading={true} loaderSize={'large'} size={70} />
            )
        }      
        return (
          <SafeAreaView style={{flex: 1}}>
              <View style={{flex: 1, paddingHorizontal: '5%', backgroundColor: '#fff'}}>
            <View style={{width: '100%', height: 300, marginTop: 20, borderRadius: 10, elevation: 3, backgroundColor: 'red', overflow: 'hidden', alignItems:'center', justifyContent:'center', backgroundColor: '#fff'}}>
                <MapView style={{width: '100%', height:'100%'}}
                provider={PROVIDER_GOOGLE}
                showsUserLocation
                initialRegion={{
                    latitude: this.state.fromLocationLatitude && Number(this.state.fromLocationLatitude),
                    longitude: this.state.fromLocationLongitude &&  Number(this.state.fromLocationLongitude),
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
            >
                    <MapView.Marker         
                    // pinColor='#0081E2'
                        coordinate={{
                            latitude: this.state.fromLocationLatitude && Number(this.state.fromLocationLatitude),
                            longitude: this.state.fromLocationLongitude &&  Number(this.state.fromLocationLongitude),
                            latitudeDelta: 0.4,
                            longitudeDelta: 0.41
                        }}
                    >
                        <Image source={require('../../assets/imsges/orangeMarker.png')} style={{height: 41, width:25 }} />
                    </MapView.Marker>
                    <MapView.Marker                 
                        // pinColor='#FCA600'
                        coordinate={{
                            latitude: this.state.toLocationLatitude && Number(this.state.toLocationLatitude),
                            longitude: this.state.toLocationLongitude &&  Number(this.state.toLocationLongitude),
                            latitudeDelta: 0.4,
                            longitudeDelta: 0.41
                        }}
                    >  
                        <Image source={require('../../assets/imsges/blueMarker.png')} style={{height: 41, width:25 }} />
                    </MapView.Marker>
                </MapView>
            {/* <Image source={require('../../assets/imsges/userDemo.jpg')}/> */}
            <View style={{ width: "100%", position: 'absolute', bottom: '5%', left: '5%'}}>
                <Text style={{width: '90%', color: '#000', elevation: 2, fontSize: 20, fontFamily: 'Oswald-Medium', }}>{this.state.chatDetails.groupName}</Text>
                <View style={{flexDirection:'row', justifyContent:'flex-start'}}>
                    <Text style={{width: '30%', color: '#000', elevation: 2, fontSize: 18, fontFamily: 'Oswald-Medium', }}>{moment(this.state.tripDetails.tripDate).format("D MMM YYYY")}</Text>
                    <Text style={{width: '20%', color: '#000', elevation: 2, fontSize: 18, fontFamily: 'Oswald-Medium', }}>{moment(this.state.tripDetails.tripDateTime).format('LT')}</Text>
                </View>

            </View>
            </View>
    
            <ScrollView showsVerticalScrollIndicator={false} style={{backgroundColor: '#fff'}}>
                {this.state.memberArray && this.state.memberArray.map((data,key)=>{
                    return(
                <View style={{width: '100%', marginVertical: 10, padding: 10, borderRadius: 10, backgroundColor: '#fff', elevation: 2, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <View style={{width: 60, height: 60, borderRadius: 60/2, justifyContent: 'center', alignItems: 'center', overflow: 'hidden'}}>
                    
                    {data.profilePicture ? 
                    <Image source={{ uri: data.profilePicture.includes('/userImage') ? imageUrl.img_url+data.profilePicture : data.profilePicture }} style={{width: 60, height: 60}}/>:
                    <Image source={require('../../assets/imsges/userDemo.jpg')} style={{width: 60, height: 60}}/>}
                    </View>
                    <Text style={{width: '75%', color: '#000', fontSize: 18, fontFamily: 'Oswald-Regular', }}>{data.name}</Text>
                </View>
                )})}
    
                {/* <View style={{width: '100%', marginVertical: 10, padding: 10, borderRadius: 10, backgroundColor: '#fff', elevation: 2, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <View style={{width: 60, height: 60, borderRadius: 60/2, justifyContent: 'center', alignItems: 'center', overflow: 'hidden'}}>
                    <Image source={require('../../assets/imsges/userDemo.jpg')} style={{width: 60, height: 60}}/>
                    </View>
                    <Text style={{width: '75%', color: '#000', fontSize: 18, fontFamily: 'Oswald-Regular', }}>Rhea Chakraborty</Text>
                </View>
    
    
                <View style={{width: '100%', marginVertical: 10, padding: 10, borderRadius: 10, backgroundColor: '#fff', elevation: 2, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <View style={{width: 60, height: 60, borderRadius: 60/2, justifyContent: 'center', alignItems: 'center', overflow: 'hidden'}}>
                    <Image source={require('../../assets/imsges/userDemo.jpg')} style={{width: 60, height: 60}}/>
                    </View>
                    <Text style={{width: '75%', color: '#000', fontSize: 18, fontFamily: 'Oswald-Regular', }}>Rhea Chakraborty</Text>
                </View>
    
                <View style={{width: '100%', marginVertical: 10, padding: 10, borderRadius: 10, backgroundColor: '#fff', elevation: 2, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <View style={{width: 60, height: 60, borderRadius: 60/2, justifyContent: 'center', alignItems: 'center', overflow: 'hidden'}}>
                    <Image source={require('../../assets/imsges/userDemo.jpg')} style={{width: 60, height: 60}}/>
                    </View>
                    <Text style={{width: '75%', color: '#000', fontSize: 18, fontFamily: 'Oswald-Regular', }}>Rhea Chakraborty</Text>
                </View> */}
                
            </ScrollView>
            </View>
          </SafeAreaView>
        );
      }
    }
export default GroupInfoDetails