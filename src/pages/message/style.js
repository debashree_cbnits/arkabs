
const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
    container: {
        paddingLeft:10,
        paddingRight:10,
        flex:1,
        backgroundColor:'#fff'
    },
    logoDiv:{      
        width:'100%',
        justifyContent:'center',
        alignItems:'center', 
        backgroundColor: '#fff'
    },
    logo:{
        width: '60%',
        height: 120
    },
    messagelist: {
        paddingTop:15,
        paddingBottom:15,
        backgroundColor:'#fff'
    },
    msghead: {
        flexDirection:'row', 
        paddingTop:20,
        paddingBottom:20,
        borderTopWidth:1,
        borderColor:'#EEE'     
    },
    tcontent:{
        width:'85%',
        paddingLeft:14,        
    },
    flextop: {
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    namemsg: {
        fontSize:15,
        fontFamily: 'Oswald-Bold',
        color:'#444',
        width:'80%'
    },
    timemuted: {
        fontSize:12,
        color:'#888',
        fontFamily: 'Oswald-Regular'       
    },
    chatext: {
        fontSize:16,
        color:'#777',
        marginTop:3,
        fontFamily: 'Oswald-Regular'
    },
    topmenu:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:30,
        paddingBottom:10,
        backgroundColor:'#3498db'
    },
    chatbody:{
        paddingLeft:15,
        paddingRight:15,
        paddingBottom:45
    },
  
     row: {
         flexDirection:'row'
     },
     uname: {
        fontSize: 14, 
        color: '#006bb3',
        fontWeight: 'bold',
        position:'relative',
        top:-12,
        paddingRight:90,
        marginBottom:-10
     },
     rightchat: {
        backgroundColor: '#0081E2',
        padding: 10,
        borderRadius: 7,
        maxWidth: '80%',
        marginLeft: 'auto',
        position: 'relative',
        marginBottom: 10,
        minWidth: 90,
        borderTopRightRadius: 0,
    },
    leftchat: {
        // backgroundColor: '#808080',
        backgroundColor: '#FCA600',
        maxWidth: '80%',
        marginLeft: 0,
        padding: 10,
        borderRadius: 7,
        marginRight: 'auto',
        position: 'relative',
        marginBottom: 10,
        minWidth: 90,
        borderBottomLeftRadius: 0,
    },
    chatdate: {
        fontSize: 11,
        padding: 10,
        // textAlign:'center',
        color: '#fff',
        position: 'absolute',
        top: -19,
        right: 0
    },
    megtext: {
        color: '#141414',
        fontSize: 14,
        flexWrap: 'wrap'
    },
    dateright: {
        color: '#fff',
        top: -3,
    },
    dateright2: {
        color: '#fff',
        top: -3,
        left: 0,
    },
    chatText: {
        color: '#fff',
        fontSize: 14,
        marginTop: 15,
    },
    footerSec: {
        backgroundColor: '#fff',
        borderTopColor: '#ddd',
        borderTopWidth: 1,
        height: 70
    },
    chatInput: {
        width: deviceWidth - 100,
        height: 45,
        color: '#000',
        marginRight: 20,
        borderRadius: 45 / 2,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: "rgba(0,0,0,0.4)",
        paddingHorizontal: 20
    },
    chatSend:{
        width: 45, 
        height: 45, 
        borderRadius: 45/2, 
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: '#0081E2'
    }
}