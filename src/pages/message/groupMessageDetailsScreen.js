import React, { Component } from 'react';
import { Image, View, ScrollView, StatusBar, TouchableOpacity, ImageBackground, TextInput, BackHandler, Alert } from "react-native";
import { Container, Button, H3, Text, Footer } from "native-base";
import { Avatar, Badge, } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from 'react-native-loading-spinner-overlay';
import styles from "./style";
import * as firebase from 'firebase';
import api from "../../Api/api"

// var firebaseConfig = {
//     apiKey: "AIzaSyBPn_p6JBWkIUSDzG0ke-0IGMNwrnfLpaY",
//     authDomain: "arkabs.firebaseapp.com",
//     databaseURL: "https://arkabs.firebaseio.com/",
//     projectId: "arkabs",
//     storageBucket: "arkabs.appspot.com",
//     messagingSenderId: "769927113864",
// }

var firebaseConfig = {
    apiKey: "AIzaSyBT6pCnWjwSOoVknx-no1HlpuSg4JRkJkQ",
    authDomain: "arkabs-280713.firebaseapp.com",
    databaseURL: "https://arkabs-280713.firebaseio.com",
    projectId: "arkabs-280713",
    storageBucket: "arkabs-280713.appspot.com",
    messagingSenderId: "1031996217292",
    appId: "1:1031996217292:web:6a5415afdc702f2a356f69",
    measurementId: "G-MMZT0R9B7E"
};

class ChatDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            senderId: '',
            senderName: '',
            senderImage: '',
            // userId: this.props.navigation.state.params.receiverDetails._id,
            // username: this.props.navigation.state.params.receiverDetails.name,
            // userImage: this.props.navigation.state.params.receiverDetails.profilePicture,
            chatRoomId: this.props.navigation.state.params.chatRoomId ? this.props.navigation.state.params.chatRoomId : '',
            chatList: [],
            typeMessage: '',
            IsCustomerSender: true,
            loader: false,
            group:this.props.navigation.state.params.group,
            chatDetails:this.props.navigation.state.params.chatDetails,
            memberArray:[],
            userIdArray:[],

        }
       // console.warn('snapshot value',this.state.chatDetails)
        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
            this.state.chatRef = firebase.database().ref().child('chatMessages');
        } else {
            this.state.chatRef = firebase.database().ref().child('chatMessages');
        }

        this.state.chatRef.on('child_added', (snapshot) => {
            //console.warn('snapshot value',snapshot.val())
            const snapShotVal = snapshot.val();
            if (snapShotVal.chatRoomId == this.state.chatRoomId) {
                let chatList = this.state.chatList;
                const item = snapShotVal;
                chatList.push(item);
                this.setState({ typeMessage: '', chatList: chatList });
            }
            setTimeout(() => {
                if (this.refs && this.refs.scrollView) {
                    this.refs.scrollView.scrollToEnd(true);
                }
            }, 400);

        })

    }

    componentDidMount() {
        this.getStorageValue();
    }

    getStorageValue = () => {
        AsyncStorage.getItem('UserDetails', (err, result) => {
            let userdata = JSON.parse(result);
            //console.warn('roomId:',roomId)
            this.setState({
                senderId: userdata.data._id,
                senderImage: userdata.data.profilePicture,
                senderName: userdata.data.name,
                chatList: this.state.chatList,
                token: userdata.token 
            }, ()=> this.getMemberInfo(this.state.token));
           
            this.state.chatRef.orderByChild('chatRoomId').equalTo(this.state.chatRoomId).once('value').then((snapshot) => {
                if (snapshot.val()) {
                    var listMesage = [];
                    for (let key in snapshot.val()) {
                        listMesage.push(snapshot.val()[key]);
                    }
                    this.setState({
                        chatList: listMesage,
                        loader: false
                    });
                }
            }).catch((Err) => {
                this.setState({
                    loader: false
                })
            })


        });

    }


    getMemberInfo (token) {  
        // console.log (this.state.chatRoomId)      
        api.GET('trip-api/trip/groupChat/list/'+this.state.chatRoomId, token).then((result) => {            
            this.setState({ memberArray:result.data}, ()=>{                
                let array=[];
                for (let i = 0 ; i< this.state.memberArray.length; i++){
                   array.push (this.state.memberArray[i].userId) 
                }  
                this.setState({userIdArray: array})                              
            })
        }).catch((err) => {        
        console.log('error', err)
        });
    }

    sendMessage() {       
        const nowDate = new Date().toLocaleDateString();
        if (this.state.typeMessage && this.state.typeMessage.trim()) {
            this.state.chatRef.push({
                "group": true,
                "groupName": this.state.chatDetails.groupName,
                "adminId": this.state.chatDetails.adminId,
                "adminName": this.state.chatDetails.adminName,
                "senderId": this.state.senderId,
                "senderName": this.state.senderName,
                "Message": this.state.typeMessage,
                "chatRoomId": this.state.chatRoomId,
                "memberId": this.state.senderId,
                "memberName":this.state.senderName,
                "date": nowDate,
            });
            let data ={
                "userId" : this.state.userIdArray,
                "senderId": this.state.senderId,
                "Message": this.state.typeMessage,
            }            
            api.POST('feedback-api/groupChat', data,  this.state.token ).then((result) => {                       
              }).catch((err) => {        
              console.log('error', err)
              });
        }        
        else {
            Alert.alert('', 'Plesae type message to send.');
        }
    }

    getGroupDetails(){  
        this.props.navigation.navigate('GroupInfoDetails' , {chatRoomId : this.state.chatRoomId, chatDetails: this.state.chatDetails})             
    }


    render() {
        // console.log('group====', this.state.chatDetails)
        return (
            <View style={[styles.homebody, { flex: 1 }]}>

                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <View style={[styles.topmenu, { paddingTop: 10 }]}>
                    <Spinner visible={this.state.loader}
                        textContent={'Loading...'}
                        textStyle={{ color: '#FFF' }} />
                    <TouchableOpacity style={{ width: '100%', justifyContent: 'center' }} onPress={()=> this.getGroupDetails()}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ marginRight: 10 }}>
                            <Avatar size="medium" rounded source={require("../../assets/imsges/logo.png")}/>                   
                            </View>
                            <View>
                                <Text style={{ color: 'white', fontFamily: 'Oswald-Bold', fontSize: 15, width:'100%',paddingRight:10 }}>{this.state.chatDetails.groupName}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <ScrollView ref='scrollView'>
                    <View style={styles.chatbody}>
                        <Text style={{ color: '#9F9FA0', fontSize: 12, paddingLeft: 15, paddingRight: 15, textAlign: 'center', marginBottom: 15 }}></Text>
                        {
                            this.state.chatList.map((data, key) => (
                                data.senderId == this.state.senderId ? (
                                    <View>
                                        {data.Message? 
                                    <View>
                                        <View style={styles.rightchat}>
                                            <Text style={[styles.msgtext, {color:'#fff', marginTop: 10}]}  numberOfLines={5}>{data.Message}</Text>
                                            <Text style={[styles.chatdate, styles.dateright]}>{data.date}</Text>
                                        </View>
                                        
                                    </View>
                                     :null}
                                     </View>
                                ) :
                                    ( <View>
                                        {data.Message?  
                                        <View>
                                            <View style={styles.leftchat}>
                                                <View style={{width:'auto'}}>
                                                    <Text style={styles.uname}>{data.senderName}: </Text>
                                                    <Text style={styles.chatdate}>{data.date}</Text>
                                                </View>
                                                <Text style={styles.msgtext}  numberOfLines={5}>{data.Message}</Text>
                                               
                                            </View>
                                        </View>
                                        :null}
                                        </View>
                                    )

                            ))
                        }

                    </View>
                </ScrollView>
                <Footer style={styles.footerSec}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput style={styles.chatInput} placeholder={'Type a message...'} onChangeText={(text) => this.setState({ typeMessage: text })} value={this.state.typeMessage} />
                        <TouchableOpacity style={styles.chatSend} onPress={() => this.sendMessage()} >
                        <Image source={require("../../assets/imsges/msg2.png")} style={{width: 23, height: 23}}/>
                            {/* <Text style={{ color: '#3498db', fontSize: 15, fontWeight: '500' }}>Send</Text> */}
                        </TouchableOpacity>
                    </View>
                </Footer>
            </View>

        )

    }
}


export default ChatDetails;
