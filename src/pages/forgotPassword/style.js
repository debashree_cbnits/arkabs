export default{
    container:{
        paddingHorizontal: 20, 
        marginTop: "15%",
        backgroundColor: '#fff'
    },
    subContainer1:{
        marginBottom: '10%', 
        alignItems: 'center' 
    },
    subContainer2:{
        fontFamily: 'Oswald-SemiBold',
        fontSize: 40,
        color: '#3498db' 
    },
    button:{
            backgroundColor: "#3498db",
            width: 220,
            marginVertical: 50,
            borderRadius: 35,
            paddingTop: 10,
            paddingBottom: 10,
            alignItems: 'center',
            elevation: 3
    },
    buttonTxt:{
        color: 'white',
        fontFamily: 'Oswald-Bold',
        fontSize: 22 
    },
    disabledButton:{
        backgroundColor: "#808080",
        width: 200,
        marginVertical: 50,
        borderRadius: 35,
        paddingTop: 5,
        paddingBottom: 10,
        alignItems: 'center',
        elevation: 3 
    },
    spinnerTextStyle:{
        color: '#FFF'
      }


}