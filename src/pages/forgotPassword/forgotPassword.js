import React, { Component } from 'react'
import { View, Text, Image, SafeAreaView, TouchableOpacity, TouchableNativeFeedback, Platform, KeyboardAvoidingView, Picker, Alert } from 'react-native'
import Input from "../../Components/floatingInput"
import AsyncStorage from '@react-native-community/async-storage';
import { GlobalStyles } from '../../assets/StylesGlobal/GlobalStyle'
import { ScrollView } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/FontAwesome';
import style from './style';
import Validator from '../../helper/validator';
import api from '../../Api/api'
import DatePicker from 'react-native-datepicker'
import { Result } from 'antd-mobile'
import Spinner from 'react-native-loading-spinner-overlay';
export class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            field:{
                email: '',
            },
            fields:{
                password: '',
                con_password: '',
                verifyOTP:''
            },
            User: {},
            errors: {},
            loader:false,
            isEmailSend:false,
            isDisabled:false
        };
    }
    componentDidMount(){
       
    }
    handleChange(value, name) {
        let fields = this.state.fields;
        fields[name] = value;
        this.setState({ fields });       
        this.setState({ errors: Validator.validateForm(name, this.state.fields, this.state.errors) });
      
    }
    handleChangeEmail(value, name) {
        let field = this.state.field;
        field[name] = value;
        this.setState({ field });     
        this.setState({ errors: Validator.validateForm(name, this.state.field, this.state.errors) });
      
    }
    handleSubmitEmail = (e) =>{
        e.preventDefault();
        this.setState({ errors: Validator.validateForm(null, this.state.field, this.state.errors) });
        if (this.state.errors.formIsValid) {
            this.setState({loader:true})
            var data={
                email:this.state.field.email,
            }
            api.post('user/forgotPassword',data).then(Result => {
                this.setState({loader:false})
                if(Result.status == 200){
                   this.setState({isEmailSend:true})
               }else{
                   Alert.alert(Result.msg)
                   this.setState({ errors: {},isEmailSend:false });
               }
			}).catch(err => {
                this.setState({loader:false})
                console.log('error',err)
			})           
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({ errors: Validator.validateForm(null, this.state.fields, this.state.errors) });
        if (this.state.errors.formIsValid) {
            this.setState({isDisabled:true,loader:true})
            var data={
                email:this.state.field.email,
                otp:this.state.fields.verifyOTP,
                password:this.state.fields.password,
                confirmPassword:this.state.fields.con_password
            }
            api.post('user/setPassword',data).then(Result => {
                this.setState({loader:false})
                 if(Result.status == 200){
                    Alert.alert(Result.msg)
                    this.props.navigation.navigate('Login')
               }else{
                   Alert.alert(Result.msg)
                   this.setState({ fields: {},isDisabled:false ,isEmailSend:false, errors: {} });
               }
			}).catch(err => {
                this.setState({loader:false})
               console.log('error',err)
			})           
        }
    }

    render() {
        let TouchableCmp = TouchableOpacity;
        if (Platform.OS === 'android' && Platform.Version >= 21)
            TouchableCmp = TouchableNativeFeedback;
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#fff'}}>
                <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={style.spinnerTextStyle}/>
                <ScrollView keyboardShouldPersistTaps="always" keyboardDismissMode='on-drag'>
                    <KeyboardAvoidingView behavior="padding">
                        <View style={style.container}>
                            <View style={style.subContainer1}>
                                <Text style={style.subContainer2}>Forgot Password</Text>
                            </View>
                            <View pointerEvents={this.state.isEmailSend == true ? 'none' : 'auto'}>
                                <Input
                                    label="University Email"
                                    name="Email"
                                    //iconName="envelope-o"
                                    keyboardType="email-address"
                                    autoCapitalize="none"
                                    value={this.state.field.email}
                                    onChangeText={text => this.handleChangeEmail(text, 'email')}
                                    errorMessage={this.state.errors['email']}
                                    ref={ref => (this.customInput3 = ref)}
                                    refInner="innerTextInput3"
                                    onSubmitEditing={() =>
                                        this.customInput4.refs.innerTextInput4.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                />
                                </View>
                            {this.state.isEmailSend == false ?
                                <View style={{ alignItems: 'center' }}>
                                    <TouchableCmp disabled={this.state.isDisabled}
                                        background={TouchableNativeFeedback.Ripple("#fca600")}
                                        onPress={this.handleSubmitEmail}>
                                        <View
                                            style={[this.state.isDisabled == false ? style.button : style.disabledButton]}>
                                            <Text style={style.buttonTxt}>
                                                Send email
                                    </Text>
                                        </View>
                                    </TouchableCmp>
                                </View>
                                : <View>
                                    <Input
                                        label="Enter the Opt sent to your mail"
                                        name="Otp"
                                        keyboardType="number-pad"
                                        //iconName="key"
                                        value={this.state.fields.verifyOTP}
                                        onChangeText={text => this.handleChange(text, 'verifyOTP')}
                                        errorMessage={this.state.errors['verifyOTP']}
                                        ref={ref => (this.customInput8 = ref)}
                                        refInner="innerTextInput8"
                                        onSubmitEditing={() =>
                                            this.customInput8.refs.innerTextInput8.focus()
                                        }
                                        returnKeyType="next"
                                        blurOnSubmit={false}
                                    />
                                    <Input
                                        label="New Password"
                                        name="password"
                                        //iconName="lock"
                                        secureTextEntry={true}
                                        value={this.state.fields.password}
                                        onChangeText={text => this.handleChange(text, 'password')}
                                        errorMessage={this.state.errors['password']}
                                        ref={ref => (this.customInput5 = ref)}
                                        refInner="innerTextInput5"
                                        onSubmitEditing={() =>
                                            this.customInput6.refs.innerTextInput6.focus()
                                        }
                                        returnKeyType="next"
                                        blurOnSubmit={false}
                                    />
                                    <Input
                                        label="Confirm Password"
                                        name="con_password"
                                        //iconName="lock"
                                        secureTextEntry={true}
                                        value={this.state.fields.con_password}
                                        onChangeText={text => this.handleChange(text, 'con_password')}
                                        errorMessage={this.state.errors['con_password']}
                                        ref={ref => (this.customInput6 = ref)}
                                        refInner="innerTextInput6"
                                        returnKeyType="done"
                                    />
                                </View>
                            }
                               <View style={{ alignItems: 'center' }}>
                                {this.state.isEmailSend == true?
                                <TouchableCmp disabled={this.state.isDisabled}
                                    background={TouchableNativeFeedback.Ripple("#fca600")}
                                    onPress={this.handleSubmit}>
                                        <View
                                            style={[this.state.isDisabled == false ? style.button : style.disabledButton]}>
                                            <Text style={style.buttonTxt}>
                                                Set Password
                                        </Text>
                                        </View>                    
                                </TouchableCmp>
                                :null}
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default ForgotPassword
