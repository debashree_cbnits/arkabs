import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, Platform, TouchableNativeFeedback, SafeAreaView, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
// import { Input } from "react-native-elements"
import Input from "../../Components/floatingInput"
import { GlobalStyles } from "../../assets/StylesGlobal/GlobalStyle"
import style from './style';
import { ScrollView } from 'react-native-gesture-handler';
import Validator from '../../helper/validator';
import api from '../../Api/api';
import { connect } from "react-redux"
import { changeAuthState } from "../../Redux/AuthReducer/Reducer";
import Spinner from 'react-native-loading-spinner-overlay';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { color } from 'react-native-reanimated';
export class AboutUs extends Component {
    constructor(props) {
        super(props)
        this.state = {
            cmsTitle: '',
            cmsContent: "",
            loader: false,
        }
    }
    componentDidMount() {
        this.setState({ loader: true });
        AsyncStorage.getItem('UserDetails').then(res => {
            if (res) {
                let userDetails = JSON.parse(res);
                api.GET('feedback-api/feedback/cms/about-us', userDetails.token).then(Result => {
                    this.setState({ loader: false ,
                        cmsTitle:Result.data.cmsTitle,
                        cmsContent:Result.data.cmsContent});
                    if (Result.status == 200) {
                    } else {
                        this.setState({ isDisabled: false });
                    }
                }).catch(err => {
                    this.setState({ loader: false });
                })
            }
        }).catch = (err) => {
            console.log(err)
        }
    }
    render() {
        let TouchableCmp = TouchableOpacity;
        if (Platform.OS === 'android' && Platform.Version >= 21)
            TouchableCmp = TouchableNativeFeedback;
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={{color:"#FFF"}} />
                <ScrollView style={{backgroundColor: '#fff'}}>
                    <View style={{...style.container, alignItems: 'center'}}>
                        <View style={style.subContainer1}>
                            <View style={style.subContainer2}>
                                <Text style={style.containerTxt}>{this.state.cmsTitle}</Text>
                            </View>
                            <Text style={style.infotext}>{this.state.cmsContent}</Text>

                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default AboutUs
