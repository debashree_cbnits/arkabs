import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, Platform, TouchableNativeFeedback, SafeAreaView, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
// import { Input } from "react-native-elements"
import Input from "../../Components/floatingInput"
import { GlobalStyles } from "../../assets/StylesGlobal/GlobalStyle"
import style from './style';
import { ScrollView } from 'react-native-gesture-handler';
import Validator from '../../helper/validator';
import api from '../../Api/api';
import { connect } from "react-redux"
import { changeAuthState } from "../../Redux/AuthReducer/Reducer";
import Spinner from 'react-native-loading-spinner-overlay';
import Ionicons from 'react-native-vector-icons/Ionicons';
export class Feedback extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comment: '',
            emoji: "0",
            userId: "",
            token: "",
            isDisabled: false,
            loader: false,
            totalStar:  
            [{"value" : 1, "select" : 0}, 
            {"value" : 2, "select" : 0}, 
            {"value" : 3, "select" : 0},
            {"value" : 4, "select" : 0},
            {"value" :5, "select" : 0}
            ] 
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('UserDetails').then(res => {
            if (res) {
                let userDetails = JSON.parse(res);
                this.setState({
                    userId: userDetails.data._id,
                    token: userDetails.token
                })
            }
        }).catch = (err) => {
            console.log(err)
        }
    }

    setRating = (value, key) => {
        let newArray = [{"value" : 1, "select" : 0}, 
         {"value" : 2, "select" : 0}, 
         {"value" : 3, "select" : 0},
         {"value" : 4, "select" : 0},
         {"value" : 5, "select" : 0}
         ] 
         for(let i = 0; i < value; i++){                
             newArray[i].select = 1                          
         } 
         this.setState({ emoji: value, showrating: key, totalStar:newArray})
     }
     
    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({ isDisabled: true, loader: true });
        let inputData = {
            emoji: this.state.emoji,
            comment: this.state.comment,
            userId: this.state.userId
        }
        
        api.POST('feedback-api/feedback', inputData, this.state.token).then(Result => {
            this.setState({ loader: false });
            if (Result.status == 200) {
                Alert.alert(Result.msg)
                this.props.navigation.navigate('Account')
            } else {
                Alert.alert(Result.msg)
                this.setState({ isDisabled: false });
            }
        }).catch(err => {
            this.setState({ loader: false });
        })

    }

    render() {
        let TouchableCmp = TouchableOpacity;
        if (Platform.OS === 'android' && Platform.Version >= 21)
            TouchableCmp = TouchableNativeFeedback;
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
                <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={style.spinnerTextStyle} />
                {/* aspectRatio:1, */}
                <ScrollView style={{backgroundColor:'#fff'}}>
                    <View style={style.container}>
                        <View style={style.subContainer1}>
                            <View style={style.subContainer2}>
                                <Text style={style.containerTxt}>Feedback</Text>
                            </View>

                            <View style={{ paddingTop: 20, width: '100%',alignItems:'center'}}>
                                <View style={{ flex: 1,justifyContent: 'space-between', width: 250, flexDirection: 'row' }}>
                                {this.state.totalStar.map((value, key) => {
                                        return (                                                                                
                                            <TouchableOpacity onPress={() => this.setRating(value.value, key)}>
                                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                                    {/* <Text style={{ paddingRight: 5, color: 'rg                console.warn (ratingArray)console.warn (ratingArray)ba(255, 152, 0, 1)', lineHeight: 12 }}>{value}</Text> */}
                                                    {
                                                        value.select == 1? (                                                           
                                                            < Ionicons id={key} size={30} name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />                                                                                                                                                                      
                                                        ) :
                                                            (
                                                            < Ionicons size={30} name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                            )
                                                    }
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    })}
                                    {/* <TouchableOpacity style={{ borderRadius: 20, borderColor: 'rgba(255, 152, 0, 1)', borderWidth: 1 }} onPress={() => this.setState({ emoji: "1" })}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                            <Text style={{ paddingRight: 5, color: 'rgba(255, 152, 0, 1)', lineHeight: 12 }}> 1</Text>
                                            {
                                                this.state.emoji == "1" ? (
                                                    < Ionicons name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                ) :
                                                    (
                                                        < Ionicons name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                    )
                                            }

                                        </View>

                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ borderRadius: 20, borderColor: 'rgba(255, 152, 0, 1)', borderWidth: 1 }} onPress={() => this.setState({ emoji: "2" })}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                            <Text style={{ paddingRight: 5, color: 'rgba(255, 152, 0, 1)', lineHeight: 12 }}> 2</Text>
                                            {
                                                this.state.emoji == "2" ? (
                                                    < Ionicons name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                ) :
                                                    (
                                                        < Ionicons name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                    )
                                            }
                                        </View>

                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ borderRadius: 20, borderColor: 'rgba(255, 152, 0, 1)', borderWidth: 1 }} onPress={() => this.setState({ emoji: "3" })}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                            <Text style={{ paddingRight: 5, color: 'rgba(255, 152, 0, 1)', lineHeight: 12 }}> 3</Text>
                                            {
                                                this.state.emoji == "3" ? (
                                                    < Ionicons name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                ) :
                                                    (
                                                        < Ionicons name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                    )
                                            }
                                        </View>

                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ borderRadius: 20, borderColor: 'rgba(255, 152, 0, 1)', borderWidth: 1 }} onPress={() => this.setState({ emoji: "4" })}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                            <Text style={{ paddingRight: 5, color: 'rgba(255, 152, 0, 1)', lineHeight: 12 }}> 4</Text>
                                            {
                                                this.state.emoji == "4" ? (
                                                    < Ionicons name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                ) :
                                                    (
                                                        < Ionicons name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                    )
                                            }
                                        </View>

                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ borderRadius: 20, borderColor: 'rgba(255, 152, 0, 1)', borderWidth: 1 }} onPress={() => this.setState({ emoji: "5" })}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                            <Text style={{ paddingRight: 5, color: 'rgba(255, 152, 0, 1)', lineHeight: 12 }}> 5</Text>
                                            {
                                                this.state.emoji == "5" ? (
                                                    < Ionicons name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                ) :
                                                    (
                                                        < Ionicons name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                    )
                                            }
                                        </View>

                                    </TouchableOpacity> */}
                                </View>

                            </View>

                            <View>
                                <Input
                                    label="Feedback"
                                    name="Feedback"
                                    value={this.state.comment}
                                    onChangeText={text => this.setState({ comment: text })}
                                    ref={ref => (this.customInput4 = ref)}
                                    refInner="innerTextInput4"
                                    onSubmitEditing={() =>
                                        this.customInput5.refs.innerTextInput5.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                />
                            </View>
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <TouchableCmp disabled={this.state.isDisabled}
                                // background={TouchableNativeFeedback.Ripple(
                                //     "#fca600",
                                // )}
                                onPress={this.handleSubmit}>
                                <View
                                    style={[this.state.isDisabled == false ? style.button : style.disabledButton]}>
                                    <Text style={style.buttonTxt}>
                                        Send
                                     </Text>
                                </View>
                            </TouchableCmp>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default Feedback
