const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
    container: {
        justifyContent: "center",
        backgroundColor: '#fff'
    },
    subContainer1: {
        paddingHorizontal: 20,
        marginTop: "15%"
    },
    subContainer2: {
        marginBottom: '15%',
        alignItems: 'center'
    },
    containerTxt: {
        fontFamily: 'Oswald-Medium',
        fontSize: 40,
        color: '#3498db'
    },
    button: {
        backgroundColor: "#3498db",
        width: "80%",
        marginVertical: 50,
        borderRadius: 35,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        elevation: 6
    },
    buttonTxt: {
        color: 'white',
        fontFamily: 'Oswald-Bold',
        fontSize: 24
    },
    disabledButton: {
        backgroundColor: "#808080",
        width: "70%",
        marginVertical: 50,
        borderRadius: 35,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        elevation: 3
    },
    forgotPassword: {
        alignItem: "center",
        marginBottom: 10
    },
    forgotPasswordTxt: {
        color: "#f00",
        fontSize: 18,
        fontFamily: 'Oswald-Light'
    },
    signUpBtn: {
        alignItem: "center",
        flexDirection: 'row'
    },
    signUpBtnTxt: {
        color: "#3498db",
        fontSize: 18,
        fontFamily: 'Oswald-Regular'
    },
    signUpBtnTxt1: {
        color: "#3498db",
        fontSize: 18,
        fontFamily: 'Oswald-SemiBold'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    topmenu: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 30,
        paddingBottom: 10,
        backgroundColor: '#3498db'
    },
    chatbody: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 45
    },
    rightchat: {
        backgroundColor: '#3498db',
        padding: 10,
        borderRadius: 7,
        maxWidth: '80%',
        marginLeft: 'auto',
        position: 'relative',
        marginBottom: 10,
        minWidth: 90,
        borderTopRightRadius: 0,
    },
    leftchat: {
        backgroundColor: '#FCA600',
        maxWidth: '80%',
        marginLeft: 0,
        padding: 10,
        borderRadius: 7,
        marginRight: 'auto',
        position: 'relative',
        marginBottom: 10,
        minWidth: 90,
        borderBottomLeftRadius: 0,
    },
    chatdate: {
        fontSize: 11,
        padding: 10,
        // textAlign:'center',
        color: '#fff',
        position: 'absolute',
        top: -19,
        right: 0
    },
    megtext: {
        color: '#141414',
        fontSize: 14,
        flexWrap: 'wrap'
    },
    dateright: {
        color: '#fff',
        top: -3,
    },
    dateright2: {
        color: '#fff',
        top: -3,
        left: 0,
    },
    chatText: {
        color: '#fff',
        fontSize: 14,
        marginTop: 15,
    },
    footerSec: {
        backgroundColor: '#fff',
        borderTopColor: '#ddd',
        borderTopWidth: 1,
        height: 70
    },
    chatInput: {
        width: deviceWidth - 100,
        height: 45,
        color: '#000',
        marginRight: 20,
        borderRadius: 45 / 2,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: "rgba(0,0,0,0.4)",
        paddingHorizontal: 20
    },
    chatSend:{
        width: 45, 
        height: 45, 
        borderRadius: 45/2, 
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: '#0081E2'
    }
}