import React, { Component } from 'react';
import { Image, View, ScrollView, StatusBar, Dimensions, TouchableOpacity, ImageBackground, TextInput, BackHandler, Alert } from "react-native";
import { Container, Button, H3, Text, Footer } from "native-base";
import { Avatar, Badge, } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from 'react-native-loading-spinner-overlay';
import styles from "./style";
import * as firebase from 'firebase';
import imageUrl from '../../Api/config';
import moment from 'moment';
import api from '../../Api/api';


const deviceWidth = Dimensions.get("window").width;

// var firebaseConfig = {
//     apiKey: "AIzaSyBPn_p6JBWkIUSDzG0ke-0IGMNwrnfLpaY",
//     authDomain: "arkabs.firebaseapp.com",
//     databaseURL: "https://arkabs.firebaseio.com/",
//     projectId: "arkabs",
//     storageBucket: "arkabs.appspot.com",
//     messagingSenderId: "769927113864",
// }
var firebaseConfig = {
    apiKey: "AIzaSyBT6pCnWjwSOoVknx-no1HlpuSg4JRkJkQ",
    authDomain: "arkabs-280713.firebaseapp.com",
    databaseURL: "https://arkabs-280713.firebaseio.com",
    projectId: "arkabs-280713",
    storageBucket: "arkabs-280713.appspot.com",
    messagingSenderId: "1031996217292",
    appId: "1:1031996217292:web:6a5415afdc702f2a356f69",
    measurementId: "G-MMZT0R9B7E"
};

class Feedback extends Component {
    constructor(props) {
        super(props)
        this.state = {
            senderId: '',
            senderName: '',
            senderImage: '',
            username:'',
            // userId: this.props.navigation.state.params.receiverDetails._id,
            // username: this.props.navigation.state.params.receiverDetails.name,
            // userImage: this.props.navigation.state.params.receiverDetails.profilePicture,
            // chatRoomId: this.props.navigation.state.params.chatRoomId ? this.props.navigation.state.params.chatRoomId : '',
            chatList: [],
            typeMessage: '',
            IsCustomerSender: true,
            loader: false,
            token: ''
            // group:this.props.navigation.state.params.group

        }

        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);            
            this.state.chatRef = firebase.database().ref().child('chats');
        } else {
            this.state.chatRef = firebase.database().ref().child('chats');
        }

        this.state.chatRef.on('child_added', (snapshot) => {
            // console.warn('snapshot value',this.state.chatRoomId)
            const snapShotVal = snapshot.val();
            if (snapShotVal.roomId == this.state.chatRoomId) {
                let chatList = this.state.chatList;
                const item = snapShotVal;
                chatList.push(item);                
                this.setState({ typeMessage: '', chatList: chatList });
            }
            setTimeout(() => {
                if (this.refs && this.refs.scrollView) {
                    this.refs.scrollView.scrollToEnd(true);
                }
            }, 400);

        })

    }


    componentDidMount() {
        this.getStorageValue();
    }


    getStorageValue = () => {
        AsyncStorage.getItem('UserDetails', (err, result) => {            
            let userdata = JSON.parse(result);                         
            let roomId = '';
            this.setState({receiverId : userdata.adminDetails._id,token:userdata.token, 
                receiverImage: userdata.adminDetails.profilePicture, receiverName: userdata.adminDetails.name}, ()=>{
                // if (userdata.adminId < this.state.userId) {
                    roomId = userdata.adminDetails._id + "_" + userdata.data._id;
                // } else {
                //     roomId = this.state.userId + "_" + userdata.adminId;
                // }
            })
            

            //console.warn('roomId:',roomId)
            this.setState({
                senderId: userdata.data._id,
                senderImage: userdata.data.profilePicture,
                senderName: userdata.data.name,
                chatRoomId: roomId,
                chatList: this.state.chatList
            });
            

            this.state.chatRef.orderByChild('roomId').equalTo(this.state.chatRoomId).once('value').then((snapshot) => {
                if (snapshot.val()) {
                    var listMesage = [];
                    for (let key in snapshot.val()) {
                        listMesage.push(snapshot.val()[key]);
                    }
                    this.setState({
                        chatList: listMesage,
                        loader: false
                    });
                    // console.warn('chatlist:',this.state.chatList)
                }
            }).catch((Err) => {
                this.setState({
                    loader: false
                })
            })


        });

    }

    sendMessage() {            
        const nowDate = Date.now();
    //    console.warn (this.state.chatRoomId)

        if (this.state.typeMessage && this.state.typeMessage.trim()) {
            this.state.chatRef.push({
                "senderId": this.state.senderId,
                "senderName": this.state.senderName,
                "senderImage": this.state.senderImage,
                "Message": this.state.typeMessage,
                "receiverId": this.state.receiverId,
                "receiverName": this.state.receiverName,
                "receiverImage": this.state.receiverImage,
                "roomId": this.state.chatRoomId,
                "date": nowDate,
            });
            let inputData={
                userId:this.state.senderId,
                receiverId: this.state.receiverId,
                content:this.state.typeMessage,
               
              }
    
               api.POST('feedback-api/feedback/contactUs',inputData,this.state.token).then(Result => {                
                // console.log(Result, 'result')
              }).catch(err => {                
              })
        }
        else {
            Alert.alert('', 'Plesae type message to send.');
        }
    }

    render() {        
        return (
            <View style={[styles.homebody, { flex: 1 }]}>

                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <View style={[styles.topmenu, { paddingTop: 10 }]}>
                    <Spinner visible={this.state.loader}
                        textContent={'Loading...'}
                        textStyle={{ color: '#FFF' }} />
                    <View style={{ width: '100%', justifyContent: 'center' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ marginRight: 10 }}>
                                {
                                    this.state.receiverImage ? (
                                        <Avatar rounded source={{ uri: this.state.receiverImage.includes('/userImage')? imageUrl.img_url+this.state.receiverImage : this.state.receiverImage}} size="medium" />
                                    ) : (
                                            <Avatar rounded source={require("../../assets/images/user.png")} size="medium" />
                                        )
                                }

                            </View>
                            <View style={{width: '80%'}}>
                                <Text numberOfLines={1} style={{ color: 'white', fontFamily: 'Oswald-Bold', fontSize: 17, width:'90%' }}>{this.state.receiverName}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <ScrollView style= {{backgroundColor:'#fff'}} ref='scrollView'>
                    <View style={styles.chatbody}>
                        <Text style={{ color: '#9F9FA0', fontSize: 12, paddingLeft:15, paddingRight: 15, textAlign: 'center', marginBottom: 15 }}></Text>
                        {
                            this.state.chatList.map((data, key) => (

                                data.senderId == this.state.senderId ? (
                                    <View >
                                        <View style={styles.rightchat}>
                                            <Text style={styles.chatText}>{data.Message}</Text>
                                            <Text style={[styles.chatdate, styles.dateright]}>{moment(data.date).format('DD/MM/YYYY')}</Text>
                                        </View>
                                    </View>
                                ) :

                                    (
                                        <View>
                                            <View style={styles.leftchat}>
                                                <Text style={styles.chatText}>{data.Message}</Text>
                                                <Text style={[styles.chatdate, styles.dateright2]}>{moment(data.date).format('DD/MM/YYYY')}</Text>
                                            </View>
                                        </View>
                                    )

                            ))
                        }

                    </View>
                </ScrollView>
                <Footer style={styles.footerSec}>
                    <View style={{ flexDirection: 'row', alignItems: 'center',  }}>
                        <TextInput style={styles.chatInput} placeholder={'Type a message...'} onChangeText={(text) => this.setState({ typeMessage: text })} value={this.state.typeMessage} />
                        <TouchableOpacity style={styles.chatSend} onPress={() => this.sendMessage()} >
                            <Image source={require("../../assets/imsges/msg2.png")} style={{width: 23, height: 23}}/>
                        </TouchableOpacity>
                    </View>
                </Footer>
            </View>

        )

    }
}


export default Feedback;
