import React, { Component } from 'react';
import {Text, Image, View, ScrollView, StatusBar, TouchableOpacity, ImageBackground, TextInput, BackHandler, Alert } from "react-native";
//import { Container, Button, H3, Text, Footer } from "native-base";
import { Avatar, Badge, Icon} from 'react-native-elements';
//import Icon from 'react-native-vector-icons/Ionicons';
import { BASE_URL, firebaseConfig } from "../../config";
import styles from "./style";
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import Loader from "../../Loader";
import imageUrl from '../../Api/config';

class MessageDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            senderId: '',
            senderName: '',
            senderImage: '',
            userId: this.props.navigation.state.params.receiverid ? this.props.navigation.state.params.receiverid:"",
            username: this.props.navigation.state.params.receivername? this.props.navigation.state.params.receivername: "",
            userImage: this.props.navigation.state.params.receiverprofilePicture ? this.props.navigation.state.params.receiverprofilePicture:"",
            chatRoomId: this.props.navigation.state.params.chatRoomId ? this.props.navigation.state.params.chatRoomId : '',
            chatList: [],
            typeMessage: '',
            IsCustomerSender: true,
            loader: false,

        }

        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
            this.state.chatRef = firebase.database().ref().child('chatMessages');
        } else {
            this.state.chatRef = firebase.database().ref().child('chatMessages');
        }

        this.state.chatRef.on('child_added', (snapshot) => {
            //console.warn('snapshot value',snapshot.val())
            const snapShotVal = snapshot.val();
            if (snapShotVal.chatRoomId == this.state.chatRoomId) {
                let chatList = this.state.chatList;
                const item = snapShotVal;
                chatList.push(item);
                this.setState({ typeMessage: '', chatList: chatList });
            }
            setTimeout(() => {
                if (this.refs && this.refs.scrollView) {
                    this.refs.scrollView.scrollToEnd(true);
                }
            }, 400);

        })

    }


    componentDidMount() {
       this.getStorageValue();
    }

    getStorageValue = () => {
        
            let userdata = this.props.userInfo.userDetails;

            this.setState({
                senderId: userdata._id,
                senderImage: userdata.profilePicture,
                senderName: userdata.username,
                //chatRoomId: roomId,
                chatList: this.state.chatList
            });
            //console.warn('chatroomId:',this.state.chatRoomId);

            this.state.chatRef.orderByChild('chatRoomId').equalTo(this.state.chatRoomId).once('value').then((snapshot) => {
                if (snapshot.val()) {
                    var listMesage = [];
                    for (let key in snapshot.val()) {
                        listMesage.push(snapshot.val()[key]);
                    }
                    this.setState({
                        chatList: listMesage,
                        loader: false
                    });
                    // console.warn('chatlist:',this.state.chatList)
                }
            }).catch((Err) => {
                this.setState({
                    loader: false
                })
            })


    }

    sendMessage = () => {
        const nowDate = new Date().toLocaleDateString();
        if (this.state.typeMessage && this.state.typeMessage.trim()) {
            this.state.chatRef.push({
                "senderId": this.state.senderId,
                "senderName": this.state.senderName,
                "senderImage": this.state.senderImage,
                "Message": this.state.typeMessage,
                "userId": this.state.userId,
                "username": this.state.username,
                "userImage": this.state.userImage,
                "chatRoomId": this.state.chatRoomId,
                "date": nowDate,
            });
        }
        else {
            Alert.alert('', 'Plesae type message to send.');
        }
    }

    render() {
        if (this.state.loader) {
            return (
                // <ActivityIndicator size="large" style={{flex:1}}/>
                <Loader isLoading={true} loaderSize={'large'} size={70} />
            )
        }
        return (
            <View style={[styles.homebody, { flex: 1 }]}>

                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <View style={[styles.topmenu, { paddingTop: 30 }]}>
                <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                    <Icon name='arrowleft' type='antdesign' color='#fff' size={25} />
                </TouchableOpacity>
                    <TouchableOpacity style={{ width: '90%', justifyContent: 'center' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ marginRight: 10 }}>
                                {
                                    this.state.userImage ? (
                                        <Avatar rounded source={{ uri:  this.state.userImage.includes('/userImage')? imageUrl.img_url+this.state.userImage : this.state.userImage}} size="medium" />
                                    ) : (
                                            <Avatar rounded source={require("../../assets/images/logo.png")} size="medium" />
                                        )
                                }

                            </View>
                            <View>
                                <Text numberOfLines={1} style={{ color: 'white', fontFamily: 'Oswald-Bold', fontSize: 15, width:'100%' }}>{this.state.username}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <ScrollView ref='scrollView'>
                    <View style={styles.chatbody}>
                        <Text style={{ color: '#9F9FA0', fontSize: 12, paddingLeft:15, paddingRight: 15, textAlign: 'center', marginBottom: 15 }}></Text>
                        {
                            this.state.chatList.map((data, key) => (

                                data.senderId == this.state.senderId ? (
                                    <View >
                                        <View style={styles.rightchat}>
                                            <Text style={{ color: '#fff', fontSize: 14 }}>{data.Message}</Text>
                                            <Text style={[styles.chatdate, styles.dateright]}>{data.date}</Text>
                                        </View>
                                    </View>
                                ) :

                                    (
                                        <View>
                                            <View style={styles.leftchat}>
                                                <Text style={{ fontSize: 14, color: '#000' }}>{data.Message}</Text>
                                                <Text style={styles.chatdate}>{data.date}</Text>
                                            </View>
                                        </View>
                                    )

                            ))
                        }

                    </View>
                </ScrollView>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <TextInput style={{ width: '70%', height: 50, color: '#000' }} placeholder={'Type a message...'} onChangeText={text => this.setState({ typeMessage: text })} value={this.state.typeMessage} />
                        <TouchableOpacity style={{ maxWidth: '15%', width: 50 }} onPress={this.sendMessage} >
                            <Text style={{ color: '#3498db', fontSize: 15, fontWeight: '500' }}>Send</Text>
                        </TouchableOpacity>
                    </View>
            </View>

        )

    }
}

const mapStateToProps = (state) => {
    return {
        details: state.User,
        userInfo: state.UserInfo
    };
};

export default connect(mapStateToProps, null)(MessageDetails)
