import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, Platform, TouchableNativeFeedback, SafeAreaView ,Alert} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
// import { Input } from "react-native-elements"
import Input from "../../Components/floatingInput"
import { GlobalStyles } from "../../assets/StylesGlobal/GlobalStyle"
import style from './style';
import { ScrollView } from 'react-native-gesture-handler';
import Validator from '../../helper/validator';
import api from '../../Api/api'
import Spinner from 'react-native-loading-spinner-overlay';
export class OtpVerification extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fields: { verifyOTP: '' },
      email:this.props.navigation.state.params.email,
      errors: {},
      isDisabled:false,
      loader:false
    }
  }
  handleChange(value, name) {
    let fields = this.state.fields;
    fields[name] = value;
    this.setState({ fields });
    this.setState({ errors: Validator.validateForm(name, this.state.fields, this.state.errors) });
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ errors: Validator.validateForm(null, this.state.fields, this.state.errors) });
    if (this.state.errors.formIsValid) {
      this.setState({isDisabled:true,loader:true})
      let inputData,data;
      AsyncStorage.getItem('UserDetails', (err, result) => {
        // console.log('Result',result)
        //  data = JSON.parse(result);
        // console.log('data',data)
      });
        inputData={
          email:this.state.email,
          otp:this.state.fields.verifyOTP
        }
         api.post('user/otpVerify',inputData).then(Result => {
           this.setState({loader:false})
         if(Result.status == 200){
          Alert.alert(Result.msg)
          this.props.navigation.navigate('Trips')
         }else{
             Alert.alert(Result.msg)
             this.setState({ fields: {}, selectedValue: "",errors:{}, isDisabled:false });
         }    
      }).catch(err => {
        this.setState({loader:false})
      })
      let fields = {},
        errors = {};
      this.setState({ fields: fields });
      this.setState({ errors: errors });
    }
  }
  render() {
    let TouchableCmp = TouchableOpacity;
    if (Platform.OS === 'android' && Platform.Version >= 21)
      TouchableCmp = TouchableNativeFeedback;
    return (
      <SafeAreaView style={{ flex: 1, }}>
        <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={style.spinnerTextStyle}/>
          <ScrollView>
          <View style={style.container}>
            <View style={style.subContainer1}>
              <View style={style.subContainer2}>
                <Text style={style.containerTxt}>Otp Verification</Text>
              </View>
                <Input
                    label="Otp"
                    name="Otp"
                    keyboardType="number-pad"
                    //iconName="key"
                    value={this.state.fields.verifyOTP}
                    onChangeText={text => this.handleChange(text, 'verifyOTP')}
                    errorMessage={this.state.errors['verifyOTP']}
                    ref={ref => (this.customInput8 = ref)}
                    refInner="innerTextInput8"
                    onSubmitEditing={() =>
                        this.customInput8.refs.innerTextInput8.focus()
                    }
                    returnKeyType="next"
                    blurOnSubmit={false}
                />
            </View>
            <View style={{ alignItems: 'center' }}>
              <TouchableCmp disabled={this.state.isDisabled}
                background={TouchableNativeFeedback.Ripple(
                  "#fca600",
                )}
                onPress={this.handleSubmit}>
                <View
                  style={[this.state.isDisabled == false ? style.button : style.disabledButton]}>
                  <Text style={style.buttonTxt}>
                    Verify
                    </Text>
                </View>
              </TouchableCmp>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}

export default OtpVerification
