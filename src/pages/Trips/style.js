export default {
    smcontainer: {
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 10,
        backgroundColor: '#fff',
    },
    logoDiv: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        // width: '60%',
        // height: 120   
        width: 180,
        height: 80,
        resizeMode: 'cover',
        marginTop: 20,
        marginBottom: 15
    },
    btnContainer: {
        borderWidth: 2,
        borderColor: '#9a9a9a',
        borderRadius: 9,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 20,
        marginRight: 20
    },
    tabbtn: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 5,
        paddingBottom: 5,
        width: '50%'
    },
    bdrright: {
        borderRightWidth: 2,
        borderRightColor: '#9a9a9a',
    },
    tbtntxt: {
        fontSize: 18,
        color: '#777',
        fontWeight: '600',
        marginLeft: 10,
        fontFamily: 'Oswald-Medium'
    },
    listContainer: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
        backgroundColor: '#fff',
        paddingTop: 10
    },
    list: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 4,
        backgroundColor: '#fff',
        minHeight: 50,
        marginVertical: 8,
        borderRadius: 5,
        width: '96%',
        marginHorizontal: '2%'
    },
    listinner: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: '5%',
        paddingVertical: 10,
        width: '100%'
    },
    locationName: {
        flexDirection: 'row',
        alignItems: 'center',
        overflow: 'hidden',
    },
    ltext: {
        color: '#777',
        fontSize: 15,
        fontFamily: 'Oswald-Bold',
        // width:70
    },
    rtext: {
        color: '#777',
        fontSize: 15,
        fontFamily: 'Oswald-Bold',
        // width:70

    },
    micon: {
        paddingTop: 5,
        marginLeft: 10,
        marginRight: 10,
    },
    datetime: {
        color: '#999',
        fontSize: 14,
        fontFamily: 'Oswald-Bold'
    },
    rideStart: {
        color: '#377d14',
        fontSize: 15,
        fontFamily: 'Oswald-Bold'
    },
    lefttop: {
        width: '55%'
    },
    righttop: {
        flexDirection: 'row',
        alignItems: 'center',
        // width:'50%',
        // paddingLeft:15,
        // marginRight:10,
        justifyContent: 'space-between',
    },
    btnlike: {
        // padding:3,
        color: '#1b87ff',
        borderColor: '#1b87ff',
        borderWidth: 2,
        borderRadius: 4,
        textAlign: 'center',
        width: 70,
        fontSize: 16,
        fontFamily: 'Oswald-Medium',
    },
    btnlikey: {
        // padding:3,
        color: '#fbb122',
        borderColor: '#fbb122',
        borderWidth: 2,
        borderRadius: 4,
        textAlign: 'center',
        width: 70,
        fontSize: 16,
        fontFamily: 'Oswald-Medium',
    },
    price: {
        fontSize: 15,
        color: '#fbb122',
        // marginLeft:3,
        // marginRight:10,
        fontFamily: 'Oswald-Bold',
    },
    tripinfo: {
        borderTopWidth: 2,
        borderTopColor: '#f2f2f2',
        padding: 15
    },
    triplocation: {
        flexDirection: 'row',
    },
    justify: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '85%',
        marginTop: 3,
        marginBottom: 3
    },
    ltext2: {
        color: '#9a9a9a',
        fontSize: 15,
        fontFamily: 'Oswald-Bold',
        width: 300
    },
    rtext2: {
        color: '#9a9a9a',
        fontSize: 15,
        fontFamily: 'Oswald-Bold',
        width: 100
    },
    micon2: {
        marginTop: -5,
        marginRight: 10,
        width: 40
    },
    mainimg: {
        width: '100%',
        height: 140,
    },
    overimg: {
        width: 70,
        height: 70,
        padding: 10
    },
    Rateimg: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        paddingTop: 30,
        paddingBottom: 20

    },
    bottompart: {
        paddingBottom: 15,
        paddingTop: 15
    },
    idtext: {
        color: '#777',
        fontSize: 20,
        fontFamily: 'Oswald-Bold',
        textAlign: 'center',
        marginBottom: 20
    },
    ratebtn: {
        borderColor: '#418643',
        borderWidth: 2,
        padding: 10,
        width: "45%",
        borderRadius: 4,
        alignItems: 'center'

    },
    rateBtn: {
        borderColor: '#418643',
        borderWidth: 2,
        padding: 10,
        width: "50%",
        borderRadius: 4,
        alignItems: 'center'

    },
    disabledratebtn: {
        // backgroundColor: "#808080",
        borderColor: '#808080',
        borderWidth: 2,
        padding: 10,
        width: "50%",
        borderRadius: 4,
        alignItems: 'center'
    },
    ratebtntxt: {
        color: '#418643',
        fontSize: 16,
        textAlign: 'center',
        textTransform: 'uppercase',
        fontFamily: 'Oswald-Regular'
    },
    reportbtn: {
        borderColor: '#d44c52',
        borderWidth: 2,
        padding: 10,
        width: '45%',
        borderRadius: 4
    },
    disableReportbtn: {
        borderColor: '#808080',
        borderWidth: 2,
        padding: 10,
        width: '45%',
        borderRadius: 4
    },
    reportTxt: {
        color: '#d44c52',
        fontSize: 16,
        textAlign: 'center',
        textTransform: 'uppercase',
        fontFamily: 'Oswald-Bold'
    },
    justifybtn: {
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'space-between',
    },
    spinnerTextStyle: {
        color: '#FFF'
    },



    container: {
        justifyContent: "center"
    },
    subContainer1: {
        paddingHorizontal: 20,
        marginTop: "15%"
    },
    subContainer2: {
        marginBottom: '15%',
        alignItems: 'center'
    },
    containerTxt: {
        fontFamily: 'Oswald-Medium',
        fontSize: 40,
        color: '#3498db'
    },
    button: {
        backgroundColor: "#3498db",
        width: "80%",
        marginVertical: 50,
        borderRadius: 35,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        elevation: 6
    },
    buttonTxt: {
        color: '#418643',
        fontFamily: 'Oswald-Regular',
        fontSize: 24,
        alignItems: 'center',
    },
    disabledButton: {
        backgroundColor: "#808080",
        width: "70%",
        marginVertical: 50,
        borderRadius: 35,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        elevation: 3,
    },
    rideRatebutton: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 3,
        color: '#1b87ff',
        borderColor: '#1b87ff',
        borderWidth: 2,
        borderRadius: 4,
        width: 80,
        fontSize: 16,
        fontFamily: 'Oswald-Medium'
    },
    rideRatebuttonTxt: {
        color: 'black',
        fontFamily: 'Oswald-Bold',
        fontSize: 24
    },
    disabledRideRateButton: {
        padding: 3,
        color: '#1b87ff',
        borderColor: '#1b87ff',
        borderWidth: 2,
        borderRadius: 4,
        textAlign: 'center',
        width: 80,
        fontSize: 16,
        fontFamily: 'Oswald-Medium'
    },
    forgotPassword: {
        alignItem: "center",
        marginBottom: 10
    },
    forgotPasswordTxt: {
        color: "#f00",
        fontSize: 18,
        fontFamily: 'Oswald-Light'
    },
    signUpBtn: {
        alignItem: "center",
        flexDirection: 'row'
    },
    signUpBtnTxt: {
        color: "#3498db",
        fontSize: 18,
        fontFamily: 'Oswald-Regular'
    },
    signUpBtnTxt1: {
        color: "#3498db",
        fontSize: 18,
        fontFamily: 'Oswald-SemiBold'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    pastTripSec: {
        width: '70%',
        height: 70,
        flexDirection: 'row',
        paddingVertical: 10,
        borderRadius: 100,
        backgroundColor: '#fff',
        // elevation: 3
    },
    pastTripUser: {
        width: 50,
        height: 50,
        overflow: "hidden",
        borderRadius: 50 / 2,
        backgroundColor: '#000'
    },
    pastUserImg: {
        width: '100%',
        height: '100%'
    },
    modalBtn:{
        width: '40%',
        height: 35, borderRadius: 5, 
        justifyContent: 'center', 
        alignItems: 'center', 
        borderColor: '#0081E2', 
        borderWidth: 1
    }



}