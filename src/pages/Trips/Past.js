import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, Alert, ActivityIndicator} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigation } from 'react-navigation';
import { Avatar, Icon, Card } from 'react-native-elements'
import styles from './style'
import moment from 'moment';
import api from '../../Api/api';
import imageUrl from '../../Api/config';

class Past extends Component {
    constructor(props) {
        super(props)

        this.state = {
            show: false,
            token: '',
            userId: '',
            pastTrips: this.props.pastTrips ?  this.props.pastTrips : [],
            tripDetails:[],
            riderArray:[],
        }
    }

    componentDidMount() {        
        this.getTripList ();
        this.props.navigation.addListener("didFocus", () => {
            this.getTripList()             
        });
      
        this.props.navigation.addListener("didBlur", () => {
            this.getTripList();
        }); 
    }
  getTripList (){
    this.setState({show:false})
        AsyncStorage.getItem('UserDetails', (err, result) => {
            let data = JSON.parse(result);
            this.setState({
                token: data.token,
                userId: data.data._id
            });
            api.GET("trip-api/trip/list/" + this.state.userId, data.token).then(result => {
                if (result.status == 200) {
                    this.setState({ pastTrips: result.data.past });
                }
            }).catch(err => {
                this.setState({ loader: false })
                console.log('error', err)
            })
        });
    }
    showCar = (key, item) => {        
        this.setState(prevState => ({
            show: !prevState.show,
            loader: true,
        }));
        this.setState({ showCar: key })
        api.GET("trip-api/trip/"+ item._id, this.state.token).then(result => {            
            if (result.status == 200) {                                 
                // get rider array
                api.GET('trip-api/trip/riderList/'+item.tripNumber, this.state.token).then((riderRes) => {        
                    this.setState({ riderArray:riderRes.data, tripDetails: result.data, loader: false })
                }).catch((err) => {                
                console.log('error', err)
                });
            }
        }).catch(err => {
            this.setState({ loader: false, loader: true, })
            console.log('error', err)
        })
    }

    rateNavigation = (item) =>{
        if(item.tripType == 'Drive'){
            this.props.navigation.navigate('RatingRider',{selectedTripDetails:this.state.tripDetails,loggedUser:this.state.userId})
        }else{
            this.props.navigation.navigate('Rating',{selectedTripDetails:this.state.tripDetails,loggedUser:this.state.userId})
            
        }
        this.setState({ show: false})
    }

    reportNavigation = (item) =>{        
        if(item.tripType == 'Drive'){
            this.props.navigation.navigate('ReportRider',{selectedTripDetails:this.state.tripDetails,loggedUser:this.state.userId})
        } else{
            if (this.state.tripDetails.userDetails && this.state.tripDetails.userDetails.length>0){
                this.props.navigation.navigate('Report',{selectedTripDetails:this.state.tripDetails,loggedUser:this.state.userId})
            } else {
                Alert.alert("Details not found")
            }
        }
        this.setState({ show: false})            
    }

    renderViewCar = (key, item) => {          
        if (this.state.loader) {
            return (
                <ActivityIndicator size="large" style={{flex:1}}/>                
            )
        }   
         return (
            <View style={styles.tripinfo} id={key}>
                <View style={styles.triplocation}>
                    <Icon name='ios-arrow-round-down' containerStyle={styles.micon2} type='ionicon' size={76} color='#999' />
                    <View style={styles.rightinfo}>
                        <View style={styles.justify}>
                            <Text style={styles.ltext2} >{item.fromLocationName}</Text>
                            {/* <Text style={styles.rtext2}>{item.tripTime}</Text> */}
                        </View>
                        <View style={styles.justify}>
                            <Text style={styles.ltext2}>{item.toLocationName}</Text>
                            {/* <Text style={styles.rtext2}>21:00</Text> */}
                        </View>
                    </View>
                </View>
                {/* <Image source={require('../../assets/imsges/car.png')} resizeMode="contain" style={styles.mainimg} /> */}
               
                <View style={styles.justifybtn}>
                {this.state.tripDetails.userDetails?
                  this.state.tripDetails.userDetails.map((value,key)=>{
                      
                    return(
                        <>
                        
                    <TouchableOpacity id={key} onPress={() => this.props.navigation.navigate('DriverProfile',{"driverId":value.userId})}>
                        {value.userId.profilePicture?
                            <Avatar size="xlarge" rounded source={{uri:value.userId.profilePicture.includes('/userImage') ? imageUrl.img_url+value.userId.profilePicture : value.userId.profilePicture}}  style={styles.overimg} />
                            : <Avatar size="xlarge" rounded source={require("../../assets/images/user.png")} style={styles.overimg}  />
                        }
                    </TouchableOpacity> 
                        {this.state.riderArray &&  this.state.riderArray.length>0 ?                     
                            <View style={styles.pastTripSec}>                                                    
                               {this.state.riderArray.map((data,key)=>{     
                                //    console.log(data.userId)                              
                                return (
                                <>
                                {/* data.profilePicture.includes('/userImage')? */}
                                {data.userId !==this.state.userId &&
                                <TouchableOpacity style={styles.pastTripUser} onPress={()=> this.props.navigation.navigate ('DriverProfile', {"driverId": data.userId})}>
                                {data.profilePicture ? 
                                    <Image source={{ uri: data.profilePicture.includes('/userImage')? imageUrl.img_url+data.profilePicture : data.profilePicture}}  style={styles.pastUserImg}/>:
                                    <Image style={styles.pastUserImg} source={require("../../assets/imsges/avatar.png")} /> 
                                }
                                </TouchableOpacity>
                               }
                                </>
                            )})}
                            </View>
                        
                     : null }
                    </>

                      )
                  })
                  :null
                }
                   
             </View>
                
                <View style={styles.bottompart}>
                    <Text style={styles.idtext}>Trip Id: {item.tripNumber}</Text>

                    <View style={styles.justifybtn}>
                        <TouchableOpacity disabled={item.isRating} style={styles.ratebtn} onPress={() =>this.rateNavigation(item)}>
                            <Text style={styles.ratebtntxt}>{item.isRating ? 'Rated' : 'Leave Rating'}  </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.reportbtn} onPress={() =>this.reportNavigation(item)}>
                            <Text style={styles.reportTxt}> Report </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
    render() {   
                 
        return (
            <View>
                {this.state.pastTrips.length != 0 ?
                    this.state.pastTrips.map((item, key) => {
                        return (
                            <View style={styles.list}>
                                {item.tripType == "Drive" ?
                                    <TouchableOpacity style={styles.listinner} key={key} onPress={() => this.showCar(key, item)}>
                                        <View style={{...styles.lefttop, width: '52%', height: '100%',}}>
                                            <View style={{...styles.locationName, backgroundColor: '#0000', flexWrap: 'wrap'}}>
                                                <Text style={styles.ltext} numberOfLines={1}>{item.fromLocationName}</Text>
                                                <Icon name='ios-arrow-round-forward' containerStyle={styles.micon} type='ionicon' size={25} color='#777' />
                                                <Text style={styles.rtext} numberOfLines={1}>{item.toLocationName}</Text>
                                            </View>
                                            <Text style={styles.datetime}>{moment(item.tripDate).format("D MMM YYYY")}{" " + item.tripTime}</Text>
                                        </View>
                                        <View style={{...styles.righttop, width: '45%',}}>
                                            <Text style={styles.btnlike}>{item.tripType}</Text>
                                            {/* <Text style={styles.price}>£{item.driverAmount-item.bookingFee}</Text> */}
                                            <Text style={styles.price}>£{ item.amount }</Text>
                                        </View>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity style={styles.listinner} key={key} onPress={() => this.showCar(key, item)}>
                                        <View style={{...styles.lefttop, width: '52%', height: '100%',}}>
                                            <View style={{...styles.locationName, backgroundColor: '#0000', flexWrap: 'wrap'}}>
                                                <Text style={styles.ltext} numberOfLines={1}>{item.fromLocationName}</Text>
                                                <Icon name='ios-arrow-round-forward' containerStyle={styles.micon} type='ionicon' size={25} color='#777' />
                                                <Text style={styles.rtext} numberOfLines={1}>{item.toLocationName}</Text>
                                            </View>
                                            <Text style={styles.datetime}>{moment(item.tripDate).format("D MMM YYYY")}{" " + item.tripTime}</Text>
                                        </View>
                                        <View style={{...styles.righttop, width: '45%',}}>
                                            <Text style={styles.btnlikey}>{item.tripType}</Text>
                                            <Text style={styles.price}>£{ item.bookingFee ? (item.amount+item.bookingFee).toFixed(2) : 0 }</Text>
                                        </View>
                                    </TouchableOpacity>
                                }
                                {this.state.show && this.state.showCar == key ? this.renderViewCar(key, item) : null}
                            </View>

                        )
                       
                    })

                    : null}
            </View>
        )
    }
}

export default withNavigation(Past);
// export default Past

