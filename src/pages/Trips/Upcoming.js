import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, Dimensions, ScrollView, Alert, InteractionManager, Modal} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigation } from 'react-navigation';
import { Avatar, Icon, Card } from 'react-native-elements'
import styles from './style'
import api from '../../Api/api'
import Spinner from 'react-native-loading-spinner-overlay';
import {NavigationEvents} from 'react-navigation';
import moment from 'moment';
import * as firebase from 'firebase';
import imageUrl from '../../Api/config';


const deviceHeight = Dimensions.get("window").height;

// var firebaseConfig = {
//     apiKey: "AIzaSyBPn_p6JBWkIUSDzG0ke-0IGMNwrnfLpaY",
//     authDomain: "arkabs.firebaseapp.com",
//     databaseURL: "https://arkabs.firebaseio.com/",
//     projectId: "arkabs",
//     storageBucket: "arkabs.appspot.com",
//     messagingSenderId: "769927113864",
//   }
var firebaseConfig = {
    apiKey: "AIzaSyBT6pCnWjwSOoVknx-no1HlpuSg4JRkJkQ",
    authDomain: "arkabs-280713.firebaseapp.com",
    databaseURL: "https://arkabs-280713.firebaseio.com",
    projectId: "arkabs-280713",
    storageBucket: "arkabs-280713.appspot.com",
    messagingSenderId: "1031996217292",
    appId: "1:1031996217292:web:6a5415afdc702f2a356f69",
    measurementId: "G-MMZT0R9B7E"
  };
  
class Upcoming extends Component {
    constructor(props) {
        super(props)

        this.state = {
            show: false,
            token: '',
            userId: '',
            upcomingTripList: this.props.upcomingTrips ? this.props.upcomingTrips : [],
            currentTime:moment().utcOffset('+05:30').format('HH:mm'),
            tripDetails:[],
            chatRef: '',
            loader:false,
            showCancelModal: false,
            cancelItem:'',
            showDriverCancelModal: false,
            detailLoader : false,            
        }
        
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
        this.state.chatRef = firebase.database().ref().child('chatMessages');       
      } else {
        this.state.chatRef = firebase.database().ref().child('chatMessages');       
      }
    }

    componentDidMount() {
        this.getTripList ();
        this.props.navigation.addListener("didFocus", () => {
            this.getTripList()                        
          });
      
          this.props.navigation.addListener("didBlur", () => {
            this.getTripList();
          }); 
    }

    getTripList (){
        this.setState({show:false})
        AsyncStorage.getItem('UserDetails', (err, result) => {
            let data = JSON.parse(result);
            this.setState({
                token: data.token,
                userId: data.data._id
            });
            api.GET("trip-api/trip/list/" + this.state.userId, data.token).then(result => {
                if (result.status == 200) {
                    this.setState({ upcomingTripList: result.data.upcoming ,loader: false});
                }
            }).catch(err => {
                this.setState({ loader: false })
                console.log('error', err)
            })

        });
    }

    showCar = (key, item) => {
        this.setState(prevState => ({
            show: !prevState.show, loader: true
        }));
        this.setState({ showCar: key })
        api.GET("trip-api/trip/"+ item._id, this.state.token).then(result => {
            if (result.status == 200) {
                this.setState({  loader: false, tripDetails: result.data })
            }profileDetails
        }).catch(err => {
            this.setState({ loader: false })
            console.log('error', err)
        })
    }

    cancelNavigation = (item) =>{
        this.setState({showDriverCancelModal: true, cancelItem: item})
    }

    cancelNavigation2 = (item) =>{
        this.setState({showCancelModal: true, cancelItem: item})
    //     Alert.alert('', 'Are you sure you want to cancel? Repeated cancellations will be monitored and action will be taken; press confirm to agree', [
    //         { text: "Confirm", onPress: () =>this.confirmCancel(item) },
    //         { text: "Cancel", style:{color:'red'}  }
    //         ],
    //         { cancelable: true });
    }
    

    confirmCancel = () => {
        this.setState({
            loader:true, showCancelModal: false, showDriverCancelModal: false
          })
        api.PUT("trip-api/trip/cancel/trip/"+ this.state.cancelItem._id, this.state.token).then(result => {
            api.GET("trip-api/trip/list/" + this.state.userId, this.state.token).then(result => {
                if (result.status == 200) {
                    this.setState({ upcomingTripList: result.data.upcoming});
                    this.setState(prevState => ({
                        show: !prevState.show
                    }));
                }
            }).catch(err => {
                this.setState({ loader: false })
                console.log('error', err)
            })
            if(this.state.cancelItem.tripType != 'Drive'){
                this.state.chatRef.orderByChild('chatRoomId').once('value').then((snapShot) => {
                    if (snapShot.val()) {
                      for (let key in snapShot.val()) {
                        if((snapShot.val()[key].memberId == this.state.userId && snapShot.val()[key].adminId != this.state.userId)){
                           firebase.database().ref(`chatMessages/${key}`).remove()    
                        }
                      }
                    }
                  }).catch((err) => {
                     this.setState({
                      loader:false
                    })
                  })
            }else{
                this.state.chatRef.orderByChild('chatRoomId').equalTo(this.state.cancelItem.tripNumber).once('value').then((snapShot) => {
                    if (snapShot.val()) {
                       for (let key in snapShot.val()) {
                           firebase.database().ref(`chatMessages/${key}`).remove()    
                      }
                    }
                  }).catch((err) => {
                    this.setState({
                      loader:false
                    })
                  })
            }
           
            Alert.alert(result.msg)
            this.setState({ loader: false })
            this.props.navigation.navigate('Trips')
        }).catch(err => {
            this.setState({ loader: false })
            console.log('error', err)
        })
     
    }

    renderViewCar = (key, item) => { 
        return (
           <View style={styles.tripinfo} id={key}>
               <Spinner visible={this.state.detailLoader}
                    textContent={'Loading...'}
                    textStyle={{ color: '#FFF' }} />
               <View style={styles.triplocation}>
                   <Icon name='ios-arrow-round-down' containerStyle={styles.micon2} type='ionicon' size={76} color='#999' />
                   <View style={styles.rightinfo}>
                       <View style={styles.justify}>
                           <Text style={styles.ltext2} numberOfLines={2}>{item.fromLocationName}</Text>
                           {/* <Text style={styles.rtext2}>{item.tripTime}</Text> */}
                       </View>
                       <View style={styles.justify}>
                           <Text style={styles.ltext2} numberOfLines={2}>{item.toLocationName}</Text>
                           {/* <Text style={styles.rtext2}>21:00</Text> */}
                       </View>
                   </View>
               </View>
               {/* <Image source={require('../../assets/imsges/car.png')} resizeMode="contain" style={styles.mainimg} /> */}
              
               <View style={styles.justifybtn}>                   
               {this.state.tripDetails.userDetails?
                 this.state.tripDetails.userDetails.map((value,key)=>{                     
                   return(
                    <>
                    
                    <TouchableOpacity id={key} onPress={() => this.props.navigation.navigate('DriverProfile',{"driverId":value.userId._id})}>
                        {value.userId.profilePicture?
                            <Avatar size="xlarge" rounded source={{uri:value.userId.profilePicture.includes('/userImage') ? imageUrl.img_url+value.userId.profilePicture : value.userId.profilePicture}}  style={styles.overimg} />
                            : <Avatar size="xlarge" rounded source={require("../../assets/images/user.png")} style={styles.overimg}  />
                        }
                    </TouchableOpacity>                                                          
                    </>
                     )
                 })
                 :null
               }
                  
            </View>
               
               <View style={styles.bottompart}>
                   <Text style={styles.idtext}>Trip Id: {item.tripNumber}</Text>

                   <View style={styles.justifybtn}>
                       {item.tripType == 'Drive' ?
                       <TouchableOpacity style={styles.ratebtn} onPress={() =>this.cancelNavigation2(item)}>
                           <Text style={styles.ratebtntxt}> Cancel trip </Text>
                       </TouchableOpacity>
                       :
                       <TouchableOpacity style={styles.ratebtn} onPress={() =>this.cancelNavigation(item)}>
                           <Text style={styles.ratebtntxt}> Cancel trip </Text>
                       </TouchableOpacity>
                        }
                       {/* <TouchableOpacity style={styles.reportbtn} onPress={() =>this.reportNavigation(item)}>
                           <Text style={styles.reportTxt}> Report </Text>
                       </TouchableOpacity> */}
                   </View>
               </View>
           </View>
       )
   }


    render() {
        return (
            <View>
                {/* Cancel modal for RIDER ------start*/}
                <Modal transparent={true} visible={this.state.showCancelModal}>
                    <View style={{ backgroundColor: "#000000aa", flex: 1,justifyContent: 'center', }}>
                        <View style={{width: '80%', marginHorizontal: '10%',  backgroundColor: '#fff', paddingHorizontal: 20, paddingVertical: 25, borderRadius: 10,}}>
                        <Text style={{fontSize: 16, color: '#000', textAlign: 'center'}}>Are you sure you want to cancel? Repeated cancellations will be monitored and action will be taken; press confirm to agree</Text>
                        <View style={{width: '90%', marginHorizontal: '5%', flexDirection: 'row', marginTop: 50, justifyContent: 'space-between'}}>
                            <TouchableOpacity style={styles.modalBtn} onPress ={()=> this.setState({showCancelModal: false})}>
                            <Text style={{fontSize: 18, color: '#0081E2'}}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.modalBtn} onPress ={()=> this.confirmCancel()}>
                            <Text style={{fontSize: 18, color: '#0081E2'}}>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                        </View>
                    </View>
                </Modal>
                {/* Cancel modal for RIDER ------end*/}

                {/* Cancel modal for DRIVER ------start*/}
                <Modal transparent={true} visible={this.state.showDriverCancelModal}>
                    <View style={{ backgroundColor: "#000000aa", flex: 1,justifyContent: 'center', }}>
                        <View style={{width: '80%', marginHorizontal: '10%',  backgroundColor: '#fff', paddingHorizontal: 20, paddingVertical: 25, borderRadius: 10,}}>
                        <Text style={{fontSize: 16, color: '#000', textAlign: 'center'}}>You will be refunded fully minus any booking fees; press confirm to agree</Text>
                        <View style={{width: '90%', marginHorizontal: '5%', flexDirection: 'row', marginTop: 50, justifyContent: 'space-between'}}>
                            <TouchableOpacity style={styles.modalBtn} onPress ={()=> this.setState({showDriverCancelModal: false})}>
                            <Text style={{fontSize: 18, color: '#0081E2'}}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.modalBtn} onPress ={()=> this.confirmCancel()}>
                            <Text style={{fontSize: 18, color: '#0081E2'}}>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                        </View>
                    </View>
                </Modal>
                {/* Cancel modal for DRIVER ------end*/}



                <NavigationEvents onDidFocus={() =>
                    api.GET("trip-api/trip/list/" + this.state.userId, this.state.token).then(result => {
                        if (result.status == 200) {
                            this.setState({ upcomingTripList: result.data.upcoming});
                        }
                    }).catch(err => {
                        this.setState({ loader: false })
                        console.log('error', err)
                    })} />
                 <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={{ color: '#FFF' }} />
                {this.state.upcomingTripList.length != 0 ?
                    this.state.upcomingTripList.map((item, key) => {                        
                        return (
                            <View style={styles.list}>
                                {item.tripType == "Drive" ?
                                    <TouchableOpacity style={styles.listinner} key={key} onPress={() => this.showCar(key, item)}>
                                        <View style={{...styles.lefttop, width: '52%', height: '100%',}}>
                                            <View style={{...styles.locationName, backgroundColor: '#0000', flexWrap: 'wrap'}}>
                                                <Text style={styles.ltext} numberOfLines={1}>{item.fromLocationName}</Text>
                                                <Icon name='ios-arrow-round-forward' containerStyle={styles.micon} type='ionicon' size={25} color='#777' />
                                                <Text style={styles.rtext} numberOfLines={1}>{item.toLocationName}</Text>
                                            </View>
                                            <Text style={styles.datetime}>{moment(item.tripDate).format("D MMM YYYY")}{" " + item.tripTime}</Text>
                                            {item.tripTime === this.state.currentTime ?
                                            <Text style={styles.rideStart}>Ride Started</Text>
                                            :null}
                                        </View>
                                        <View style={{...styles.righttop, width: '45%',}}>
                                            <Text style={styles.btnlike}>{item.tripType}</Text>
                                            {/* <Text style={styles.price}>£{ item.driverAmount-item.bookingFee }</Text> */}
                                            <Text style={styles.price}>£{ item.amount }</Text>
                                        </View>
                                    </TouchableOpacity>
                                    : <TouchableOpacity style={styles.listinner} key={key} onPress={() => this.showCar(key, item)}>
                                        <View style={{...styles.lefttop, width: '52%', height: '100%',}}>
                                            <View style={{...styles.locationName, backgroundColor: '#0000', flexWrap: 'wrap'}}>
                                                <Text style={styles.ltext} numberOfLines={1}>{item.fromLocationName}</Text>
                                                <Icon name='ios-arrow-round-forward' containerStyle={styles.micon} type='ionicon' size={25} color='#777' />
                                                <Text style={styles.rtext} numberOfLines={1}>{item.toLocationName}</Text>
                                            </View>
                                            <Text style={styles.datetime}>{moment(item.tripDate).format("D MMM YYYY")}{" " + item.tripTime}</Text>
                                            {item.tripTime === this.state.currentTime ?
                                            <Text style={styles.rideStart}>Ride Started</Text>
                                            :null}
                                            
                                        </View>
                                        <View style={{...styles.righttop, width: '45%',}}>
                                            <Text style={styles.btnlikey}>{item.tripType}</Text>
                                            {/* <Text style={styles.price}>£{Math.round(item.amount+item.bookingFee).toFixed(2)}</Text> */}
                                            <Text style={styles.price}>£{ (item.amount+item.bookingFee).toFixed(2) }</Text>
                                            
                                        </View>
                                      </TouchableOpacity>
                                }
                             {this.state.show && this.state.showCar == key ? this.renderViewCar(key, item) : null}
                            </View>
                        )
                    })

                    : null
                }

            </View>
        )
    }
}

export default withNavigation(Upcoming);

