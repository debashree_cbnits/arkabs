import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, Platform, TouchableNativeFeedback, SafeAreaView, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
// import { Input } from "react-native-elements"
import Input from "../../Components/floatingInput"
import { GlobalStyles } from "../../assets/StylesGlobal/GlobalStyle"
import style from './style';
import { ScrollView } from 'react-native-gesture-handler';
import Validator from '../../helper/validator';
import api from '../../Api/api';
import { connect } from "react-redux"
import { changeAuthState } from "../../Redux/AuthReducer/Reducer";
import Spinner from 'react-native-loading-spinner-overlay';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Avatar } from 'react-native-elements';
import imageUrl from '../../Api/config';

export class Rating extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comment: '',
            rating: 0,
            showrating: 0,
            userId: "",
            tripId : this.props.navigation.state.params.selectedTripDetails.tripDetails._id,
            receiverId: this.props.navigation.state.params.selectedTripDetails.userDetails[0]._id,
            token: "",
            isDisabled: false,
            loader: false,
            totalStar: [{"value" : 1, "select" : 0}, 
                        {"value" : 2, "select" : 0}, 
                        {"value" : 3, "select" : 0},
                        {"value" : 4, "select" : 0},
                        {"value" : 5, "select" : 0}
                    ],
            userProfileImage: this.props.navigation.state.params.selectedTripDetails.userDetails[0].userId.profilePicture
        }
    }
    componentDidMount() {        
        AsyncStorage.getItem('UserDetails').then(res => {
            if (res) {
                let userDetails = JSON.parse(res);
                this.setState({
                    userId: userDetails.data._id,
                    token: userDetails.token
                })
            }
        }).catch = (err) => {
            console.log(err)
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();

        this.setState({ isDisabled: true, loader: true });
        let inputData = {
            rating: this.state.rating,
            comment: this.state.comment,
            senderId: this.state.userId,
            receiverId: this.state.receiverId,
            tripId : this.state.tripId
        }

        api.POST('/rating-api/rating/', inputData, this.state.token).then(Result => {
            this.setState({ loader: false });
            if (Result.status == 200) {
                Alert.alert(Result.msg)
                this.props.navigation.navigate('Trips')
            } else {
                Alert.alert(Result.msg)
                this.setState({ isDisabled: false });
            }
        }).catch(err => {
            this.setState({ loader: false });
        })

    }
    setRating = (value, key) => {
       let newArray = [{"value" : 1, "select" : 0}, 
        {"value" : 2, "select" : 0}, 
        {"value" : 3, "select" : 0},
        {"value" : 4, "select" : 0},
        {"value" : 5, "select" : 0}
        ] 
        for(let i = 0; i < value; i++){                
            newArray[i].select = 1                          
        } 
        this.setState({ rating: value, showrating: key, totalStar:newArray})
    }
    render() {       
        let TouchableCmp = TouchableOpacity;
        if (Platform.OS === 'android' && Platform.Version >= 21)
            TouchableCmp = TouchableNativeFeedback;
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={style.spinnerTextStyle} />
                <ScrollView>
                    <View style={style.container}>
                        <View style={style.subContainer1}>
                            <View style={style.subContainer2}>
                                <Text style={style.containerTxt}>Rate</Text>
                            </View>
                            <View style={style.Rateimg} >
                                {this.state.userProfileImage ?
                                    <Avatar size="xlarge" rounded source={{ uri: this.state.userProfileImage.includes('/userImage')? imageUrl.img_url+this.state.userProfileImage : this.state.userProfileImage }} />
                                    : <Avatar size="xlarge" rounded source={require('../../assets/imsges/car.png')} />
                                }
                            </View>
                            <View style={{ paddingTop: 20, width: '100%', alignItems:'center' }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    {this.state.totalStar.map((value, key) => {
                                        return (                                                                                
                                            <TouchableOpacity onPress={() => this.setRating(value.value, key)}>
                                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                                    {/* <Text style={{ paddingRight: 5, color: 'rg                console.warn (ratingArray)console.warn (ratingArray)ba(255, 152, 0, 1)', lineHeight: 12 }}>{value}</Text> */}
                                                    {
                                                        value.select == 1? (                                                           
                                                            < Ionicons id={key} size={30} name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />                                                                                                                                                                      
                                                        ) :
                                                            (
                                                            < Ionicons size={30} name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                            )
                                                    }
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    })}
                                    
                                    
                                    

                                </View>

                            </View>

                            <View>
                                <Input
                                    label="Comment"
                                    name="Comment"
                                    value={this.state.comment}
                                    onChangeText={text => this.setState({ comment: text })}
                                    ref={ref => (this.customInput4 = ref)}
                                    refInner="innerTextInput4"
                                    onSubmitEditing={() =>
                                        this.customInput5.refs.innerTextInput5.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                />
                            </View>
                        </View>
                        <View style={{ alignItems: 'center',paddingBottom:20 }}>
                            <TouchableCmp disabled={this.state.isDisabled}
                                // background={TouchableNativeFeedback.Ripple(
                                //     "#fca600",
                                // )}
                                onPress={this.handleSubmit}>
                                <View
                                    style={[this.state.isDisabled == false ? style.rateBtn : style.disabledratebtn]}>
                                    <Text style={style.buttonTxt}>
                                        Leave Rating
                                     </Text>
                                </View>
                            </TouchableCmp>
                        </View>
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default Rating
