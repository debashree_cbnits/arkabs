import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, Platform, TouchableNativeFeedback, SafeAreaView, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
// import { Input } from "react-native-elements"
import Input from "../../Components/floatingInput"
import { GlobalStyles } from "../../assets/StylesGlobal/GlobalStyle"
import style from './style';
import { ScrollView } from 'react-native-gesture-handler';
import Validator from '../../helper/validator';
import api from '../../Api/api';
import { connect } from "react-redux"
import { changeAuthState } from "../../Redux/AuthReducer/Reducer";
import Spinner from 'react-native-loading-spinner-overlay';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Avatar } from 'react-native-elements';
import imageUrl from '../../Api/config';

export class ReportRider extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comment: '',
            userId: this.props.navigation.state.params.loggedUser,
            showComment: 0,
            toId: '',
            token: "",
            tripId: this.props.navigation.state.params.selectedTripDetails.tripDetails._id,
            isDisabled: false,
            loader: false,
            ridersDetails: this.props.navigation.state.params.selectedTripDetails.userDetails,
            tripType: this.props.navigation.state.params.selectedTripDetails.tripDetails.tripType
        }
      
    }
    componentDidMount() {
        AsyncStorage.getItem('UserDetails').then(res => {
            if (res) {
                let userDetails = JSON.parse(res);
                this.setState({ token: userDetails.token });
            }
        }).catch = (err) => {
            console.log(err)
        }
    }

    setComment = (text, key, userId) => {
        this.setState({
            showComment: key,
            comment: text,
            toId:userId._id
        });
    }

    handleSubmit = () => {
        // e.preventDefault();
        this.setState({ isDisabled: true, loader: true });
        let inputData = {
            comment: this.state.comment,
            fromId: this.state.userId,
            toId: this.state.userId
        }
        api.POST('rating-api/report/',inputData,this.state.token).then(Result => { 
            this.setState({loader:false});
           if(Result.status == 200){
            Alert.alert(Result.msg)
            this.props.navigation.navigate('Trips')
           }else{
               Alert.alert(Result.msg)
             this.setState({ isDisabled:false });
           }
          }).catch(err => {
            this.setState({ loader:false });
          })

    }

    render() {
        let TouchableCmp = TouchableOpacity;
        if (Platform.OS === 'android' && Platform.Version >= 21)
            TouchableCmp = TouchableNativeFeedback;
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={style.spinnerTextStyle} />
                {/* aspectRatio:1, */}
                <ScrollView>
                    <View style={style.container}>
                        <View style={style.subContainer1}>
                            <View style={style.subContainer2}>
                                <Text style={style.containerTxt}>Report Rider</Text>
                            </View>
                            {this.state.ridersDetails.length > 0 ?
                                this.state.ridersDetails.map((item, key) => {
                                    return (
                                        <View>
                                            {item.userId.profilePicture ?
                                                <Avatar size="xlarge" rounded source={{ uri: item.userId.profilePicture.includes('/userImage')? imageUrl.img_url+item.userId.profilePicture : item.userId.profilePicture}} style={style.overimg} />
                                                : <Avatar size="xlarge" rounded source={require("../../assets/images/user.png")} style={style.overimg} />

                                            }
                                            <Input
                                                label="Comment"
                                                name="Comment"
                                                value={this.state.showComment == key?this.state.comment:''}
                                                onChangeText={(text) => this.setComment(text, key, item.userId)}
                                                ref={ref => (this.customInput4 = ref)}
                                                refInner="innerTextInput4"
                                                onSubmitEditing={() =>
                                                    this.customInput5.refs.innerTextInput5.focus()
                                                }
                                                returnKeyType="next"
                                                blurOnSubmit={false}
                                            />

                                            <TouchableCmp disabled={this.state.isDisabled}
                                                onPress={this.handleSubmit}>
                                                <View style={style.righttop}>
                                                    <Text style={style.btnlike}>
                                                        send
                                                    </Text>
                                                </View>
                                            </TouchableCmp>

                                        </View>
                                    )
                                })
                                : <View style={style.subContainer2}>
                                    <Text style={{ fontFamily: 'Oswald-Medium', fontSize: 30, color: 'grey' }}>No Rider</Text>
                                </View>
                            }
                        </View>

                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default ReportRider
