import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, Alert  } from 'react-native'
import { Avatar, Icon, Card } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style'
import Upcoming from "./Upcoming"
import Past from './Past'
import {colors} from "../../assets/StylesGlobal/globalColors"
import api from "../../Api/api";

// import PushNotificationIOS from "@react-native-community/push-notification-ios";


export default class Trips extends Component {
    constructor(props) {
        super(props)
        this.state = {
             Tab:"upcoming",
             upcomingBtn:colors.appBlue,
             pastBtn:"#777",
             token:'',
             userId:'',
             upcoming:[],
             past:[]
        }
      
    }

    componentDidMount(){  
        
        this.getTripList()
        this.props.navigation.addListener("didFocus", () => {
            this.getTripList()
          });
      
          this.props.navigation.addListener("didBlur", () => {
            this.getTripList();
          });       
    }
    getTripList (){
        AsyncStorage.getItem('UserDetails', (err, result) => {
            let data=JSON.parse(result);
            this.setState({
                token:data.token,
                userId:data.data._id
            });
           
            api.GET("trip-api/trip/list/"+this.state.userId,this.state.token).then(result=>{           
                if(result.status == 200){
                    this.setState({upcoming:result.data.upcoming,
                                    past:result.data.past});
              
                }
            }).catch(err => {
                this.setState({loader:false})
               console.log('error',err)
			})        

        });
    }
    
    render(){ 
        
        return (
            <View style={{flex:1, backgroundColor:'#fff'}}>
                <View style={styles.smcontainer}>
                    <View style={styles.logoDiv}>
                        <Image source={require("../../assets/imsges/logo.png")} style={styles.logo} />
                    </View>
                    <View style={styles.btnContainer}>
                        <TouchableOpacity style={[styles.tabbtn, styles.bdrright]} onPress={()=>this.setState({Tab:"upcoming",upcomingBtn:colors.appBlue,pastBtn:"#777"})}>
                        <Icon  name='md-navigate'  type='ionicon' size={24} color={this.state.upcomingBtn} />
                            <Text style={{...styles.tbtntxt,color:`${this.state.upcomingBtn}`}}> 
                                    Upcoming
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabbtn} onPress={()=>this.setState({Tab:"Past",pastBtn:colors.appBlue,upcomingBtn:"#777"})}>
                        <Icon  name='ios-checkmark-circle-outline'  type='ionicon' size={24} color={this.state.pastBtn} />
                            <Text style={{...styles.tbtntxt,color:`${this.state.pastBtn}`}}> 
                                    Past
                                </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            <ScrollView>               
               <View>
                    {
                        (this.state.Tab === "upcoming") ? <Upcoming upcomingTrips={this.state.upcoming}/> : <Past pastTrips={this.state.past} />
                    }
                    </View>
               </ScrollView>
            </View>
        )
    }
}


