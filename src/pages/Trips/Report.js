import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, Platform, TouchableNativeFeedback, SafeAreaView, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
// import { Input } from "react-native-elements"
import Input from "../../Components/floatingInput"
import { GlobalStyles } from "../../assets/StylesGlobal/GlobalStyle"
import style from './style';
import { ScrollView } from 'react-native-gesture-handler';
import Validator from '../../helper/validator';
import api from '../../Api/api';
import { connect } from "react-redux"
import { changeAuthState } from "../../Redux/AuthReducer/Reducer";
import Spinner from 'react-native-loading-spinner-overlay';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Avatar } from 'react-native-elements';
import imageUrl from '../../Api/config';


export class Report extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comment: '',
            userId: "",
            token: "",
            toId: this.props.navigation.state.params.selectedTripDetails.userDetails[0]._id,
            userProfileImage: this.props.navigation.state.params.selectedTripDetails.userDetails[0].userId.profilePicture,
            isDisabled: false,
            loader: false,
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('UserDetails').then(res => {
            if (res) {
                let userDetails = JSON.parse(res);
                this.setState({
                    userId: userDetails.data._id,
                    token: userDetails.token
                });

            }
        }).catch = (err) => {
            console.log(err)
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({ isDisabled: true, loader: true });
        let inputData = {
            comment: this.state.comment,
            fromId: this.state.userId,
            toId: this.state.toId
        }
        api.POST('rating-api/report/', inputData, this.state.token).then(Result => {
            this.setState({ loader: false });
            if (Result.status == 200) {
                Alert.alert(Result.msg)
                this.props.navigation.navigate('Trips')
            } else {
                Alert.alert(Result.msg)
                this.setState({ isDisabled: false });
            }
        }).catch(err => {
            this.setState({ loader: false });
        })

    }

    render() {
        let TouchableCmp = TouchableOpacity;
        if (Platform.OS === 'android' && Platform.Version >= 21)
            TouchableCmp = TouchableNativeFeedback;
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={style.spinnerTextStyle} />
                {/* aspectRatio:1, */}
                <ScrollView>
                    <View style={style.container}>
                        <View style={style.subContainer1}>
                            <View style={style.subContainer2}>
                                <Text style={style.containerTxt}>Report</Text>
                            </View>
                            <View style={style.Rateimg} >
                                {this.state.userProfileImage ?
                                    <Avatar size="xlarge" rounded source={{ uri: this.state.userProfileImage.includes('/userImage')? imageUrl.img_url+this.state.userProfileImage : this.state.userProfileImage }} />
                                    : <Avatar size="xlarge" rounded source={require('../../assets/imsges/car.png')} />
                                }
                            </View>
                            <View>
                                <Input
                                    label="Comment"
                                    name="Comment"
                                    value={this.state.comment}
                                    onChangeText={text => this.setState({ comment: text })}
                                    ref={ref => (this.customInput4 = ref)}
                                    refInner="innerTextInput4"
                                    onSubmitEditing={() =>
                                        this.customInput5.refs.innerTextInput5.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                />
                            </View>
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <TouchableCmp disabled={this.state.isDisabled}
                                // background={TouchableNativeFeedback.Ripple(
                                //     "#fca600",
                                // )}
                                onPress={this.handleSubmit}>
                                <View
                                    style={[this.state.isDisabled == false ? style.reportbtn : style.disableReportbtn]}>
                                    <Text style={style.reportTxt}>
                                        Report
                                     </Text>
                                </View>
                            </TouchableCmp>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default Report
