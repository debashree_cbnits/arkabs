import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, Platform, TouchableNativeFeedback, SafeAreaView, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
// import { Input } from "react-native-elements"
import Input from "../../Components/floatingInput"
import { GlobalStyles } from "../../assets/StylesGlobal/GlobalStyle"
import style from './style';
import { ScrollView } from 'react-native-gesture-handler';
import Validator from '../../helper/validator';
import api from '../../Api/api';
import { connect } from "react-redux"
import { changeAuthState } from "../../Redux/AuthReducer/Reducer";
import Spinner from 'react-native-loading-spinner-overlay';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Avatar } from 'react-native-elements';
import imageUrl from '../../Api/config';

export class RatingRider extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comment: '',
            showCommnnt: 0,
            rating: 0,
            showrating: 0,
            userId: this.props.navigation.state.params.loggedUser,
            receiverId: '',
            token: "",
            isDisabled: false,
            loader: false,
            totalStar: [1, 2, 3, 4, 5],
            ridersDetails: this.props.navigation.state.params.selectedTripDetails.userDetails,
            tripType: this.props.navigation.state.params.selectedTripDetails.tripDetails.tripType
        }

    }
    componentDidMount() {
        AsyncStorage.getItem('UserDetails').then(res => {
            if (res) {
                let userDetails = JSON.parse(res);
                this.setState({
                    token: userDetails.token,
                    userId: userDetails.data._id
                })
            }
        }).catch = (err) => {
            console.log(err)
        }
    }
    giveRate = (rate, key, item) => {
        this.setState({
            rating: rate,
            showrating: key,
            comment: 'great'
        });
        this.handleSubmit(rate, 'nice', item)
    }
    handleSubmit = (rate, comment, id) => {
        this.setState({ isDisabled: true, loader: true });
        let inputData = {
            rating: rate,
            comment: comment,
            senderId: this.state.userId,
            receiverId: id._id
        }

        api.POST('rating-api/rating', inputData, this.state.token).then(Result => {
           this.setState({ loader: false });
            if (Result.status == 200) {
                Alert.alert(Result.msg)
                this.props.navigation.navigate('Trips')
            } else {
                Alert.alert(Result.msg)
                this.setState({ isDisabled: false });
            }
        }).catch(err => {
            this.setState({ loader: false });
        })

    }

    render() {
        let TouchableCmp = TouchableOpacity;
        if (Platform.OS === 'android' && Platform.Version >= 21)
            TouchableCmp = TouchableNativeFeedback;
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={style.spinnerTextStyle} />
                {/* aspectRatio:1, */}
                <ScrollView>
                    <View style={style.container}>
                        <View style={style.subContainer1}>
                            <View style={style.subContainer2}>
                                <Text style={style.containerTxt}>Rate Riders</Text>
                            </View>
                            {this.state.ridersDetails.length > 0 ?
                                this.state.ridersDetails.map((item, key) => {
                                    return (
                                        <View id={key}>
                                            <View style={{ paddingTop: 20, width: '100%', marginLeft: 20, flexDirection: 'row' }}>
                                                {item.userId.profilePicture ?
                                                    <Avatar size="xlarge" rounded source={{ uri: item.userId.profilePicture.includes('/userImage')? imageUrl.img_url+item.userId.profilePicture : item.userId.profilePicture}} style={style.overimg} />
                                                    : <Avatar size="xlarge" rounded source={require("../../assets/images/user.png")} style={style.overimg} />

                                                }
                                                <View style={{ flex: 1, justifyContent: 'space-between', width: 250, flexDirection: 'row', padding: 10 }}>
                                                    <TouchableOpacity onPress={() => this.giveRate(1, key, item.userId)}>
                                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                                            <Text style={{ paddingRight: 5, color: 'rgba(255, 152, 0, 1)' }}> 1</Text>
                                                            {
                                                                this.state.showrating == key && this.state.rating == 1 ? (
                                                                    < Ionicons name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                                ) :
                                                                    (
                                                                        < Ionicons name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                                    )
                                                            }

                                                        </View>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.giveRate(2, key, item.userId)}>
                                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                                            <Text style={{ paddingRight: 5, color: 'rgba(255, 152, 0, 1)' }}> 2</Text>
                                                            {
                                                                this.state.showrating == key && this.state.rating == 2 ? (
                                                                    < Ionicons name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                                ) :
                                                                    (
                                                                        < Ionicons name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                                    )
                                                            }
                                                        </View>

                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.giveRate(3, key, item.userId)}>
                                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                                            <Text style={{ paddingRight: 5, color: 'rgba(255, 152, 0, 1)' }}> 3</Text>
                                                            {
                                                                this.state.showrating == key && this.state.rating == 3 ? (
                                                                    < Ionicons name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                                ) :
                                                                    (
                                                                        < Ionicons name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                                    )
                                                            }
                                                        </View>

                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.giveRate(4, key, item.userId)}>
                                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                                            <Text style={{ paddingRight: 5, color: 'rgba(255, 152, 0, 1)' }}> 4</Text>
                                                            {
                                                                this.state.showrating == key && this.state.rating == 4 ? (
                                                                    < Ionicons name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                                ) :
                                                                    (
                                                                        < Ionicons name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                                    )
                                                            }
                                                        </View>

                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.giveRate(5, key, item.userId)}>
                                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingTop: 5, paddingBottom: 5, paddingRight: 10, paddingLeft: 5 }}>
                                                            <Text style={{ paddingRight: 5, color: 'rgba(255, 152, 0, 1)' }}> 5</Text>
                                                            {
                                                                this.state.showrating == key && this.state.rating == 5 ? (
                                                                    < Ionicons name="ios-star" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                                ) :
                                                                    (
                                                                        < Ionicons name="ios-star-outline" style={{ color: 'rgba(255, 152, 0, 1)', }} />
                                                                    )
                                                            }
                                                        </View>

                                                    </TouchableOpacity>
                                                </View>

                                            </View>

                                            {/* <View>
                                        <Input
                                            label="Comment"
                                            name="Comment"
                                            value={this.state.showCommnnt == key? this.state.comment:''}
                                            onChangeText={text => this.setState({ comment: text ,showCommnnt:key,receiverId:this.state.showCommnnt==key?item.userId._id:''})}
                                            ref={ref => (this.customInput4 = ref)}
                                            refInner="innerTextInput4"
                                            onSubmitEditing={() =>
                                                this.customInput5.refs.innerTextInput4.focus()
                                            }
                                            returnKeyType="next"
                                            blurOnSubmit={false}
                                        /> */}

                                            {/* <TouchableCmp disabled={this.state.showCommnnt == key && this.state.showrating == key?false:true}
                                            onPress={this.state.showCommnnt == key && this.state.showrating == key? this.handleSubmit(this.state.showrating):''}>
                                            <View style={style.righttop}>
                                                <Text style={style.btnlike}>
                                                    send
                                         </Text>
                                            </View>
                                        </TouchableCmp> */}

                                            {/* </View> */}

                                        </View>
                                    )
                                })
                                : <View style={style.subContainer2}>
                                    <Text style={{ fontFamily: 'Oswald-Medium', fontSize: 30, color: 'grey' }}>No Rider</Text>
                                </View>
                            }


                        </View>

                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}


export default RatingRider
