export default {
    container: {
        paddingHorizontal: 20,
        marginTop: "15%",
        backgroundColor: '#fff'
    },
    subContainer1: {
        marginBottom: '10%',
        alignItems: 'center'
    },
    subContainer2: {
        fontFamily: 'Oswald-SemiBold',
        fontSize: 40,
        color: '#3498db'
    },
    picker: {
        width: "90%",
        height: 30
    },
    picker2: {
        width: "100%",
        height: 45,
    },
    datePicker: {
        dateIcon: {
            position: 'absolute',
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            marginLeft: 0
        },
        dateInput: {
            width: "100%",
            height: 35,
            marginLeft: 10,
        },
        placeholderText: {
            fontSize: 15,
            color: '#aaa',
        }
    },
    datePicker2: {
        dateIcon: {
            position: 'absolute',
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            marginLeft: 0,
        },
        dateInput: {
            width: '100%',
            height: 45,
            marginLeft: 0,
            borderRadius: 5,
        },
        placeholderText: {
            fontSize: 15,
            color: '#aaa',
        },
    },
    datePickerContainer: {
        width: 200,
        paddingBottom: 10
    },
    datePickerContainer2: {
        width: '100%',
        paddingBottom: 10
    },
    button: {
        backgroundColor: '#0081E2',
        width: '80%',
        marginVertical: 50,
        borderRadius: 10,
        paddingTop: 5,
        paddingBottom: 10,
        alignItems: 'center',
        elevation: 3
    },
    buttonTxt: {
        color: 'white',
        fontFamily: 'Oswald-Bold',
        fontSize: 22
    },
    disabledButton: {
        backgroundColor: "#808080",
        width: '80%',
        marginVertical: 50,
        borderRadius: 10,
        paddingTop: 5,
        paddingBottom: 10,
        alignItems: 'center',
        elevation: 3
    },
    spinnerTextStyle: {
        color: '#FFF'
    },

    signUpBtn: {
        alignItem: "center",
        flexDirection: 'row'
    },
    signUpBtnTxt: {
        color: "#3498db",
        fontSize: 18,
        fontFamily: 'Oswald-Regular'
    },
    signUpBtnTxt1: {
        color: "#3498db",
        fontSize: 18,
        fontFamily: 'Oswald-SemiBold'
    },
}