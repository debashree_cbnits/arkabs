import React, { Component } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, TouchableNativeFeedback, Platform, KeyboardAvoidingView, Picker, Alert } from 'react-native'
import Input from "../../Components/floatingInput"
import AsyncStorage from '@react-native-community/async-storage';
import { GlobalStyles } from '../../assets/StylesGlobal/GlobalStyle';
import style from "./style"
import { ScrollView } from 'react-native-gesture-handler'
import Validator from '../../helper/validator';
import api from '../../Api/api'
import DatePicker from 'react-native-datepicker';
import { connect } from "react-redux"
import { changeAuthState } from "../../Redux/AuthReducer/Reducer";
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';

export class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {
                first_name: '',
                last_name: '',
                email: '',
                phone: '',
                password: '',
                con_password: '',
                dateOfBirth: ''
            },
            User: {},
            selectedValue: "",
            errors: {},
            dataSource: [],
            isDisabled: false,
            loader: false
        };
    }
    componentDidMount() {
        api.get('getUniversity').then(responseJson => {
            this.setState({ dataSource: responseJson.data });
        }).catch(err => { })
    }

    // field/ form validation ---start
    handleChange(value, name) {
        let fields = this.state.fields;
        fields[name] = value;
        this.setState({ fields });
        this.setState({ errors: Validator.validateForm(name, this.state.fields, this.state.errors) });
    }
    // field/ form validation ---end


    // sign up function---start

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({ errors: Validator.validateForm(null, this.state.fields, this.state.errors) });
        if (this.state.errors.formIsValid) {
            this.setState({ isDisabled: true, loader: true })
            var data = {
                email: this.state.fields.email,
                name: this.state.fields.first_name + ' ' + this.state.fields.last_name,
                password: this.state.fields.password,
                universityId: this.state.selectedValue,
                dateOfBirth: moment(this.state.fields.dateOfBirth).format("YYYY-MM-DD"),
            }
            api.post('user', data).then(Result => {
                this.setState({ loader: false })
                if (Result.status == 200) {
                    AsyncStorage.setItem("UserDetails", JSON.stringify(Result));
                    Alert.alert(Result.message)
                    this.props.navigation.navigate('OtpVerification', { email: Result.data.email })
                } else {
                    Alert.alert(Result.msg)
                    this.setState({ fields: {}, selectedValue: "", isDisabled: false, errors: {} });
                }
            }).catch(err => {
                this.setState({ loader: false })
                console.log('error', err)
            })
        }
    }
    // sign up function---start


    render() {
        let TouchableCmp = TouchableOpacity;
        if (Platform.OS === 'android' && Platform.Version >= 21)
            TouchableCmp = TouchableNativeFeedback;
        return (
            <SafeAreaView style={{ backgroundColor: '#fff' }}>
                <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={style.spinnerTextStyle} />
                <ScrollView keyboardShouldPersistTaps="always" keyboardDismissMode='on-drag'>
                    <KeyboardAvoidingView>
                        <View style={style.container}>
                            <View style={style.subContainer1}>
                                <Text style={style.subContainer2}>Create Account</Text>
                            </View>
                            <View>

                                <Input
                                    label="First Name"
                                    name="first_name"
                                    value={this.state.fields.first_name}
                                    onChangeText={text => this.handleChange(text, 'first_name')}
                                    errorMessage={this.state.errors['first_name']}
                                    onSubmitEditing={() =>
                                        this.customInput2.refs.innerTextInput2.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                />

                                <Input
                                    label="Last Name"
                                    name="last_name"
                                    value={this.state.fields.last_name}
                                    onChangeText={text => this.handleChange(text, 'last_name')}
                                    errorMessage={this.state.errors['last_name']}
                                    ref={ref => (this.customInput2 = ref)}
                                    refInner="innerTextInput2"
                                    onSubmitEditing={() =>
                                        this.customInput3.refs.innerTextInput3.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                />

                                <Input
                                    label="University Email"
                                    name="Email"
                                    keyboardType="email-address"
                                    autoCapitalize="none"
                                    value={this.state.fields.email}
                                    onChangeText={text => this.handleChange(text, 'email')}
                                    errorMessage={this.state.errors['email']}
                                    ref={ref => (this.customInput3 = ref)}
                                    refInner="innerTextInput3"
                                    onSubmitEditing={() =>
                                        this.customInput4.refs.innerTextInput4.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                />

                                <Input
                                    label="Phone"
                                    name="Phone"
                                    keyboardType="number-pad"
                                    maxLength={11}
                                    value={this.state.fields.phone}
                                    onChangeText={text => this.handleChange(text, 'phone')}
                                    errorMessage={this.state.errors['phone']}
                                    ref={ref => (this.customInput4 = ref)}
                                    refInner="innerTextInput4"
                                    onSubmitEditing={() =>
                                        this.customInput5.refs.innerTextInput5.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                />
                                <View style={GlobalStyles.picker2}>
                                    <DatePicker
                                        style={style.datePickerContainer2}
                                        label="D.O.B"
                                        date={this.state.fields.dateOfBirth}
                                        mode="date"
                                        placeholder="Select Date of Birth"
                                        maxDate={moment().subtract(17, "years")}
                                        format="DD/MM/YYYY"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={style.datePicker2}
                                        onDateChange={date => this.handleChange(date, 'dateOfBirth')}

                                    />
                                </View>

                                <View style={GlobalStyles.picker3}>
                                    <Picker
                                        selectedValue={this.state.selectedValue || "select one"}
                                        style={style.picker2}
                                        onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue: itemValue, val: 1 })}

                                    >
                                        {this.state.dataSource.map((item, key) => <Picker.Item label={item.universityName}
                                            key={key} value={item._id}
                                            color="#7f8c8d" />)}
                                    </Picker>
                                </View>

                                <Input
                                    label="Password"
                                    name="password"
                                    secureTextEntry={true}
                                    value={this.state.fields.password}
                                    onChangeText={text => this.handleChange(text, 'password')}
                                    errorMessage={this.state.errors['password']}
                                    ref={ref => (this.customInput5 = ref)}
                                    refInner="innerTextInput5"
                                    onSubmitEditing={() =>
                                        this.customInput6.refs.innerTextInput6.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                />

                                <Input
                                    label="Confirm Password"
                                    name="con_password"
                                    secureTextEntry={true}
                                    value={this.state.fields.con_password}
                                    onChangeText={text => this.handleChange(text, 'con_password')}
                                    errorMessage={this.state.errors['con_password']}
                                    ref={ref => (this.customInput6 = ref)}
                                    refInner="innerTextInput6"
                                    returnKeyType="done"
                                />
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <TouchableCmp disabled={this.state.isDisabled}
                                    onPress={this.handleSubmit}>
                                    <View
                                        style={[this.state.isDisabled == false ? style.button : style.disabledButton]}>
                                        <Text style={style.buttonTxt}>
                                            SignUp
                                        </Text>
                                    </View>
                                </TouchableCmp>
                                <View style={{ flexDirection: 'row', paddingBottom: 30 }}>
                                    <Text style={style.signUpBtnTxt}>Already a member? </Text>
                                    <TouchableOpacity style={style.signUpBtn} onPress={() => this.props.navigation.navigate('Login')}>
                                        <Text style={style.signUpBtnTxt1}>Sign in</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
        )
    }
}
const mapDispatchToProps = dispatch => {
    return {
        changeAuthState: details => dispatch(changeAuthState(details)),
    };
};

export default connect(null, mapDispatchToProps)(SignUp)

