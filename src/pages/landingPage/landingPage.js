import React, { Component } from 'react'
import { Image, Platform, Text, TouchableOpacity, View,} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Swiper from 'react-native-swiper'
import styles from './style';

export default class landingScreen extends Component {
  render(){
  return (       
      <View style={styles.container}>
        <View style={styles.logoDiv}>
          <Image source={require("../../assets/imsges/logo.png")}  style={styles.logo} />   

          <Text h4 style={styles.topTxt}>
              Saving your wallet and the enviroment
           </Text>  
        </View>
              
          <View style={styles.custSlider}>
          <Swiper style={styles.wrapper} showsButtons={true}>
            <View style={styles.slide1}>
              <Text style={styles.text}>Hello Swiper</Text>
            </View>
            <View style={styles.slide2}>
              <Text style={styles.text}>Beautiful</Text>
            </View>
            <View style={styles.slide3}>
              <Text style={styles.text}>And simple</Text>
            </View>
          </Swiper>
          </View>
        
         

        <View style={styles.Btnwrap}>
          <TouchableOpacity style={styles.blubtn} onPress={()=>this.props.navigation.navigate('Signup')}>
            <Text style={styles.btntext}>Sign Up</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.orangebtn} onPress={()=>this.props.navigation.navigate('Login')}>
            <Text style={styles.btntext}>Log in</Text>
          </TouchableOpacity>
        </View>

      </View>   
  );
}
}

