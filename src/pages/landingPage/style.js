export default {
    container: {
        paddingLeft:10,
        paddingRight:10,
        flex:1,
        backgroundColor:'#fff',
    },
    logoDiv:{      
        width:'100%',
        justifyContent:'center',
        alignItems:'center',  
        paddingbottom:20      
    },
    logo:{
        width:'100%',
        height:200
    },
    topTxt:{
        fontWeight:'500',
        fontSize:22, 
        textAlign:'center',
        paddingTop:10,
        color:'#777',
        fontFamily:'Oswald-Light'       
    },
    custSlider:{
        width:'100%',        
        flex:1,
        paddingTop:20
    },   
    Btnwrap:{
        paddingBottom:10,
    },
    blubtn:{       
        marginTop:10,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#007aff',
        borderRadius:10,        
    },
    btntext:{
        color:'#fff',
        textAlign:'center',
        fontWeight:'900',
        fontSize:24,
        fontFamily:'Oswald-Regular'
    },
    orangebtn:{       
        marginTop:10,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#fca600',
        borderRadius:10,       
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB'
      },
      slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5'
      },
      slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9'
      },
      text: {
        color: '#fff',
        fontSize: 30,
        //fontWeight: 'bold',
        fontFamily:'Oswald-Bold'
      }
}