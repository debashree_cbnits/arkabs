export default {
  container: {
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  subContainer1: {
    paddingHorizontal: 20,
    marginTop: '15%',
    backgroundColor: '#fff',
  },
  subContainer2: {
    marginBottom: '15%',
    alignItems: 'center',
  },
  containerTxt: {
    fontFamily: 'Oswald-Medium',
    fontSize: 35,
    color: '#3498db',
  },
  button: {
    backgroundColor: '#3498db',
    width: '80%',
    marginVertical: 50,
    borderRadius: 10,
    paddingTop: 5,
    paddingBottom: 10,
    alignItems: 'center',
    elevation: 6,
  },
  buttonTxt: {
    color: 'white',
    fontFamily: 'Oswald-Bold',
    fontSize: 24,
  },
  disabledButton: {
    backgroundColor: '#808080',
    width: '70%',
    marginVertical: 50,
    borderRadius: 10,
    paddingTop: 5,
    paddingBottom: 10,
    alignItems: 'center',
    elevation: 3,
  },
  forgotPassword: {
    alignItem: 'center',
    marginBottom: 10,
  },
  forgotPasswordTxt: {
    color: '#f00',
    fontSize: 18,
    fontFamily: 'Oswald-Light',
  },
  signUpBtn: {
    alignItem: 'center',
    flexDirection: 'row',
  },
  signUpBtnTxt: {
    color: '#3498db',
    fontSize: 18,
    fontFamily: 'Oswald-Regular',
  },
  signUpBtnTxt1: {
    color: '#3498db',
    fontSize: 18,
    fontFamily: 'Oswald-SemiBold',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  multiFldRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  datePickerContainer: {
    width: '100%',
    paddingBottom: 10,
  },

  datePicker: {
    dateIcon: {
      position: 'absolute',
      left: 0,
      top: 0,
      width: 0,
      height: 0,
      marginLeft: 0,
    },
    dateInput: {
      width: '100%',
      height: 45,
      marginLeft: 0,
      borderRadius: 5,
    },
    placeholderText: {
      fontSize: 15,
      color: '#aaa',
    },
  },
  picker: {
    marginStart: 25,
    marginEnd: 25,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  width50: {
    width: '50%',
  },
  info: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  label2: {
    fontSize: 18,
    color: '#666',
    fontFamily: 'Oswald-Bold',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 6,
    paddingTop: 6,
  },

  //cardDetails new styles (payment Details)

  cardInfoHead: {
    backgroundColor: '#fff',
    elevation: 2,
    borderRadius: 5,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  cardNumSec: {
    width: '100%',
    height: 50,
    paddingHorizontal: 10,
    borderBottomColor: '#C0C0C0',
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: 'space-between'
  },
  cardNum: {
    width: '55%',
    height: 50,
    fontFamily: 'Oswald-Regular',
    fontSize: 16,
  },
  cardExpiry: {
    width: '50%',
    height: 50,
    paddingHorizontal: 15,
    borderRightColor: '#C0C0C0',
    borderRightWidth: 1,
    fontFamily: 'Oswald-Regular',
    fontSize: 16,
  },
  cardCVV: {
    height: 50,
    fontFamily: 'Oswald-Regular',
    fontSize: 16,
  },
  payText: {
    fontSize: 18,
    color: '#666',
    fontFamily: 'Oswald-Regular',
    marginBottom: 10,
  },
  cardHolder: {
    width: '100%',
    height: 50,
    borderRadius: 5,
    paddingHorizontal: 15,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    backgroundColor: '#fff',
    elevation: 2,
    fontFamily: 'Oswald-Regular',
    fontSize: 16,
  },
  cvv: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  payImg: {
    width: 29,
    height: 15
  },
  payImg2: {
    width: 26,
    height: 17,
    marginRight: 5,
    borderRadius: 3,
  },
};
