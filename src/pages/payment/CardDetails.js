import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableNativeFeedback,
  TouchableOpacity,
  SafeAreaView,
  Platform,
  Alert,
  TextInput,
  Image,
} from 'react-native';
import Input from '../../Components/floatingInput';
import style from './style';
import {ScrollView} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import Validator from '../../helper/validator';
import api from '../../Api/api';
import stripe from 'tipsi-stripe';
import Spinner from 'react-native-loading-spinner-overlay';
import * as firebase from 'firebase';
// var firebaseConfig = {
//   apiKey: "AIzaSyBPn_p6JBWkIUSDzG0ke-0IGMNwrnfLpaY",
//   authDomain: "arkabs.firebaseapp.com",
//   databaseURL: "https://arkabs.firebaseio.com/",
//   projectId: "arkabs",
//   storageBucket: "arkabs.appspot.com",
//   messagingSenderId: "769927113864",
// }

var firebaseConfig = {
  apiKey: 'AIzaSyBT6pCnWjwSOoVknx-no1HlpuSg4JRkJkQ',
  authDomain: 'arkabs-280713.firebaseapp.com',
  databaseURL: 'https://arkabs-280713.firebaseio.com',
  projectId: 'arkabs-280713',
  storageBucket: 'arkabs-280713.appspot.com',
  messagingSenderId: '1031996217292',
  appId: '1:1031996217292:web:6a5415afdc702f2a356f69',
  measurementId: 'G-MMZT0R9B7E',
};

export default class CardDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        cardNumber: '',
        // expMonth: 0,
        // expYear: 0,        
        cvc: 0,
        name: '',
      },
      cardExpiry:'',
      cardToken: '',
      cardLast4: '',
      errors: {},
      tripDetails: this.props.navigation.state.params.tripDetails,
      transactionId: this.props.navigation.state.params.transactionId,
      chatRoomId: this.props.navigation.state.params.tripNumber,
      isDisabled: false,
      loader: false,
      userId: '',
      token: '',
      userName: '',
    };

    stripe.setOptions({
      // publishableKey: 'pk_test_2AX76WTx3ZnLnWdnDmnNDEXD00Oj8gyvrj',
      publishableKey:
        'pk_test_51GqlbCKG1GemScOFHH85EzwBsyw2tVNPGcviGnAjI5HhrxOAi0SVhjxCumeWWL3TUQUvWClNSyo8jsrz5ICGedO600CQv7OHQI',
    });

    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
      this.state.chatRef = firebase
        .database()
        .ref()
        .child('chatMessages');
    } else {
      this.state.chatRef = firebase
        .database()
        .ref()
        .child('chatMessages');
    }
  }

  componentDidMount() {

    AsyncStorage.getItem('UserDetails').then(res => {
      if (res) {
        this.setState({userDetails: JSON.parse(res)});
        this.setState({
          userId: this.state.userDetails.data._id,
          token: this.state.userDetails.token,
          userName: this.state.userDetails.data.name,
        });

        api
          .getUserDetails('user/' + this.state.userId, this.state.token)
          .then(result => {
            if (result.status == 200) {

            }
          })
          .catch(err => {});
      }
    }).catch = err => {};
  }

  handleChange(value, name) {
    let fields = this.state.fields;
    fields[name] = value;
    this.setState({fields});
    this.setState({
      errors: Validator.validateForm(
        name,
        this.state.fields,
        this.state.errors,
      ),
    });
  }

  handlingCardExpiry(text) {    
    if (text.indexOf(".") >= 0 || text.length > 7) {
      return;
    }

    if (text.length == 2 && this.state.cardExpiry.length === 1) {
      text += "/";
    }
    this.setState({
      cardExpiry: text,
    });
  }
  

  handleSubmit = e => {    
    e.preventDefault();
    this.setState({
      errors: Validator.validateForm(
        null,
        this.state.fields,
        this.state.errors,
      ),
    });
    if (this.state.fields.cardNumber == "" && this.state.fields.cardNumber == "" && this.state.fields.cvv == "" && this.state.fields.name ==""  ){
      Alert.alert("Please fill up all the details")
    } else if (this.state.fields.cardNumber == ""){
      Alert.alert("Please enter card number")
    } else if (this.state.fields.cardExpiry == ""){
      Alert.alert("Please enter card expiry date")
    } else if (this.state.fields.cvv == ""){
      Alert.alert("Please enter cvv number")
    } else if (this.state.fields.name == ""){
      Alert.alert("Please enter name")
    } else {
    // if (this.state.errors.formIsValid) {
      this.setState({isDisabled: true, loader: true});
      var card = this.state.cardExpiry;
        var expiry = card.split('/');
        var mnth = expiry[0];
        var yr = expiry[1];
      const params = {
        // mandatory
        number: this.state.fields.cardNumber,
        expMonth: Number(mnth),
        expYear: Number(yr),
        cvc: this.state.fields.cvv,
        // optional
        name: this.state.fields.name,
        // currency: 'usd',
        // addressLine1: '123 Test Street',
        // addressLine2: 'Apt. 5',
        // addressCity: 'Test City',
        // addressState: 'Test State',
        // addressCountry: 'Test Country',
        // addressZip: '55555',
      };
      // console.log('paramsparams', params)
      stripe
        .createTokenWithCard(params)
        .then(result => {
          var cardData = {
            cardToken: result.tokenId,
            cardLast4: this.state.fields.cardNumber.slice(
              this.state.fields.cardNumber.length - 4,
            ),
            transactionId: this.state.transactionId,
          };
          //console.warn('input',cardData)
          api
            .POST(
              '/payment-api/stripe/create/charge',
              cardData,
              this.state.token,
            )
            .then(Result => {
              //  console.warn('result',Result)
              this.setState({loader: false});
              if (Result.status == 200) {
                const nowDate = new Date().toLocaleDateString();
                this.state.chatRef.push({
                  group: true,
                  groupName:
                    this.state.tripDetails.fromLocationName +
                    '-' +
                    this.state.tripDetails.toLocationName,
                  adminId: this.state.tripDetails.userId._id,
                  adminName: this.state.tripDetails.userId.name,
                  senderId: this.state.tripDetails.userId._id,
                  senderName: this.state.tripDetails.userId.name,
                  //"senderImage": this.state.senderImage,
                  //"Message": this.state.typeMessage,
                  //"userId": this.state.userId,
                  //"username": this.state.username,
                  //"userImage": this.state.userImage,
                  chatRoomId: this.state.tripDetails.tripNumber,
                  memberId: this.state.userId,
                  memberName: this.state.userName,
                  date: nowDate,
                });

                Alert.alert('Booking success');
                this.setState({fields: {}, isDisabled: false, errors: {}});
                this.props.navigation.navigate('Trips');
              } else {
                Alert.alert(Result.msg);
                this.setState({fields: {}, isDisabled: false, errors: {}});
              }
            })
            .catch(err => {
              this.setState({loader: false});
              console.log('error', err);
            });
        })
        .catch(error => {
          {
            this.setState({fields: {}, isDisabled: false, errors: {}});
            // console.warn('stripe_token error',error)
          }
        });
    }
  };
  render() {
    let TouchableCmp = TouchableOpacity;
    if (Platform.OS === 'android' && Platform.Version >= 21)
      TouchableCmp = TouchableNativeFeedback;
    return (
      <SafeAreaView style={{flex: 1}}>
        <Spinner
          visible={this.state.loader}
          textContent={'Loading...'}
          textStyle={style.spinnerTextStyle}
        />
        <ScrollView style={{backgroundColor: '#fff'}}>
          <View style={style.container}>
            <View style={style.subContainer1}>
              <View style={style.subContainer2}>
                <Text style={style.containerTxt}>Payment Details</Text>
              </View>

              <View style={{marginBottom: 20}}>
                <Text style={style.payText}>Card Information</Text>
                <View style={style.cardInfoHead}>
                  <View style={style.cardNumSec}>
                    <TextInput
                      placeholder="1234 1234 1234 1234"
                      placeholderTextColor="#C0C0C0"
                      returnKeyType="go"
                      maxLength={16}
                      keyboardType="numeric"
                      style={style.cardNum}
                      value={this.state.fields.cardNumber}
                      onChangeText={(text)=> this.setState({cardNumber: text}, ()=>{
                        this.handleChange(text, 'cardNumber')
                      })}
                      errorMessage={this.state.errors['cardNumber']}
                    />
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={require('../../assets/imsges/visa.png')}
                        style={style.payImg2}
                      />
                      <Image
                        source={require('../../assets/imsges/mastercard.png')}
                        style={style.payImg2}
                      />
                      <Image
                        source={require('../../assets/imsges/american.png')}
                        style={style.payImg2}
                      />
                      <Image
                        source={require('../../assets/imsges/maestro.png')}
                        style={style.payImg2}
                      />
                    </View>                    
                  </View>

                  <View style={{flexDirection: 'row'}}>
                    <TextInput
                      placeholder="MM / YY"
                      placeholderTextColor="#C0C0C0"
                      returnKeyType="go"                      
                      keyboardType="numeric"
                      style={style.cardExpiry}
                      // errorMessage={this.state.errors['cardExpiry']}                      
                      maxLength={5}
                      value={this.state.cardExpiry}
                      onChangeText={(text)=> 
                        this.handlingCardExpiry(text)
                      }
                    />

                    <View style={style.cvv}>
                      <TextInput
                        placeholder="CVC"
                        placeholderTextColor="#C0C0C0"
                        returnKeyType="done"
                        maxLength={3}
                        keyboardType="numeric"
                        style={style.cardCVV}
                        value={this.state.fields.cvv}
                        // onChangeText={text => this.handleChange(text, 'cvv')}
                        onChangeText={(text)=> this.setState({cvv: text}, ()=>{
                          this.handleChange(text, 'cvv')
                        })}
                        errorMessage={this.state.errors['cvv']}
                      />
                      <Image
                        source={require('../../assets/imsges/cvv.png')}
                        style={style.payImg}
                      />
                    </View>
                  </View>
                </View>
              </View>

              <View>
                <Text style={style.payText}>Name on Card</Text>
                <TextInput
                  placeholder="Name of Card Holder"
                  placeholderTextColor="#C0C0C0"
                  returnKeyType="done"
                  style={style.cardHolder}
                  value={this.state.fields.name}
                  onChangeText={(text)=> this.setState({name: text}, ()=>{
                    this.handleChange(text, 'name')
                  })}
                />
              </View>

              {/* <Input
                label="Card Number"
                name="Card Number"
                keyboardType="number-pad"
                maxLength={16}
                value={this.state.fields.cardNumber}
                onChangeText={text => this.handleChange(text, 'cardNumber')}
                errorMessage={this.state.errors['cardNumber']}
                blurOnSubmit={false}
              />

              <View style={style.multiFldRow}>
                <View style={style.width50}>
                  <Input
                    label="Expiry Month"
                    name="Expiry Month"
                    keyboardType="number-pad"
                    maxLength={2}
                    value={this.state.fields.expMonth}
                    onChangeText={text => this.handleChange(text, 'expMonth')}
                    errorMessage={this.state.errors['expMonth']}
                    blurOnSubmit={false}
                  />
                </View>
                <View style={style.width50}>
                  <Input
                    label="Expiry Year"
                    name="Expiry Year"
                    keyboardType="number-pad"
                    maxLength={2}
                    value={this.state.fields.expYear}
                    onChangeText={text => this.handleChange(text, 'expYear')}
                    errorMessage={this.state.errors['expYear']}
                    blurOnSubmit={false}
                  />
                </View>
              </View>
              <View style={style.width50}>
                  <Input
                    label="CVV"
                    name="CVV"
                    keyboardType="number-pad"
                    maxLength={3}
                    value={this.state.fields.cvv}
                    onChangeText={text => this.handleChange(text, 'cvv')}
                    errorMessage={this.state.errors['cvv']}
                    blurOnSubmit={false}
                  />
                </View>
              <Input
                label="Name Of Card Holder"
                name="Name Of Card Holder"
                value={this.state.fields.name}
                onChangeText={text => this.handleChange(text, 'name')}
                errorMessage={this.state.errors['name']}
                returnKeyType="next"
                blurOnSubmit={false}
              /> */}
            </View>
            <View style={{alignItems: 'center'}}>
              <TouchableCmp
                disabled={this.state.isDisabled}
                background={TouchableNativeFeedback.Ripple('#fca600')}
                onPress={this.handleSubmit}>
                <View
                  style={[
                    this.state.isDisabled == false
                      ? style.button
                      : style.disabledButton,
                  ]}>
                  <Text style={style.buttonTxt}>Payment</Text>
                </View>
              </TouchableCmp>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
