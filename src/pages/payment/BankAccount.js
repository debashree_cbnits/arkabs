import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableNativeFeedback,
  TouchableOpacity,
  SafeAreaView,
  Platform,
  Alert,
} from 'react-native';
import Input from '../../Components/floatingInput';
import style from './style';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../Api/api'
import { NetworkInfo } from "react-native-network-info";
import Validator from '../../helper/validator';
import Spinner from 'react-native-loading-spinner-overlay';
export default class BankAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //isBankAdded: false,
      token: '',
      userId: '',
      fields: {
        email: '',
        // dateOfBirth: '',
        address: '',
        city: '',
        postalCode: '',
        // state: '',
        first_name: "",
        last_name: "",
        // sortCode: "",
        accountNumber: ""
      },
      sortCode:'',
      errors: {},
      ipAddress: "",
      dobDay: "",
      dobMonth: "",
      dobYear: "",
      isDisabled: false,
      loader: false,
      userBankInfo: [],
      sortCodeError: false
    };

    NetworkInfo.getIPAddress().then(ipAddress => {
      this.setState({ ipAddress: ipAddress })
    });

    // Get IPv4 IP (priority: WiFi first, cellular second)
    //   NetworkInfo.getIPV4Address().then(ipv4Address => {
    //     this.setState({ ipAddress: ipv4Address })
    //     console.log(ipv4Address);
    //  });
  }

  componentDidMount() {
    this.setState({loader: true})
    AsyncStorage.getItem('UserDetails').then(res => {
      if (res) {
        this.setState({ userDetails: JSON.parse(res) })
        this.setState({
          userId: this.state.userDetails.data._id,
          token: this.state.userDetails.token
        })
        api.getUserDetails('user/' + this.state.userId, this.state.token).then((result) => {
          if (result.status == 200) {
            if (result.data.user.bankVerified == true) {
              this.setState({ isBankAdded: true, userBankInfo: result.data.user, loader: false })
            } else {
              this.setState({ isBankAdded: false,  loader: false })
            }
          }
        }).catch((err) => {
          this.setState({ loader: false})
        });
      }
    }).catch = (err) => {

    }
  }

  handleChange(value, name) {
    let fields = this.state.fields;
    fields[name] = value;
    this.setState({ fields });
    this.setState({ errors: Validator.validateForm(name, this.state.fields, this.state.errors) });
  }

  handleSortCode (value) {     
    this.setState ({sortCodeError: false}) 
    if (value.indexOf('.') >= 0 || value.length > 8) {
      return;
    }    
      if (value.length === 2 && this.state.sortCode.length === 1 || value.length === 5 ) {        
        value += "-";
      }
      this.setState({
        sortCode: value,
      });    
  }
  removeSortCode = () => {
    this.setState({sortCode: ""})
  }
  handleSubmit = (e) => {     
    e.preventDefault();
    if (this.state.sortCode == ""){
      this.setState({sortCodeError: true})
    }
    this.setState({ errors: Validator.validateForm(null, this.state.fields, this.state.errors) });
    if (this.state.errors.formIsValid) {  
        
      this.setState({ isDisabled: true, 
        loader: true
       })
      var data = {
        email: this.state.fields.email,
        // dobDay: moment(this.state.fields.dateOfBirth, 'DD/MM/YYYY').date(),
        // dobMonth: 1 + moment(this.state.fields.dateOfBirth, 'DD/MM/YYYY').month(),
        // dobYear: moment(this.state.fields.dateOfBirth, 'DD/MM/YYYY').year(),
        address: this.state.fields.address,
        city: this.state.fields.city,
        postalCode: this.state.fields.postalCode,
        // state: this.state.fields.state,
        ipAddress: this.state.ipAddress,
        // ipAddress: "157.43.131.138",
        firstName: this.state.fields.first_name,
        lastName: this.state.fields.last_name,
        sortCode: this.state.sortCode.replace(/-/g,""),
        accountNumber: this.state.fields.accountNumber

      }  
      
      api.POST('payment-api/stripe/create/bankAccount', data, this.state.token).then(Result => {
        this.setState({ loader: false })
        if (Result.status == 200) {
          Alert.alert("Bank account created")
          this.props.navigation.navigate('Account')
        } else {
          Alert.alert(Result.msg)
          this.setState({ fields: {},sortCode:"", selectedValue: "", isDisabled: false, errors: {} });
        }
      }).catch(err => {
        this.setState({ loader: false })
        console.log('error', err)
      })
    }
  }

  AddBank = () => {
    this.setState({ isBankAdded: false })
  }
  render() {
    let TouchableCmp = TouchableOpacity;
    if (Platform.OS === 'android' && Platform.Version >= 21)
      TouchableCmp = TouchableNativeFeedback;
   
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Spinner visible={this.state.loader}
          textContent={'Loading...'}
          textStyle={style.spinnerTextStyle} />
          <ScrollView style={{backgroundColor: '#fff'}}>
          {this.state.isBankAdded == false ?
            <View style={style.container}>
              <View style={style.subContainer1}>
                <View style={style.subContainer2}>
                  <Text style={style.containerTxt}>Add Withdraw Method</Text>
                </View>
                <Input
                  label="First Name"
                  name="First Name"
                  value={this.state.fields.first_name}
                  onChangeText={text => this.handleChange(text, 'first_name')}
                  errorMessage={this.state.errors['first_name']}
                  returnKeyType="next"
                  blurOnSubmit={false}
                />

                <Input
                  label="Last Name"
                  name="Last Name"
                  value={this.state.fields.last_name}
                  onChangeText={text => this.handleChange(text, 'last_name')}
                  errorMessage={this.state.errors['last_name']}
                  returnKeyType="next"
                  blurOnSubmit={false}
                />

                <Input
                  label="Email"
                  name="Email"
                  //iconName="envelope-o"
                  keyboardType="email-address"
                  autoCapitalize="none"
                  value={this.state.fields.email}
                  onChangeText={text => this.handleChange(text, 'email')}
                  errorMessage={this.state.errors['email']}
                  ref={ref => (this.customInput3 = ref)}
                  refInner="innerTextInput3"
                  onSubmitEditing={() =>
                    this.customInput4.refs.innerTextInput4.focus()
                  }
                  returnKeyType="next"
                  blurOnSubmit={false}
                />                
                <Input
                  label="Account Number"
                  name="Account Number"
                  keyboardType="number-pad"
                  maxLength={8}
                  value={this.state.fields.accountNumber}
                  onChangeText={text => this.handleChange(text, 'accountNumber')}
                  errorMessage={this.state.errors['accountNumber']}
                  blurOnSubmit={false}
                />
                <Input
                      label="Sort Code"
                      name="Sort Code"
                      maxLength={8}
                      keyboardType="number-pad"
                      value={this.state.sortCode}
                      onChangeText={text => this.handleSortCode(text)}
                      onKeyPress={({ nativeEvent }) => {
                        if (nativeEvent.key === 'Backspace') {
                          this.removeSortCode(this.state.sortCode)
                        }
                      }
                      }
                      errorMessage={this.state.sortCodeError? "Please enter sortCode" : ""}
                      blurOnSubmit={false}
                    />


                {/* <View style={style.picker}>
                  <DatePicker
                    style={style.datePickerContainer}
                    label="D.O.B"
                    mode="date"
                    date={this.state.fields.dateOfBirth}
                    placeholder="Select Date of Birth"
                    maxDate={moment().subtract(17, 'years')}
                    format="DD/MM/YYYY"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={style.datePicker}
                    onDateChange={date => this.handleChange(date, 'dateOfBirth')}
                  />
                </View> */}

                <Input
                  label="Address"
                  name="Address"
                  value={this.state.fields.address}
                  onChangeText={text => this.handleChange(text, 'address')}
                  errorMessage={this.state.errors['address']}
                  blurOnSubmit={false}
                />

                <View style={style.multiFldRow}>
                  <View style={style.width50}>
                    <Input
                      label="City"
                      name="City"
                      value={this.state.fields.city}
                      onChangeText={text => this.handleChange(text, 'city')}
                      errorMessage={this.state.errors['city']}
                      blurOnSubmit={false}
                    />
                  </View>
                  <View style={style.width50}>
                    <Input
                      label="Postal Code"
                      name="Postal Code"
                      value={this.state.fields.postalCode}
                      onChangeText={text => this.handleChange(text, 'postalCode')}
                      errorMessage={this.state.errors['postalCode']}
                      blurOnSubmit={false}
                    />
                  </View>
                  {/* <View style={style.width50}>
                    <Input
                      label="State"
                      name="State"
                      value={this.state.fields.state}
                      onChangeText={text => this.handleChange(text, 'state')}
                      errorMessage={this.state.errors['state']}
                      blurOnSubmit={false}
                    />
                  </View> */}
                </View>

                <View style={style.multiFldRow}>
                  {/* <View style={style.width50}>
                    <Input
                      label="Sort Code"
                      name="Sort Code"
                      value={this.state.fields.sortCode}
                      onChangeText={text => this.handleChange(text, 'sortCode')}
                      errorMessage={this.state.errors['sortCode']}
                      blurOnSubmit={false}
                    />
                  </View> */}

                  
                </View>
              </View>
              <View style={{ alignItems: 'center' }}>
                <TouchableCmp
                  disabled={this.state.isDisabled}
                  // background={TouchableNativeFeedback.Ripple('#fca600')}
                  onPress={this.handleSubmit}>
                  <View
                    style={[
                      this.state.isDisabled == false
                        ? style.button
                        : style.disabledButton,
                    ]}>
                    <Text style={style.buttonTxt}>Confirm</Text>
                  </View>
                </TouchableCmp>
              </View>
            </View>
            : <View style={style.container}>
              <View style={style.subContainer1}>
                <View style={style.subContainer2}>
                  <Text style={style.containerTxt}>Bank Account</Text>
                </View>

                <View style={style.row}>
                  <Text style={style.label2}>Account holder</Text>
                  <Text style={style.label2}>{this.state.userBankInfo.accountHolderName}</Text>
                </View>
                <View style={style.row}>
                  <Text style={style.label2}>Account email</Text>
                  <Text style={style.label2}>{this.state.userBankInfo.accountEmail}</Text>
                </View>
                <View style={style.row}>
                  <Text style={style.label2}>Account Number</Text>
                  <Text style={style.label2}>**** {this.state.userBankInfo.bankAccountNumber}</Text>
                </View>
                <View style={style.row}>
                  <Text style={style.label2}>Bank Name</Text>
                  <Text style={style.label2}>{this.state.userBankInfo.bankName}</Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                  <TouchableCmp
                    disabled={this.state.isDisabled}
                    background={TouchableNativeFeedback.Ripple('#fca600')}
                    onPress={() => this.AddBank()}>
                    <View
                      style={[
                        this.state.isDisabled == false
                          ? style.button
                          : style.disabledButton,
                      ]}>
                      <Text style={style.buttonTxt}>Add bank</Text>
                    </View>
                  </TouchableCmp>
                </View>
              </View>
            </View>
          }
        </ScrollView>
      </SafeAreaView>
    );
  }
}
