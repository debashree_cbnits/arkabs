export default {
  mapHead: {
    width: '100%',
    // height: 295,
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    paddingVertical: 15,
    position: 'absolute',
    bottom: 0,
  },
  locationSec: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // alignItems: 'center',
  },
  userSec: {
    width: 70,
    alignItems: 'center',
  },
  userImg: {
    width: 70,
    height: 70,
    borderRadius: 70 / 2,
  },
  userName: {
    marginTop: 5,
    fontSize: 18,
    fontFamily: 'Oswald-Medium',
    color: '#5a5958',
    textAlign: 'center',
    flexWrap: 'wrap',
  },
  username: {
    fontSize: 22,
    color: '#222',
    fontFamily: 'Oswald-Bold',
    paddingTop: 10,
  },
  journeyDt: {
    fontFamily: 'Oswald-Medium',
    color: '#5a5958',
    fontSize: 16,
    marginBottom: 10,
  },
  journeyTime: {
    flexDirection: 'row',
    width: '100%',
  },
  journeyTimeTxt: {
    color: '#9b9b9b',
    fontFamily: 'Oswald-Regular',
  },
  tripLocationSec: {
    flexDirection: 'row',
    width: '85%',
  },
  pinImg: {
    marginLeft: 10,
    marginRight: 15,
    resizeMode:'center',
    height: 75,
    width: 15,
  },
  tripLocations: {
    justifyContent: 'space-between',
    width: '80%',
  },
  tripText: {
    color: '#9b9b9b',
    fontFamily: 'Oswald-Regular',
  },
  btnText: {
    color: '#fff',
    fontFamily: 'Oswald-Medium',
    fontSize: 20,
  },
  bookingBtn: {
    width: '80%',
    height: 50,
    backgroundColor: '#0081E2',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 5,
  },
  logoDiv: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  logo: {
    // width: '60%',
    // height: 120
    width: 180,
    height: 80,
    resizeMode: 'cover',
    marginTop: 20,
    marginBottom: 15,
  },

  container: {
    paddingLeft: 10,
    paddingRight: 10,
    flex: 1,
    backgroundColor: '#fff',
  },
  logoDiv2: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profile: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    paddingTop: 10,
    paddingBottom: 20,
    backgroundColor: '#fff',
  },
  mtext: {
    fontSize: 16,
    fontFamily: 'Oswald-Bold',
    color: '#666',
    textAlign: 'center',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 6,
    paddingTop: 6,
  },
  chk: {
    backgroundColor: 'transparent',
    borderWidth: 0,
  },
  label: {
    fontSize: 16,
    color: '#666',
    fontFamily: 'Oswald-Regular',
  },
  info: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  label2: {
    fontSize: 18,
    color: '#666',
    fontFamily: 'Oswald-Bold',
  },
  frontBack: {
    width: '100%',
    marginTop: 15,
    flexDirection: 'row',
  },
  frontBackText: {
    width: '50%',
    textAlign: 'center',
    fontFamily: 'Oswald-Medium',
    fontSize: 16,
    color: '#9b9b9b',
  },
  ratingTrip: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
  },
  labeltext: {
    fontSize: 20,
    color: '#111',
    fontFamily: 'Oswald-Regular',
    marginBottom: 10,
  },
  driverName :{
    textAlign:'center', 
    marginTop: 5, 
    fontFamily: 'Oswald-Regular',  
    color: '#9b9b9b'
  }
};
