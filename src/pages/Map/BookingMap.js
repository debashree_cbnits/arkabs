import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableNativeFeedback,
  TouchableOpacity,
  SafeAreaView,
  Platform,
  Alert,BackHandler
} from 'react-native';
import style from './styles';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import styles from './styles';
import api from '../../Api/api'
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import { connect } from "react-redux"
import Spinner from 'react-native-loading-spinner-overlay';
import imageUrl from '../../Api/config';


const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

class BookingMap extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      token: '',
      tripDate: this.props.navigation.state.params.selectedItem.driveDate,
      tripTime: this.props.navigation.state.params.selectedItem.driveTime,
      tripNumber: this.props.navigation.state.params.selectedItem.tripNumber,
      driverId: this.props.navigation.state.params.selectedItem.userId._id,
      driverName : this.props.navigation.state.params.selectedItem.userId.name,
      driverImage :this.props.navigation.state.params.selectedItem.userId.profilePicture,  
      origin: {
        latitude: this.props.navigation.state.params.selectedItem.fromLocation.coordinates[1],
        longitude: this.props.navigation.state.params.selectedItem.fromLocation.coordinates[0]
      },
      destination: {
        latitude: this.props.navigation.state.params.selectedItem.toLocation.coordinates[1],
        longitude: this.props.navigation.state.params.selectedItem.toLocation.coordinates[0]
      },
      driveId: this.props.navigation.state.params.selectedItem._id,
      toLocationName: this.props.navigation.state.params.bookingTo,
      toLocation: {
        type: "Point",
        coordinates: []
      },
      fromLocationName: this.props.navigation.state.params.bookingFrom,
      fromLocation: {
        type: "Point",
        coordinates: []
      },
      userId: '',
      isDisabled: false,
      bookingAmount: this.props.navigation.state.params.selectedItem.amount,
      bookingFeePercent: this.props.navigation.state.params.bookingFeePercent,
      bookigFee: 0,
      isShowingBookingFee  : this.props.navigation.state.params.isShowingBookingFee ? this.props.navigation.state.params.isShowingBookingFee : '',      

    };    
  }

  componentDidMount() {
    
    AsyncStorage.getItem('UserDetails', (err, result) => {
      let parseData = JSON.parse(result)
      this.setState({
        userId: parseData.data._id,
        token: parseData.token
      })
    });

    BackHandler.addEventListener('hardwareBackPress', function () {
      if (this.props.navigation.state.routeName === 'BookingMap') {
        this.props.navigation.navigate('Ride');
        return true;
      } else {
        this.props.navigation.goBack(null);
        return true;
      }

    }.bind(this));
  }

  rideBooking = () => {
    let data = {
      driveId: this.state.driveId,
      toLocationName: this.state.toLocationName,
      toLocation: {
        type: "Point",
        coordinates: [this.state.destination.longitude, this.state.destination.latitude]
      },
      fromLocationName: this.state.fromLocationName,
      fromLocation: {
        type: "Point",
        coordinates: [this.state.origin.longitude, this.state.origin.latitude]
      },
      userId: this.state.userId

    }
    this.setState({ loader: true })    
    api.POST('trip-api/trip/booking', data, this.state.token).then((result) => {      
      this.setState({ loader: false })
      if (result.status == 200) {
        this.props.navigation.navigate('CardDetails', { transactionId: result.transactionId, tripNumber: this.state.tripNumber, tripDetails: this.props.navigation.state.params.selectedItem })
      } else {
        Alert.alert(result.msg)
      }
    }).catch((err) => {
      console.log('error', err)
      this.setState({ loader: false })
      console.log('error', err)
    });

  }

  render() {   
    return (
      <>
        <MapView
          style={{width: '100%', height: deviceHeight - 300}}
          provider={PROVIDER_GOOGLE}
          showsUserLocation
          initialRegion={{
            latitude: this.state.origin && Number(this.state.origin.latitude),
            longitude: this.state.origin &&  Number(this.state.origin.longitude),
            longitudeDelta: 0.9,
            latitudeDelta: 0.9
          }}
        >
           <MapView.Marker         
              // pinColor='#0081E2'
              coordinate={{
                latitude: this.state.origin && Number(this.state.origin.latitude),
                longitude: this.state.origin &&  Number(this.state.origin.longitude),
                latitudeDelta: 0.4,
                longitudeDelta: 0.41
              }}
            >
              <Image source={require('../../assets/imsges/blueMarker.png')} style={{height: 41, width:25 }} />
          </MapView.Marker>
           <MapView.Marker                 
                  // pinColor='#FCA600'
                  coordinate={{
                    latitude: this.state.destination && Number(this.state.destination.latitude),
                    longitude: this.state.destination &&  Number(this.state.destination.longitude),
                    latitudeDelta: 0.4,
                    longitudeDelta: 0.41
                  }}
                >  
              <Image source={require('../../assets/imsges/orangeMarker.png')} style={{height: 41, width:25 }} />
            </MapView.Marker>
        </MapView>
        <View style={styles.mapHead}>
          <View style={styles.locationSec}>
            <View style={{width: deviceWidth - 100}}>
              <Text style={styles.journeyDt}>{moment(this.state.tripDate).format("D MMM YYYY")}</Text>
              <View style={styles.journeyTime}>
                <Text style={styles.journeyTimeTxt}>{this.state.tripTime}</Text>
                <View style={styles.tripLocationSec}>
                  <Image
                    source={require('../../assets/imsges/locationLine.png')}
                    style={styles.pinImg}
                  />
                  <View style={styles.tripLocations}>
                    <Text style={styles.tripText}>{this.state.fromLocationName}</Text>
                    <Text style={styles.tripText}>{this.state.toLocationName}</Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.userSec}>
            <TouchableOpacity style={styles.userName} onPress={()=> this.props.navigation.navigate ('DriverProfile', {"driverId": this.state.driverId})}>
            {
                this.state.driverImage ? (
                    <Image  style={styles.userImg} source={{ uri: this.state.driverImage.includes('/userImage') ? imageUrl.img_url+this.state.driverImage : this.state.driverImage }}/>
                ) : (
                  <Image
                    source={require('../../assets/imsges/avatar.png')}
                    style={styles.userImg}
                  />
                    )
            }
            </TouchableOpacity>                           
                <Text style={styles.driverName}>{this.state.driverName}</Text>              
            </View>
          </View>

          <View
            style={{
              width: '100%',
              marginVertical: 16,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View>
              <Text style={styles.tripText}>Price</Text>
              {this.state.isShowingBookingFee ? 
              <Text style={styles.tripText}>{this.state.bookingFeePercent}% Booking Fee</Text>
              :
              <Text style={styles.tripText}>Booking Fee</Text>
              }
              <Text style={styles.tripText}>Total</Text>
            </View>
            <View style={{alignItems: 'flex-end'}}>
              <Text style={styles.tripText}>
                £{(this.state.bookingAmount).toFixed(2)}
              </Text>
              {this.state.isShowingBookingFee ? 
              <Text style={styles.tripText}>£{(Math.round(((this.state.bookingFeePercent / 100) * this.state.bookingAmount)*100)/100).toFixed(2)}
              </Text>
              : 
              <Text style={styles.tripText}>
              £0
              </Text>
              }
              {this.state.isShowingBookingFee ?
              <Text style={styles.tripText}>£{(this.state.isShowingBookingFee ? Math.round((this.state.bookingAmount + Math.round(((this.state.bookingFeePercent / 100) * this.state.bookingAmount)*100)/100)*100)/100 : this.state.bookingAmount).toFixed(2)}</Text>
              :
              <Text style={styles.tripText}>£{this.state.bookingAmount.toFixed(2)}</Text>
              }
            </View>
          </View>

          <TouchableOpacity style={styles.bookingBtn} onPress={() => this.rideBooking()}>
            <Text style={styles.btnText}>Confirm Booking</Text>
          </TouchableOpacity>
        </View>
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    RideReducer: state.Ride,
  };
};
export default connect(mapStateToProps, null)(BookingMap)

