import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Image, Alert, ActivityIndicator} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { Avatar, Icon, CheckBox } from 'react-native-elements'
import style from './styles';
import ImagePicker from "react-native-image-crop-picker";
import api from "../../Api/api"
import config from "../../Api/config"
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Item } from 'native-base';
import styles from './styles';
import imageUrl from '../../Api/config';


export default class DriverProfile extends Component {
      constructor(props) {
            super(props);
            this.state = {
                  userDetails: '',
                  userName: '',
                  profileDetails: [],
                  tripRating: '',
                  UniversityDetails: [],                  
                  driverId: this.props.navigation.state.params.driverId,
                  profile_picture: [],
                  profilePicture: '',
                  mimeType: '',
                  fileName: '',
                  drivingLicence: [],
                  drivingLicenseFront: '',
                  drivingLicenseBack: '',
                  drivingLicenceFrontName: '',
                  drivingLicenceBackName: '',
                  loader: false,
            }          
      }

      componentDidMount() {            
            this.getUserDetails()   
            this.props.navigation.addListener("didFocus", () => {
                  this.getUserDetails()                   
            });            
      }

      getUserDetails () {
            this.setState({loader: true})
            AsyncStorage.getItem('UserDetails').then(res => {
                  if (res) {
                        this.setState({ userDetails: JSON.parse(res),  token: JSON.parse(res).token })
                        api.getUserDetails('user/' +this.props.navigation.state.params.driverId, this.state.token).then((result) => {                                                            
                              if (result.status == 200) {
                                    this.setState({
                                          tripRating: result.data,
                                          profileDetails: result.data.user,
                                          UniversityDetails: result.data.user.universityId,
                                          loader: false
                                    //       profile_picture: result.data.user.profilePicture,
                                    //       drivingLicenseFront: result.data.user.drivingLicence.length > 0 && result.data.user.drivingLicence[0] ? result.data.user.drivingLicence[0].path : '',
                                    //       drivingLicenseBack: result.data.user.drivingLicence.length > 0 && result.data.user.drivingLicence[1] ? result.data.user.drivingLicence[1].path : '',
                                    });

                              }
                        }).catch((err) => {
                              this.setState({loader: false})
                        });
                  }
            }).catch = (err) => {
                  this.setState({loader: false})
            }
      }

      render() {            
            if (this.state.loader) {
                  return (
                      <ActivityIndicator size="large" style={{flex:1}}/>                      
                  )
            }
            return (
                  <View style={style.container}>
                        <View style={style.logoDiv2}>
                              <Image source={require("../../assets/imsges/logo.png")} style={style.logo} />
                        </View>
                        <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{...style.profile, backgroundColor: '#0000'}}>
                              <View>
                              {this.state.profileDetails.profilePicture ?
                                   <Avatar rounded source={{ uri: this.state.profileDetails.profilePicture.includes('/userImage') ? imageUrl.img_url+this.state.profileDetails.profilePicture : this.state.profileDetails.profilePicture }} size="xlarge" />
                                    : 
                                    <Avatar size="xlarge" rounded source={require("../../assets/images/user.png")} />

                              }
                              {/* <TouchableOpacity style={{ position: 'absolute', bottom: 0 , right: 0 }} onPress={() => this.showActionSheet()}>
                                    <Image source={require("../../assets/imsges/edit.png")} style={{ width: 40, height: 40 }} />
                              </TouchableOpacity> */}
                              </View>
                              <Text style={style.username}>{this.state.profileDetails.name}</Text>
                              <View style={styles.ratingTrip}>
                              <Text style={style.mtext}> Rating:
                              {this.state.tripRating.averageRating ? Math.round(this.state.tripRating.averageRating) : 0} 
                              </Text>
                              <Text style={style.mtext}> Trips:
                              {this.state.tripRating.totalTrip} 
                              </Text>
                              </View>
                              
                        </View>
                        
                              <View style={style.details}>
                                    <View style={style.row}>
                                          <CheckBox 
                                          disabled                                          
                                          checked = {this.state.profileDetails.isAdminVerified} 
                                          title='Verified ID' 
                                          containerStyle={style.chk} 
                                          textStyle={style.label} />

                                          <CheckBox 
                                          disabled
                                          title='Verified Student'                                          
                                          checked={this.state.profileDetails.isVerified}
                                          containerStyle={style.chk} textStyle={style.label} />
                                    </View>

                                    <View style={style.info}>
                                          <View style={style.row}>
                                                <Text style={style.label2}>University</Text>
                                                <Text style={styles.labeltext}>{this.state.UniversityDetails.universityName ? this.state.UniversityDetails.universityName : '.................'}</Text>
                                          </View>
                                          <View style={style.row}>
                                                <Text style={[style.label2, {width:'50%'}]}>Bio</Text>
                                                <Text style={[styles.labeltext, {width:'50%', textAlign:'right'}]}>{this.state.profileDetails.aboutMe ? this.state.profileDetails.aboutMe : '.................'}</Text>
                                          </View>
                                          <View style={style.row}>
                                                <Text style={style.label2}>Make/Model</Text>
                                                <Text style={styles.labeltext}>{this.state.profileDetails.model ? this.state.profileDetails.model : '.................'}</Text>
                                          </View>
                                          <View style={style.row}>
                                                <Text style={style.label2}>Number Plate</Text>
                                                <Text style={styles.labeltext}>{this.state.profileDetails.carDetails ? this.state.profileDetails.carDetails : '.................'}</Text>
                                          </View>
                                          <View style={style.row}>
                                                <Text style={style.label2}>Colour</Text>
                                                <Text style={styles.labeltext}>{this.state.profileDetails.color ? this.state.profileDetails.color : '.................'}</Text>
                                          </View>
                                          <View style={style.row}>
                                                {/* <Text style={style.label2}>Driving Licence</Text> */}
                                          </View>
                                          {/* <View style={style.frontBack}>
                                                <Text style={style.frontBackText}>Front</Text>
                                                <Text style={style.frontBackText}>Back</Text>
                                          </View> */}

                                    </View>

                              </View>
                        </ScrollView>
                  </View>
            )
      }
}
