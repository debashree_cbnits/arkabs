import React, { Component } from 'react'
import { View, Text, Image, SafeAreaView, TouchableOpacity, TouchableNativeFeedback, Platform, KeyboardAvoidingView, Picker, Alert } from 'react-native'
import Input from "../../Components/floatingInput"
import AsyncStorage from '@react-native-community/async-storage';
import { GlobalStyles } from '../../assets/StylesGlobal/GlobalStyle';
import style from "../Signup/style"
import { ScrollView } from 'react-native-gesture-handler'
// import GooglePlacesInput from "../../Components/googlePlacesApi"
import Icon from 'react-native-vector-icons/FontAwesome';
import Validator from '../../helper/validator';
import api from '../../Api/api'
import DatePicker from 'react-native-datepicker'
import { Result } from 'antd-mobile'
import Spinner from 'react-native-loading-spinner-overlay';

export default class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //     first_name:this.props.navigation.state.params.userDetails.data.first_name?this.props.navigation.state.params.userDetails.data.first_name:'',
            //     last_name: this.props.navigation.state.params.userDetails.data.last_name?this.props.navigation.state.params.userDetails.data.last_name:'',
            fields: {
                name: this.props.navigation.state.params.userDetails.name ? this.props.navigation.state.params.userDetails.name : '',
                email: this.props.navigation.state.params.userDetails.email ? this.props.navigation.state.params.userDetails.email : '',
                dateOfBirth: this.props.navigation.state.params.userDetails.dateOfBirth ? this.props.navigation.state.params.userDetails.dateOfBirth : '',
                aboutMe: this.props.navigation.state.params.userDetails.aboutMe ? this.props.navigation.state.params.userDetails.aboutMe : '',
                carDetails: this.props.navigation.state.params.userDetails.carDetails ? this.props.navigation.state.params.userDetails.carDetails : '',
                model: this.props.navigation.state.params.userDetails.model ? this.props.navigation.state.params.userDetails.model : '',
                color: this.props.navigation.state.params.userDetails.color ? this.props.navigation.state.params.userDetails.color : '',
            },
            token: this.props.navigation.state.params.token ? this.props.navigation.state.params.token : '',
            userId: this.props.navigation.state.params.userDetails._id ? this.props.navigation.state.params.userDetails._id : '',
            User: {},
            errors: {},
            loader: 0,
            dataSource: [],
            isDisabled: false,
            isAdminVerified :  this.props.navigation.state.params.userDetails.isAdminVerified && this.props.navigation.state.params.userDetails.isAdminVerified
        };        
    }

    handleChange(value, name) {
        let fields = this.state.fields;
        fields[name] = value;
        this.setState({ fields });
        this.setState({ errors: Validator.validateForm(name, this.state.fields, this.state.errors) });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({ errors: Validator.validateForm(null, this.state.fields, this.state.errors) });
        if (this.state.errors.formIsValid) {
            this.setState({ isDisabled: true, loader: true })
            var data = {
                name: this.state.fields.name,
                email: this.state.fields.email,
                dateOfBirth: this.state.fields.dateOfBirth,
                aboutMe: this.state.fields.aboutMe,
                carDetails: this.state.fields.carDetails,
                model: this.state.fields.model,
                color: this.state.fields.color
            }
            api.put('user/' + this.state.userId, data, this.state.token).then(Result => {
                this.setState({ loader: false })
                Alert.alert(Result.msg)
                this.props.navigation.navigate('Account')
            }).catch(err => {
                this.setState({ loader: false })
                console.log('error', err)
            })
        }
    }
    render() {
        // console.warn (this.state.isAdminVerified)
        let TouchableCmp = TouchableOpacity;
        if (Platform.OS === 'android' && Platform.Version >= 21)
            TouchableCmp = TouchableNativeFeedback;
        return (
            <SafeAreaView>
                <Spinner visible={this.state.loader}
                    textContent={'Loading...'}
                    textStyle={{ color: '#FFF' }} />
                <ScrollView style = {{backgroundColor: '#fff'}} keyboardShouldPersistTaps="always" keyboardDismissMode='on-drag'>
                    <KeyboardAvoidingView behavior="padding">
                        {/* <View>
                                <Image source={require('../../assets/imsges/logo.png')} style={GlobalStyles.logo} />
                            </View> */}
                        <View style={style.container}>
                            <View style={style.subContainer1}>
                                <Text style={style.subContainer2}>Update Account</Text>
                            </View>
                            <View>

                                <Input
                                    label="Name"
                                    name="name"
                                    //iconName="user-circle-o"
                                    value={this.state.fields.name}
                                    onChangeText={text => this.handleChange(text, 'name')}
                                    errorMessage={this.state.errors['name']}
                                    onSubmitEditing={() =>
                                        this.customInput2.refs.innerTextInput2.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                    
                                    editable = {false}
                                    
                                    
                                />

                                {/* <Input
                                        label="Last Name"
                                        name="last_name"
                                        iconName="user-circle-o"
                                        value={this.state.last_name}
                                        onChangeText={text => this.handleChange(text, 'last_name')}
                                        errorMessage={this.state.errors['last_name']}
                                        ref={ref => (this.customInput2 = ref)}
                                        refInner="innerTextInput2"
                                        onSubmitEditing={() =>
                                            this.customInput3.refs.innerTextInput3.focus()
                                        }
                                        returnKeyType="next"
                                        blurOnSubmit={false}
                                    /> */}

                                <Input
                                    label="University Email"
                                    name="Email"
                                    // iconName="envelope-o"
                                    keyboardType="email-address"
                                    autoCapitalize="none"
                                    value={this.state.fields.email}
                                    onChangeText={text => this.handleChange(text, 'email')}
                                    errorMessage={this.state.errors['email']}
                                    ref={ref => (this.customInput3 = ref)}
                                    refInner="innerTextInput3"
                                    onSubmitEditing={() =>
                                        this.customInput4.refs.innerTextInput4.focus()
                                    }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                    editable = {false}

                                />

                                {/* <Input
                                        label="Phone no."
                                        name="Phone no."
                                        keyboardType="number-pad"
                                        maxLength={10}
                                        iconName="phone"
                                        value={this.state.fields.phone}
                                        onChangeText={text => this.handleChange(text, 'phone')}
                                        errorMessage={this.state.errors['phone']}
                                        ref={ref => (this.customInput4 = ref)}
                                        refInner="innerTextInput4"
                                        onSubmitEditing={() =>
                                            this.customInput5.refs.innerTextInput5.focus()
                                        }
                                        returnKeyType="next"
                                        blurOnSubmit={false}
                                    /> */}
                                <View style={GlobalStyles.picker2}>
                                    <DatePicker
                                        style={style.datePickerContainer2}
                                        label="D.O.B"
                                        date={this.state.fields.dateOfBirth}
                                        mode="date"
                                        placeholder={this.state.fields.dateOfBirth}
                                        format="YYYY-MM-DD"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={style.datePicker2}
                                        onDateChange={date => this.handleChange(date, 'dateOfBirth')}
                                        disabled ={true}
                                    />
                                </View>
                                {/* <View>
                                    <GooglePlacesInput 
                                    getVal={this.fetchDetail}
                                     ref="address"
                                      next={this.refs.dateRef} />
                                    </View> */}
                                <Input
                                    label="Bio"
                                    name="About Me"
                                    //iconName="user-circle-o"
                                    value={this.state.fields.aboutMe}
                                    onChangeText={text => this.handleChange(text, 'aboutMe')}
                                    //errorMessage={this.state.errors['aboutMe']}
                                    // ref={ref => (this.customInput2 = ref)}
                                    // refInner="innerTextInput2"
                                    // onSubmitEditing={() =>
                                    //     this.customInput3.refs.innerTextInput3.focus()
                                    // }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                />
                                <Input
                                    label="Number Plate"
                                    name="carDetails"
                                    //iconName="user-circle-o"
                                    value={this.state.fields.carDetails}
                                    onChangeText={text => this.handleChange(text, 'carDetails')}
                                    // errorMessage={this.state.errors['last_name']}
                                    // ref={ref => (this.customInput2 = ref)}
                                    // refInner="innerTextInput2"
                                    // onSubmitEditing={() =>
                                    //     this.customInput3.refs.innerTextInput3.focus()
                                    // }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                    // editable = {this.state.isAdminVerified}
                                />
                                <Input
                                    label="Make/Model"
                                    name="model"
                                    //iconName="user-circle-o"
                                    value={this.state.fields.model}
                                    onChangeText={text => this.handleChange(text, 'model')}
                                    // errorMessage={this.state.errors['last_name']}
                                    // ref={ref => (this.customInput2 = ref)}
                                    // refInner="innerTextInput2"
                                    // onSubmitEditing={() =>
                                    //     this.customInput3.refs.innerTextInput3.focus()
                                    // }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                    // editable = {this.state.isAdminVerified}
                                />
                                <Input
                                    label="Color"
                                    name="color"
                                    //iconName="user-circle-o"
                                    value={this.state.fields.color}
                                    onChangeText={text => this.handleChange(text, 'color')}
                                    // errorMessage={this.state.errors['last_name']}
                                    // ref={ref => (this.customInput2 = ref)}
                                    // refInner="innerTextInput2"
                                    // onSubmitEditing={() =>
                                    //     this.customInput3.refs.innerTextInput3.focus()
                                    // }
                                    returnKeyType="next"
                                    blurOnSubmit={false}
                                    // editable = {this.state.isAdminVerified}
                                />

                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <TouchableCmp disabled={this.state.isDisabled}
                                    // background={TouchableNativeFeedback.Ripple("#fca600")}
                                    onPress={this.handleSubmit}>
                                    <View
                                        style={[this.state.isDisabled == false ? style.button : style.disabledButton]}>
                                        <Text style={style.buttonTxt}>
                                            Update
                                            </Text>
                                    </View>
                                </TouchableCmp>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
        )
    }
}
