import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Image, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { Avatar, Icon, CheckBox } from 'react-native-elements'
import styles from './style'
import ImagePicker from "react-native-image-crop-picker";
import api from "../../Api/api"
import imageUrl from '../../Api/config';

export default class AccountScreen extends Component {
      constructor(props) {
            super(props);
            this.state = {
                  userDetails: '',
                  userName: '',
                  profileDetails: [],
                  tripRating: '',
                  UniversityDetails: [],
                  token: this.props.navigation.state.params.token,
                  userId: this.props.navigation.state.params.userId,
                  profile_picture: [],
                  profilePicture: '',
                  mimeType: '',
                  fileName: '',
                  drivingLicence: [],
                  drivingLicenseFront: '',
                  drivingLicenseBack: '',
                  drivingLicenceFrontName: '',
                  drivingLicenceBackName: ''

            }
      }

      // get the user details at componentDidMount
      componentDidMount() {
            AsyncStorage.getItem('UserDetails').then(res => {
                  if (res) {
                        this.setState({ userDetails: JSON.parse(res) })
                        this.setState({
                              userName: this.state.userDetails.data.name,
                              token: this.state.userDetails.token
                        })
                        api.getUserDetails('user/' + this.state.userId, this.state.token).then((result) => {
                              if (result.status == 200) {
                                    this.setState({
                                          tripRating: result.data,
                                          profileDetails: result.data.user,
                                          UniversityDetails: result.data.user.universityId,
                                          profile_picture: result.data.user.profilePicture,
                                          drivingLicenseFront: result.data.user.drivingLicence.length > 0 && result.data.user.drivingLicence[0] ? result.data.user.drivingLicence[0].path : '',
                                          drivingLicenseBack: result.data.user.drivingLicence.length > 0 && result.data.user.drivingLicence[1] ? result.data.user.drivingLicence[1].path : '',
                                    });

                              }
                        }).catch((err) => {

                        });
                  }
            }).catch = (err) => {

            }

      }
      // Profile Image Upload ---start

      showActionSheet = () => {
            Alert.alert(
                  'Choose Image',
                  '',
                  [
                        { text: 'Gallery', onPress: () => this.chooseGalleryImage() },
                        {
                              text: 'Cancel',
                              style: 'cancel',
                        },
                        { text: 'Camera', onPress: () => this.chooseCameraImage() },
                  ],
                  { cancelable: false },
            );

      }

      chooseCameraImage() {
            ImagePicker.openCamera({
                  width: 400,
                  height: 300,
                  cropping: true,
                  includeBase64: true
            }).then(image => {
                  this.setState({ profilePicture: 'data:' + image.mime + ';base64,' + image.data, fileName: image.path.substring(image.path.lastIndexOf('/') + 1, image.path.length), mimeType: image.mime });
                  this.uploadImage(this.state)
            });
      }

      chooseGalleryImage() {
            ImagePicker.openPicker({
                  width: 300,
                  height: 400,
                  cropping: true,
                  includeBase64: true
            }).then(image => {
                  this.setState({ profilePicture: 'data:' + image.mime + ';base64,' + image.data, fileName: image.path.substring(image.path.lastIndexOf('/') + 1, image.path.length), mimeType: image.mime });
                  this.uploadImage(this.state)
            });
      }

      uploadImage = (input) => {
            var data = {
                  profilePicture: this.state.profilePicture,
                  filename: this.state.fileName
            }

            api.put('user/imageUpload/base64/' + this.state.userDetails.data._id, data, this.state.token).then((upload) => {
                  this.setState({
                        profile_picture: upload.data
                  })
                  Alert.alert(upload.msg)
            }).catch(err => {
                  Alert.alert('File size is too large')
                  console.log('err', err)
            })

      }
      // Profile Image Upload ---end

      // Driving License Upload ---start      
      showActionSheetForDrivingLicense = (type) => {
            Alert.alert(
                  'Choose Image',
                  '',
                  [
                        { text: 'Gallery', onPress: () => this.chooseGalleryImageForDrivingLicense(type) },
                        {
                              text: 'Cancel',
                              style: 'cancel',
                        },
                        { text: 'Camera', onPress: () => this.chooseCameraImageForDrivingLicense(type) },
                  ],
                  { cancelable: false },
            );
      }

      chooseCameraImageForDrivingLicense(type) {
            ImagePicker.openCamera({
                  width: 400,
                  height: 300,
                  cropping: true,
                  includeBase64: true
            }).then(image => {
                  if (type == "front") {
                        this.setState({ drivingLicenseFront: 'data:' + image.mime + ';base64,' + image.data, drivingLicenceFrontName: image.path.substring(image.path.lastIndexOf('/') + 1, image.path.length), drivingLicensePathF: image.path });
                        this.uploadImageForDrivingLicensefront();
                  } else {
                        this.setState({ drivingLicenseBack: 'data:' + image.mime + ';base64,' + image.data, drivingLicenceBackName: image.path.substring(image.path.lastIndexOf('/') + 1, image.path.length), drivingLicensePathB: image.path });
                        this.uploadImageForDrivingLicenseback();

                  }

            });
      }

      chooseGalleryImageForDrivingLicense(type) {
            ImagePicker.openPicker({
                  width: 300,
                  height: 400,
                  cropping: true,
                  includeBase64: true
            }).then(image => {
                  if (type == "front") {
                        this.setState({ drivingLicenseFront: 'data:' + image.mime + ';base64,' + image.data, drivingLicenceFrontName: image.path.substring(image.path.lastIndexOf('/') + 1, image.path.length), drivingLicensePathF: image.path });
                        this.uploadImageForDrivingLicensefront();
                  } else {
                        this.setState({ drivingLicenseBack: 'data:' + image.mime + ';base64,' + image.data, drivingLicenceBackName: image.path.substring(image.path.lastIndexOf('/') + 1, image.path.length), drivingLicensePathB: image.path });
                        this.uploadImageForDrivingLicenseback();
                  }

            });
      }

      uploadImageForDrivingLicensefront = (image, imageName) => {
            var data = {
                  drivingLicenceFront: this.state.drivingLicenseFront ? this.state.drivingLicenseFront : '',
            }
            api.put('user/drivingLicenceUpload/base64Front/' + this.state.userDetails.data._id, data, this.state.token).then((upload) => {
                  Alert.alert(upload.msg)
            }).catch(err => {
                  Alert.alert('File size is too large')
                  console.log('err', err)
            })
      }

      uploadImageForDrivingLicenseback = (image, imageName) => {

            var data = {
                  drivingLicenceBack: this.state.drivingLicenseBack ? this.state.drivingLicenseBack : ''
            }
            api.put('user/drivingLicenceUpload/base64Back/' + this.state.userDetails.data._id, data, this.state.token).then((upload) => {
                  Alert.alert(upload.msg)
            }).catch(err => {
                  Alert.alert('File size is too large')
                  console.log('err', err)
            })
      }
      // Driving License Upload ---end


      render() {
            return (
                  <View style={styles.container}>
                        <View style={styles.logoDiv}>
                              <Image source={require("../../assets/imsges/logo.png")} style={styles.logo} />
                        </View>
                        <ScrollView style={{backgroundColor: "#fff"}} showsVerticalScrollIndicator={false}>
                              <View style={{ top: 5, right: '4%' }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate("EditProfile", { userDetails: this.state.profileDetails, UniversityDetails: this.state.UniversityDetails, token: this.state.token })}>
                                          <Text style={styles.editButtonTxt}>Edit</Text>
                                    </TouchableOpacity>

                              </View>
                              <View style={{ ...styles.profile, backgroundColor: '#0000' }}>
                                    <View>
                                          {this.state.profileDetails.profilePicture ?
                                                <Avatar size="xlarge" rounded source={{ uri: this.state.profileDetails.profilePicture.includes('/userImage') ? imageUrl.img_url+ this.state.profileDetails.profilePicture : this.state.profileDetails.profilePicture}} />
                                                : <Avatar size="xlarge" rounded source={require("../../assets/images/user.png")} />

                                          }
                                          <TouchableOpacity style={{ position: 'absolute', bottom: 0, right: 0 }} onPress={() => this.showActionSheet()}>
                                                <Image source={require("../../assets/imsges/edit.png")} style={{ width: 40, height: 40 }} />
                                          </TouchableOpacity>
                                    </View>
                                    <Text style={styles.username}>{this.state.profileDetails.name}</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                          <Text style={styles.mtext}> Rating:{this.state.tripRating.averageRating ? Math.round(this.state.tripRating.averageRating) : 0} </Text>
                                          <Text style={styles.mtext}> Trips:{this.state.tripRating.totalTrip} </Text>
                                    </View>

                              </View>

                              <View style={styles.details}>
                                    <View style={styles.row}>
                                          <CheckBox
                                                disabled
                                                checked={this.state.profileDetails.isLicenceVerified}
                                                title='Verified ID'
                                                containerStyle={styles.chk}
                                                textStyle={styles.label} />

                                          <CheckBox
                                                disabled
                                                title='Verified University'
                                                checked={this.state.profileDetails.isAdminVerified}
                                                containerStyle={styles.chk} textStyle={styles.label} />
                                    </View>

                                    <View style={styles.info}>
                                          <View style={styles.row}>
                                                <Text style={styles.label2}>University</Text>
                                                <Text style={styles.labeltext}>{this.state.UniversityDetails.universityName ? this.state.UniversityDetails.universityName : '.................'}</Text>
                                          </View>
                                          <View style={[styles.row, {width:'100%'}]}>
                                                <Text style={[styles.label2, {width:'10%'}]}>Bio</Text>
                                                <Text style={[styles.labeltext, {textAlign:'right',width:'90%'}]}>{this.state.profileDetails.aboutMe ? this.state.profileDetails.aboutMe : '.................'}</Text>
                                          </View>
                                          <View style={styles.row}>
                                                <Text style={styles.label2}>Make/Model</Text>
                                                <Text style={styles.labeltext}>{this.state.profileDetails.model ? this.state.profileDetails.model : '.................'}</Text>
                                          </View>
                                          <View style={styles.row}>
                                                <Text style={styles.label2}>Number Plate</Text>
                                                <Text style={styles.labeltext}>{this.state.profileDetails.carDetails ? this.state.profileDetails.carDetails : '.................'}</Text>
                                          </View>
                                          <View style={styles.row}>
                                                <Text style={styles.label2}>Colour</Text>
                                                <Text style={styles.labeltext}>{this.state.profileDetails.color ? this.state.profileDetails.color : '.................'}</Text>
                                          </View>
                                          <View style={styles.row}>
                                                <Text style={styles.label2}>Driving Licence</Text>
                                          </View>
                                          {/* <View style={styles.frontBack}>
                                                <Text style={styles.frontBackText}>Front</Text>
                                                <Text style={styles.frontBackText}>Back</Text>
                                          </View> */}

                                          <View style={styles.row}>
                                                <View >
                                                      <Text style={styles.frontBackText}>Front</Text> 
                                                      {this.state.drivingLicenseFront != '' ?
                                                            <Image style={{width:150, height:100}} source={{ uri: this.state.drivingLicenseFront }} />
                                                            : <Image  style={{width:150, height:100}} source={require("../../assets/imsges/licence.jpg")} />
                                                      }                                                                                                                                                                                              
                                                <TouchableOpacity style={{ position: 'absolute', bottom: 15, left: 9, }} onPress={() => this.showActionSheetForDrivingLicense('front')}>
                                                      <Image source={require("../../assets/imsges/edit.png")} style={{ width: 35, height: 35, }} />
                                                </TouchableOpacity> 
                                                </View>
                                                <View>
                                                <Text style={styles.frontBackText}>Back</Text>
                                                {this.state.drivingLicenseBack != '' ?
                                                      <Image style={{width:150, height:100}}  source={{ uri: this.state.drivingLicenseBack }} />
                                                      : <Image style={{width:150, height:100}}  source={require("../../assets/imsges/licence.jpg")} />
                                                }                                                
                                                <TouchableOpacity style={{ position: 'absolute', bottom: 15, right: 9 }} onPress={() => this.showActionSheetForDrivingLicense('back')}>
                                                      <Image source={require("../../assets/imsges/edit.png")} style={{ width: 35, height: 35 }} />
                                                </TouchableOpacity>
                                                </View> 
                                          </View>
                                    </View>
                              </View>
                        </ScrollView>
                  </View>
            )
      }
}
