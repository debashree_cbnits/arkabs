import React, { Component } from 'react'
import { Text, View,  ScrollView, TouchableOpacity, Image, Alert } from 'react-native'
import ActionSheet from 'react-native-actionsheet'
import AsyncStorage from '@react-native-community/async-storage';
import {Avatar, Icon, } from 'react-native-elements'
import styles from './style';
// import ImagePicker from 'react-native-image-picker';
import ImagePicker from "react-native-image-crop-picker";
import api from '../../Api/api';
import config from '../../Api/config';
import imageUrl from '../../Api/config';

var BUTTONS = [
  { text: 'camera', icon: "ios-camera", iconColor: "#2c8ef4" },
  { text: 'file', icon: "ios-images", iconColor: "#f42ced" }
];


export default class SettingScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            userDetails:'',
            userName:'',
            fileName: '',
            userImage:'',
            profile_picture:'',
            userId:'',
            token:'',
                      
        }
      
    }

    componentDidMount(){
      this.props.navigation.addListener('willFocus', payload  => {
        api.getUserDetails('user/' + this.state.userId, this.state.token).then((result) => {
           if (result.status == 200) {
                this.setState({
                        profile_picture:result.data.user.profilePicture,
                        userName:result.data.user.name
                });
               
          }
    }).catch((err) => {
          console.log('error', err)
    });
      });
         AsyncStorage.getItem('UserDetails').then(res=>{
            if(res){
             this.setState({userDetails:JSON.parse(res)});
              this.setState({userName:this.state.userDetails.data.name,
              userId:this.state.userDetails.data._id,
              token:this.state.userDetails.token});
              
              api.getUserDetails('user/' + this.state.userId, this.state.token).then((result) => {
                
                if (result.status == 200) {
                       this.setState({
                              profile_picture:result.data.user.profilePicture,
                              userName:result.data.user.name
                      });
                     
                }
          }).catch((err) => {
                console.log('error', err)
          });
            }

            }).catch=(err)=>{
                console.log(err)
            }
    }

    signOut = () =>{
      api.logOut('user/logout/'+this.state.userId,this.state.token).then(Result => {
        AsyncStorage.removeItem("UserDetails")
        .then(clear =>{
                this.setState({userDetails:clear})
                setTimeout(()=>{
                    this.props.navigation.navigate("Login")
                },1000)
        }).catch=(err)=>{
            console.log(err)
        }
           Alert.alert(Result.msg)
           this.props.navigation.navigate('Account')
  }).catch(err => {
     console.log('error',err)
  })           
        
    }
  
  render() {
     return (
       
      <>
              <View style={styles.logoDiv}>
                  <Image source={require("../../assets/imsges/logo.png")} style={styles.logo}  />
              </View>
              <ScrollView>
              <View style={styles.profile}>
               <TouchableOpacity disabled={true}>
                 {this.state.profile_picture?
                 <Avatar size="xlarge" rounded  source={{ uri: this.state.profile_picture.includes('/userImage')? imageUrl.img_url+this.state.profile_picture : this.state.profile_picture}}/>
                :
                <Avatar size="xlarge" rounded source={require("../../assets/images/user.png")}/>
                }
                 
               </TouchableOpacity>
               {/* <ActionSheet
                  ref={o => this.ActionSheet = o}
                  title={'Choose Image'}
                  options={['gallery','camera','cancel']}
                  cancelButtonIndex={2}
                  destructiveButtonIndex={2}
                  onPress={(index) => { 
                    this.fileUploadType(index);
                      }} 
                />    */}
                <Text style={styles.username}>{this.state.userName}</Text>
              </View>
              
                <View style={styles.container}>
                  <View style={styles.prolist}>
                      <TouchableOpacity style={styles.menulink} onPress={()=>this.props.navigation.navigate("Profile",{userId:this.state.userId,token:this.state.token})}>
                          <Text style={styles.linktext}>Profile</Text>
                          <Icon type="ionicon" name='md-person-add'  color='#777' size={30} />
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.menulink} onPress={()=>this.props.navigation.navigate("BankAccount")}>
                          <Text style={styles.linktext}>Payment</Text>
                          <Icon type="font-awesome" name='credit-card'  color='#777' size={30} />
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.menulink} onPress={()=>this.props.navigation.navigate("AboutUs")}>
                          <Text style={styles.linktext}>About Us</Text>
                          <Icon type="ionicon" name='md-person'  color='#777' size={30} />
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.menulink} onPress={()=>this.props.navigation.navigate("Feedback")}>
                          <Text style={styles.linktext}>Feedback</Text>
                          <Icon type="font-awesome" name='pencil'  color='#777' size={30} />
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.menulink} onPress={()=>this.props.navigation.navigate("ContactUs")}>
                          <Text style={styles.linktext}>Contact Us</Text>
                          <Icon type="font-awesome" name='paper-plane'  color='#777' size={30} />
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.menulink} onPress={()=>this.props.navigation.navigate("ChangePassword")}>
                          <Text style={styles.linktext}>Change Password</Text>
                          <Icon type="ionicon" name='ios-unlock'  color='#777' size={30} />
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.menulink} onPress={this.signOut}>
                          <Text style={styles.linktext}>Sign Out</Text>
                          <Icon type="ionicon" name='ios-log-out'  color='#777' size={30} />
                      </TouchableOpacity>                     
                  </View>
                  </View>
              </ScrollView> 
      </>
    )
  }
}
