export default {
    container: {
        paddingLeft: 10,
        paddingRight: 10,
        flex: 1,
        backgroundColor: '#fff'
    },
    logoDiv: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    logo: {
        // width: '60%',
        // height: 120
        width:180,
        height:80,
        resizeMode:'cover',
        marginTop:20,
        marginBottom: 15  
    },
    // lefttop:{
    //     width: 50,
    //     overflow: 'hidden',
    //     //textOverflow: ellipsis, 

    // },
    // righttop:{
    //     width: 50,
    //     overflow: 'hidden',
    // },
    mainheading: {
        color: '#444',
        fontSize: 24,
        textAlign: 'center',
        marginBottom: 20,
        fontFamily: 'Oswald-Regular',
        paddingTop: 5,
        // fontFamily: 'Oswald-Bold'
        
    },
    btntext: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '900',
        fontSize: 24,
        fontFamily: 'Oswald-Regular'
    },
    orangebtn: {
        marginTop: 10,
        paddingTop: 5,
        paddingBottom: 10,
        backgroundColor: '#fca600',
        borderRadius: 10,
        // borderRadius: 35,
        marginBottom: 10
    },
    disableOrangebtn: {
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#808080',
        borderRadius: 10,
        marginBottom: 10
    },
    blutext: {
        width: '15%',
        color: '#377de1',
        fontSize: 20,
        fontFamily: 'Oswald-Bold',
        marginLeft: 10

    },
    orangetext: {
        width: '15%',
        color: '#fca600',
        fontSize: 20,
        fontFamily: 'Oswald-Bold',
        marginLeft: 10

    },
    pickerin: {
        width: "80%",
        color: '#555',
        fontSize: 18,
        fontFamily: 'Oswald-Regular'
    },
    pickera: {
        width: "100%",
        color: '#555',
        fontFamily: 'Oswald-Regular'
    },
    pickerviewcard: {
        height: 60,
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 4,
        backgroundColor: '#fff',
        marginTop: 6,
        marginBottom: 6,
        marginLeft: 6,
        marginRight: 6,
        overflow: 'hidden',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'

    },
    pickerview: {
        width: "100%",
        height: 50,
        borderWidth: 2,
        borderColor: '#ddd',
        borderRadius: 5,
        marginTop: 30
    },
    list: {
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.5,
        // shadowRadius: 2,
        // elevation: 4,       
        // minHeight: 50,
        // marginBottom: 8,
        // marginTop: 8,
        // backgroundColor: '#fff'
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 4,
        backgroundColor:'#fff',
        minHeight:50,
        marginVertical:8,
        borderRadius: 5,
        width: '96%',
        marginHorizontal: '2%'  
    },
    listinner: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15
    },
    locationName: {
        flexDirection: 'row',
        alignItems: 'center',
        overflow: 'hidden',
    },
    ltext: {
        color: '#777',
        fontSize: 15,
        fontFamily: 'Oswald-Bold',
        // width: 50

    },
    rtext: {
        color: '#777',
        fontSize: 15,
        fontFamily: 'Oswald-Bold',
        // width: 50

    },
    micon: {
        marginLeft: 10,
        marginRight: 10,
    },
    datetime: {
        color: '#999',
        fontSize: 14,
        fontFamily: 'Oswald-Bold'
    },
    righttop: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '44%',
        paddingLeft: 15,
        justifyContent: 'space-between',
    },
    btnlike: {
        padding: 3,
        color: '#1b87ff',
        borderColor: '#1b87ff',
        borderWidth: 2,
        borderRadius: 4,
        textAlign: 'center',
        width: 80,
        fontSize: 16,
        fontFamily: 'Oswald-Regular'
    },
    btnlikey: {
        padding: 3,
        color: '#fbb122',
        borderColor: '#fbb122',
        borderWidth: 2,
        borderRadius: 4,
        textAlign: 'center',
        width: 80,
        fontSize: 16,
        fontFamily: 'Oswald-Regular'
    },
    price: {
        fontSize:18,
        color:'#fbb122',
        marginLeft:0,
        marginRight:10,
        fontFamily: 'Oswald-Bold'
    },
    flexwrap: {
        flexDirection: 'row',
    },
    avtl: {
        marginLeft: -20
    },
    blubtn: {
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#007aff',
        borderRadius: 10,
    },
    imagebg: {
        width: '100%',
        flex: 1,
        height: 1080,
        paddingTop: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainimg: {
        width: "100%",
        height: 150,

    },
    button: {
        backgroundColor: "#3498db",
        width: "80%",
        marginVertical: 10,
        borderRadius: 20,
        paddingTop:10,
        paddingBottom:10,
        paddingLeft:8,
        paddingRight:8,
        alignItems: 'center',
        elevation: 3
    },
    buttonTxt: {
        color: 'white',
        fontFamily: 'Oswald-Bold',
        fontSize: 25
    },
    disabledButton: {
        backgroundColor: "#808080",
        width: "80%",
        marginVertical: 0,
        borderRadius: 20,
        paddingTop:10,
        paddingBottom:10,
        paddingLeft:8,
        paddingRight:8,
        alignItems: 'center',
        elevation: 3
    },
}