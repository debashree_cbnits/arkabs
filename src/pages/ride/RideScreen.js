import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, Picker, Alert, } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import DatePicker from "react-native-datepicker";
import styles from './style'
import { colors } from "../../assets/StylesGlobal/globalColors"
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import api from '../../Api/api'
import Spinner from 'react-native-loading-spinner-overlay';

export default class Rides extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      token: '',
      date: '',
      time:'',
      fromLocationLat: 0.00,
      fromLocationLong: 0.00,
      fromLocationName: '',
      fromLocation: { type: 'Point', coordinates: [] },
      toLocationLat: 0.00,
      toLocationLong: 0.00,
      toLocationName: '',
      toLocation: { type: 'Point', coordinates: [] },
      loader: false
    };

  }

  componentDidMount() {
    AsyncStorage.getItem('UserDetails', (err, result) => {
      let parseData = JSON.parse(result)
      this.setState({
        userId: parseData.data._id,
        token: parseData.token
      })
    });
  }

  searchRide = () => {
    let data = {
      toLocationName: this.state.toLocationName,
      toLocation: {
        type: 'Point',
        coordinates: [this.state.toLocationLong, this.state.toLocationLat]
      }, 
      fromLocationName: this.state.fromLocationName,
      fromLocation: { type: 'Point', coordinates: [this.state.fromLocationLong, this.state.fromLocationLat] }, // [latitude, longitude]
      userId: this.state.userId,
      amount: this.state.amount,
      availableSeat: this.state.availableSeat,
      tripDate: this.state.date,
      tripTime: this.state.time
    }

    if (!this.state.toLocationLat && !this.state.toLocationLong && !this.state.fromLocationLat && !this.state.fromLocationLong &&  !this.state.date && !this.state.time){
      this.searchAllRide()
    } else {

    api.getRides('trip?toLatt=' + this.state.toLocationLat + '&toLong=' + this.state.toLocationLong + '&fromLatt=' + this.state.fromLocationLat + '&fromLong=' + this.state.fromLocationLong + '&date=' + this.state.date + '&time'
      , this.state.token).then((result) => {
        this.setState({ loader: false })
        if (result.status == 200) {
          this.props.navigation.navigate('journey', { 
            searchResult: result.data, 
            bookingFrom: this.state.fromLocationName, 
            bookingTo: this.state.toLocationName, 
            token: this.state.token,
            isShowingBookingFee : result.isShowingBookingFee
          })
        } else {
          Alert.alert(result.msg)
        }
      }).catch((err) => {
        this.setState({ loader: false })
        console.log('error', err)
      });
    }

  }

  searchAllRide = () => {
    this.setState({ loader: true })
    api.getRides('trip?toLatt=&toLong=&fromLatt=&fromLong=&date', this.state.token).then((result) => {
      this.setState({ loader: false })        
        if (result.status == 200) {
          this.props.navigation.navigate('journey', { 
            searchResult: result.data, 
            bookingFrom: this.state.fromLocationName, 
            bookingTo: this.state.toLocationName, 
            token: this.state.token,
            bookingFeePercent:result.bookingFeePercent,
            isShowingBookingFee : result.isShowingBookingFee
          })
        } else {
          Alert.alert(result.msg)
        }
      }).catch((err) => {
        this.setState({ loader: false })
        console.log('error', err)
      });

  }

  render() {
    return (
      <>
      <View style={styles.logoDiv}>
            <Image source={require("../../assets/imsges/logo.png")} style={styles.logo} />
          </View>
      
        <ScrollView style={{backgroundColor:'#fff'}} keyboardShouldPersistTaps={'always'}>
        <View style={styles.container}>
          <Spinner visible={this.state.loader}
            textContent={'Loading...'}
            textStyle={{ color: '#FFF' }} />
          <Text style={styles.mainheading}>Find A Journey</Text>

          <View>
            <View>
              <GooglePlacesAutocomplete
                placeholder={this.state.fromLocationName ? this.state.fromLocationName : 'Select your location'}
                minLength={2} // minimum length of text to search
                autoFocus={false}
                returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                listViewDisplayed={false}    // true/false/undefined
                fetchDetails={true}
                renderLeftButton={() => <Text style={styles.blutext}>From</Text>}
                renderDescription={(row) => row.structured_formatting.main_text}
                onPress={(data, details = null) => {
                 if (details.geometry.location) {
                    this.setState({
                      fromLocationLat: details.geometry.location.lat,
                      fromLocationLong: details.geometry.location.lng,

                    });

                  }
                  if (data) {
                    this.setState({
                      fromLocationName: data.structured_formatting.main_text
                    });
                  }
                }}

                getDefaultValue={() => ''}
                query={{
                  // available options: https://developers.google.com/places/web-service/autocomplete
                  // key: 'AIzaSyCaZ7DeAIwVCOzQjPHyOs6ZRWMBB3R-F3A',
                  key: 'AIzaSyDdzB09qeDT4xMloFsz0KFVOaivyWMSZDg',
                  language: 'en', // language of the results
                  components : 'country:uk',
                }}
                styles={{
                  textInputContainer: {
                    height: 60,
                    borderRadius: 5,
                    shadowColor: '#000',
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.5,
                    shadowRadius: 2,
                    elevation: 4,
                    backgroundColor: '#fff',
                    marginTop: 6,
                    marginBottom: 6,
                    marginLeft: 6,
                    marginRight: 6,
                    overflow: 'hidden',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                  },
                  textInput: {
                    fontSize: 15,
                    color: `${colors.textColor}`,
                    fontFamily: 'Oswald-Regular'
                  }

                }}
                currentLocation={false}
                GooglePlacesSearchQuery={{
                  // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                  rankby: 'distance',
                  types: 'food'
                }}

                debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
              />
            </View>
            <View>
              <GooglePlacesAutocomplete
                placeholder={this.state.toLocationName ? this.state.toLocationName : 'Select your destination'}
                minLength={2} // minimum length of text to search
                autoFocus={false}
                returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                listViewDisplayed={false}    // true/false/undefined
                fetchDetails={true}
                renderLeftButton={() => <Text style={styles.blutext}>To</Text>}
                renderDescription={(row) => row.structured_formatting.main_text}
                onPress={(data, details = null) => {
                  if (details.geometry.location) {
                    this.setState({
                      toLocationLat: details.geometry.location.lat,
                      toLocationLong: details.geometry.location.lng,

                    });

                  }
                  if (data) {
                    this.setState({
                      toLocationName: data.structured_formatting.main_text
                    });
                  }
                }}
                getDefaultValue={() => {
                  return ''; // text input default value
                }}
                query={{
                  // available options: https://developers.google.com/places/web-service/autocomplete
                  // key: 'AIzaSyCaZ7DeAIwVCOzQjPHyOs6ZRWMBB3R-F3A',
                  key: 'AIzaSyDdzB09qeDT4xMloFsz0KFVOaivyWMSZDg',
                  language: 'en', // language of the results
                  components : 'country:uk',
                }}
                styles={{
                  textInputContainer: {
                    height: 60,
                    borderRadius: 5,
                    shadowColor: '#000',
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.5,
                    shadowRadius: 2,
                    elevation: 4,
                    backgroundColor: '#fff',
                    marginTop: 6,
                    marginBottom: 6,
                    marginLeft: 6,
                    marginRight: 6,
                    overflow: 'hidden',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                  },
                  textInput: {
                    fontSize: 15,
                    color: `${colors.textColor}`,
                    fontFamily: 'Oswald-Regular'
                  }
                }}

                currentLocation={false}

                GooglePlacesSearchQuery={{
                  // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                  rankby: 'distance',
                  types: 'food'
                }}

                filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
              />
            </View>
            <DatePicker
              ref={(ref) => this.datePickerRef = ref}
              style={{ width: 0, height: 0 }}
              date={this.state.datetime}
              mode="date"
              format="YYYY-MM-DD"
              minDate={new Date()}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              showIcon={false}
              hideText={true}
              onDateChange={(date) => {
                this.setState({ date: date })
              }}

            />
            <TouchableOpacity onPress={() => this.datePickerRef.onPressDate()} style={styles.pickerviewcard}>
              <Text style={styles.blutext}>Date</Text>
              <View style={{ width: "100%", alignItems: "flex-start", borderWidth: 0, marginStart: 38, paddingHorizontal: 20 }}>
                <Text style={{ fontSize: 20, color: `${colors.textColor}`, fontFamily: 'Oswald-Regular' }}>{this.state.date}</Text>
              </View>
            </TouchableOpacity>
          </View>
          </View>
        </ScrollView>
        <View style={{paddingHorizontal: '4%', backgroundColor:'#fff'}}>
        <TouchableOpacity style={styles.orangebtn} onPress={() => this.searchRide()} >
          <Text style={styles.btntext}>Search</Text>
        </TouchableOpacity>
        </View>
      </>

    )
  }
}




