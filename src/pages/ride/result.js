import React, { Component } from 'react'
import { View, Text, SafeAreaView, Image, TouchableOpacity, TouchableNativeFeedback, Alert, ScrollView } from 'react-native'
import { GlobalStyles } from '../../assets/StylesGlobal/GlobalStyle'
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import BottomSheet from 'reanimated-bottom-sheet'
// import Polyline from '@mapbox/polyline';
import style from './style';
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import { connect } from "react-redux"
import Spinner from 'react-native-loading-spinner-overlay';
import api from '../../Api/api'
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

export class result extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loader: false,
      token: '',
      tripDate: this.props.navigation.state.params.selectedItem.driveDate,
      tripTime: this.props.navigation.state.params.selectedItem.driveTime,
      tripNumber: this.props.navigation.state.params.selectedItem.tripNumber,
      origin: {
        latitude: this.props.navigation.state.params.selectedItem.fromLocation.coordinates[1],
        longitude: this.props.navigation.state.params.selectedItem.fromLocation.coordinates[0]
      },
      destination: {
        latitude: this.props.navigation.state.params.selectedItem.toLocation.coordinates[1],
        longitude: this.props.navigation.state.params.selectedItem.toLocation.coordinates[0]
      },
      driveId: this.props.navigation.state.params.selectedItem._id,
      toLocationName: this.props.navigation.state.params.bookingTo,
      toLocation: {
        type: "Point",
        coordinates: []
      },
      fromLocationName: this.props.navigation.state.params.bookingFrom,
      fromLocation: {
        type: "Point",
        coordinates: []
      },
      userId: '',
      isDisabled: false,
      bookingAmount: this.props.navigation.state.params.selectedItem.amount,
      bookingFeePercent: this.props.navigation.state.params.bookingFeePercent,
      bookigFee: 0,
      isShowingBookingFee  : this.props.navigation.state.params.bookingFeePercent ? this.props.navigation.state.params.isShowingBookingFee : '',
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('UserDetails', (err, result) => {
      let parseData = JSON.parse(result)
      this.setState({
        userId: parseData.data._id,
        token: parseData.token
      })
    });
  }

  rideBooking = () => {
    let data = {
      driveId: this.state.driveId,
      toLocationName: this.state.toLocationName,
      toLocation: {
        type: "Point",
        coordinates: [this.state.destination.longitude, this.state.destination.latitude]
      },
      fromLocationName: this.state.fromLocationName,
      fromLocation: {
        type: "Point",
        coordinates: [this.state.origin.longitude, this.state.origin.latitude]
      },
      userId: this.state.userId

    }
    this.setState({ loader: true })
    api.POST('trip-api/trip/booking', data, this.state.token).then((result) => {
      this.setState({ loader: false })
      if (result.status == 200) {
        this.props.navigation.navigate('CardDetails', { transactionId: result.transactionId, tripNumber: this.state.tripNumber, tripDetails: this.props.navigation.state.params.selectedItem })
      } else {
        Alert.alert(result.msg)
      }
    }).catch((err) => {
      console.log('error', err)
      this.setState({ loader: false })
      console.log('error', err)
    });

  }


  renderContent = () => {
    return (
      <View style={{ height:500 }}>
        <ScrollView>
          <View style={{ alignItems: 'center', flex:1, width:'100%' }}>
            <Text style={{ marginTop: 10, fontSize: 15 }}>Price<Text style={{ color: '#fcba03', fontSize: 15, }}>  {Math.round(this.state.bookingAmount)}</Text></Text>
            <Text style={{ marginTop: 10, fontSize: 15 }}>{this.state.bookingFeePercent}% Booking Fee<Text style={{ color: '#fcba03', fontSize: 15, }}>  {Math.round((this.state.bookingFeePercent / 100) * this.state.bookingAmount)}</Text></Text>
            <Text style={{ marginTop: 10, fontSize: 15 }}>Total<Text style={{ color: '#fcba03', fontSize: 15, }}>  {Math.round(this.state.bookingAmount + Math.round((this.state.bookingFeePercent / 100) * this.state.bookingAmount))}</Text></Text>
            <Text style={{ marginTop: 10, fontSize: 15 }}>{this.state.fromLocationName}</Text>
            <Icon style={{ marginTop: 10 }} name="arrow-down" size={30} />
            <Text style={{ marginTop: 10, fontSize: 15 }}>{this.state.toLocationName}</Text>
            <Text style={{ color: '#fcba03', marginTop: 10, fontSize: 15 }}>{moment(this.state.tripDate).format("D MMM YYYY")}{" " + this.state.tripTime}</Text>
            <TouchableOpacity disabled={this.state.isDisabled} background={TouchableNativeFeedback.Ripple("#fca600")} onPress={() => this.rideBooking()}>
              <View
                style={[this.state.isDisabled == false ? style.button : style.disabledButton]}>
                <Text style={style.buttonTxt}>
                  Confirm booking
            </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{ marginTop: 10 }} onPress={() => this.props.navigation.goBack()}>
              <Text style={{ color: '#3498db', fontSize: 15 }}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>


    )
  }


  render() {
    return (
      <React.Fragment>
        <SafeAreaView style={{ paddingHorizontal: 15 }}>
          <Spinner visible={this.state.loader}
            textContent={'Loading...'}
            textStyle={{ color: '#FFF' }} />
          <View style={{ ...GlobalStyles.logoDiv }}>
            <Image source={require("../../assets/imsges/logo.png")} style={GlobalStyles.logo} />
          </View>
          <ScrollView>
            <MapView style={{ height: 205, width:'100%' }}>
              <MapViewDirections
                origin={this.state.origin}
                destination={this.state.destination}
                apikey="AIzaSyBPn_p6JBWkIUSDzG0ke-0IGMNwrnfLpaY"
              />
            </MapView>
          </ScrollView>

        </SafeAreaView>
        <BottomSheet
          snapPoints={[200, 120, 20]}
          renderContent={this.renderContent}
        // renderHeader = {this.renderHeader}
        />
      </React.Fragment>
    )
  }
}
const mapStateToProps = state => {
  return {
    RideReducer: state.Ride,
  };
};
export default connect(mapStateToProps, null)(result)
