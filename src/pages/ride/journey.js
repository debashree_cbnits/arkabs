import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, } from 'react-native'
import { Avatar, Icon, Card } from 'react-native-elements'
import styles from './style';
import moment from 'moment';
import imageUrl from '../../Api/config';


export default class journey extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      token: this.props.navigation.state.params.token,
      fromLocationName: this.props.navigation.state.params.bookingFrom ? this.props.navigation.state.params.bookingFrom : '',
      toLocationName: this.props.navigation.state.params.bookingTo ? this.props.navigation.state.params.bookingTo : '',
      searchList: this.props.navigation.state.params.searchResult,
      bookingFeePercent: this.props.navigation.state.params.bookingFeePercent ? this.props.navigation.state.params.bookingFeePercent : '',      
      isShowingBookingFee  : this.props.navigation.state.params.isShowingBookingFee ? this.props.navigation.state.params.isShowingBookingFee : '',
    };    
  }

  render() {
    return (
      <View style={{ flex: 1,  backgroundColor:'#fff'}}>
        <View style={styles.logoDiv}>
          <Image source={require("../../assets/imsges/logo.png")} style={styles.logo} />
        </View>
        <Text style={styles.mainheading}>Find A Journey</Text>
        <ScrollView  style={{backgroundColor:'#fff'}}>
          {this.state.searchList.length != 0 ?
            this.state.searchList.map((item, key) => {
              // console.log(item, 'itemitemitemitem')
              return (
                <View style={styles.list}>
                  <TouchableOpacity style={styles.listinner} onPress={() =>
                     this.props.navigation.navigate('BookingMap', 
                     { selectedItem: item, 
                        bookingFrom: this.state.fromLocationName ? this.state.fromLocationName : item.fromLocationName, 
                        bookingTo: this.state.toLocationName ? this.state.toLocationName : item.toLocationName, 
                        token: this.state.token, 
                        bookingFeePercent: this.state.bookingFeePercent,
                        isShowingBookingFee : this.state.isShowingBookingFee
                      })}>
                      <View style={{...styles.lefttop, width: '60%', paddingRight: 15}}>
                        <View style={{...styles.locationName, flexWrap: 'wrap'}}>
                          <Text style={styles.ltext} numberOfLines={1}>{item.fromLocationName}</Text>
                          <Icon name='ios-arrow-round-forward' containerStyle={styles.micon} type='ionicon' size={30} color='#777' />
                          <Text style={styles.rtext} numberOfLines={1}>{item.toLocationName}</Text>
                        </View>
                        <Text style={styles.datetime}>{moment(item.driveDate).format("D MMM YYYY")}{" " + item.driveTime}</Text>
                      </View>
                    <View style={styles.righttop}>
                      <View style={styles.flexwrap}>
                        {item.userId.profilePicture ?
                          <Avatar size="medium" rounded source={{ uri: item.userId.profilePicture.includes('/userImage') ? imageUrl.img_url+item.userId.profilePicture : item.userId.profilePicture}} containerStyle={styles.avtl} />
                          : <Avatar size="medium" rounded source={{ uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg', }} containerStyle={styles.avtl} />
                        }
                      </View>
                      <Text style={styles.price} numberOfLines={1}>£{item.amount % 1 === 0 ? item.amount : Number(item.amount).toFixed(2)}</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              )
            })
            :
            <View >
              <View style={{alignItems: 'center', paddingTop: 20}}>
                <Text>{"No ride found"}</Text>
              </View>
            </View>}
          {/* {this.state.searchList.length != 0 ?
            <TouchableOpacity style={styles.blubtn} onPress={() => props.navigation.navigate("results")} disabled={true}>
              <Text style={styles.btntext}>Create Trip Alert</Text>
            </TouchableOpacity>
            : null
          } */}
        </ScrollView>

      </View>
    )
  }

}



