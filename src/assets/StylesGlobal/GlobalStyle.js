import React, {Component} from 'react';
import {StyleSheet, Platform} from 'react-native';
export const GlobalStyles=StyleSheet.create({
    logoDiv:{  
        // marginTop:20,   
        // width:'100%',
        justifyContent:'center',
        alignItems:'center',  
        // paddingbottom:20   ,
        // borderWidth:1,

    },
    logo:{
        // resizeMode:"center",
        width:"100%",
        height:200,
        // borderWidth:1,
        // borderColor:"red"
    },
    submitButton: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        display: 'flex',
        backgroundColor: '#2B55B1',
        padding: 9,
        marginTop: 20,
        marginHorizontal: '30%',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: {width: 3, height: 6},
        shadowRadius: 10,
        shadowOpacity: 0.3,
        elevation: 3,
      },
      picker:{
          marginStart:25,
          marginEnd:25,
          borderBottomWidth:1,
          borderBottomColor:"#aaa",
          flexDirection:"row",
          alignItems:"center",
          marginVertical:10,
      },
      picker2:{
        marginStart:25,
        marginEnd:25,
        flexDirection:"row",
        alignItems:"center",
        marginTop:10,
    },
    picker3:{
        marginStart:25,
        marginEnd:25,
        marginBottom: 10,
        borderWidth:1,
        borderColor:"#aaa",
        flexDirection:"row",
        alignItems:"center",
        marginTop:10,        
        borderRadius: 5
    },
      body:{
          fontFamily:'Oswald-Bold'
      }
})