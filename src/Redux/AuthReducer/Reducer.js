import {AUTH_INFO} from "./Type"
const initialState = {
    AuthDeails: {},
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTH_INFO:
            return {
                ...state,
                AuthDeails: action.payload
            };
        default:
            return state;
    }
}
export default reducer;