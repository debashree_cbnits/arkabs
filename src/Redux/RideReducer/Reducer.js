import {RIDE_INFO} from "./Type"
const initialState = {
    RideDetails: {},
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case RIDE_INFO:
            return {
                ...state,
                RideDetails: action.payload
            };
        default:
            return state;
    }
}
export default reducer;
