import { combineReducers } from 'redux' 
import RideReducer from "./RideReducer/Reducer"
import AuthReducer from "./AuthReducer/Reducer"
const rootReducer = combineReducers({
    Ride:RideReducer,
    Auth:AuthReducer,
})

export default rootReducer