import React, { Component } from 'react'    
import { Image, Text,View } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Icon from 'react-native-vector-icons/FontAwesome';
const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};
const styles={labelStyle: {
  position: 'absolute',
  left:52,
  top: 0,
  fontSize: 10,
  color: '#3498db',
},}
export class googlePlacesApi extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       isFocused:false
    }
  }
  
    render() {
      const label=this.state.isFocused?"Location":null
      const label2=!this.state.isFocused?"Location":null
      const labelStyle = styles.labelStyle;
        return (
            <View>
              <Text style={labelStyle}>{label}</Text>
                <GooglePlacesAutocomplete
      placeholder={label2}
      minLength={2} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
      listViewDisplayed={this.state.isFocused.toString()}    // true/false/undefined
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render
      onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
       this.props.getVal(data.description)
      }}
      textInputProps={{
        onFocus: () => this.setState({isFocused: true}),
        onBlur: () => this.setState({isFocused: false}),
     }}
      getDefaultValue={() => ''}

      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyCbMTT2CVS2GbGwOF-v8M4Umy_TG_qsu0M',
        language: 'en', // language of the results
        types: '(cities)' // default: 'geocode'
      }}

      styles={{
        textInputContainer: {
          padding:0,
          marginStart:25,
          marginEnd:25,
          borderTopWidth:0,
          backgroundColor:"transparent",
          borderBottomWidth:1,
          marginBottom:20
        },
        description: {
          fontWeight: '100'
        },
        predefinedPlacesDescription: {
          color: '#1faadb'
        },
        textInput: {
          margin:0,
          backgroundColor: "transparent",
          padding: 0,
          width: "100%",
          height:Platform.OS === 'ios' ? 30 : 30,
          fontSize: 15,
          color: '#000',
          // borderBottomWidth: 1,
          // borderBottomColor: '#ccc',
          marginTop: 15,
          // marginBottom:20,
          paddingLeft:20,
        },
        listView: {
          position: "absolute",
          backgroundColor: "#dfe6e9",
          opacity: 2,
          marginStart: 25,
          marginEnd: 25,
          marginTop: 50,
          zIndex: 30,
          elevation: 6,
          // borderWidth:1
        },
        poweredContainer: {
          backgroundColor: "transparent",
          height: "10%"
        },
        powered: { height: 10, width: 100 }
      }}

      currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
      currentLocationLabel="Current location"
      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        type: 'cafe'
      }}
      
      GooglePlacesDetailsQuery={{
        // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
        fields: 'formatted_address',
      }}

      filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
      predefinedPlaces={[homePlace, workPlace]}

      debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
      renderLeftButton={()  =><Icon
        name="map-marker"
        size={20}
        color="#3498db"
        style={{position:"absolute",top:19}}
      />}
    //   renderRightButton={() => <Text>Custom text after the input</Text>}
    />
            </View>
        )
    }
}

export default googlePlacesApi
