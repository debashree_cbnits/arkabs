import React from 'react';
import Navigation from "./Navigation/navigation"
import { Provider, connect } from 'react-redux';
import Store from "./src/Redux/store"
import PushNotification from "react-native-push-notification";

var firebaseConfig = {
  apiKey: "AIzaSyBT6pCnWjwSOoVknx-no1HlpuSg4JRkJkQ",
  authDomain: "arkabs-280713.firebaseapp.com",
  databaseURL: "https://arkabs-280713.firebaseio.com",
  projectId: "arkabs-280713",
  storageBucket: "arkabs-280713.appspot.com",
  messagingSenderId: "1031996217292",
  appId: "1:1031996217292:web:6a5415afdc702f2a356f69",
  measurementId: "G-MMZT0R9B7E"
};

export default class App extends React.Component {

  componentDidMount(){
    PushNotification.configure({     
      onRegister: function (token) {
        // console.log("TOKEN:", token);
      },

      onNotification: function (notification) {
        // console.log("NOTIFICATION:", notification);            
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

    })
  }

  render(){
  return (
    <Provider store={Store}>
      <Navigation />
    </Provider>
  )
  }
}
