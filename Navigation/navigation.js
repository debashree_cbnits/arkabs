import React from 'react';
import { Button, Text, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createAppContainer, createSwitchNavigator, NavigationActions, StackActions } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import Drive from '../src/pages/drive/DriveScreen';
import LocationAutoCompleteFrom from '../src/pages/drive/LocationAutocompleteFrom'
import Ride from '../src/pages/ride/RideScreen';
import Account from '../src/pages/account/Setting';
import Profile from '../src/pages/account/AccountScreen';
import EditProfile from "../src/pages/account/EditProfileScreen";
import Chatlist from '../src/pages/message/messageScreen';
import ChatDetails from '../src/pages/message/massageDetailsScreen';
import GroupChatDetails from '../src/pages/message/groupMessageDetailsScreen';
import TripLists from '../src/pages/Trips/TripList';
import Rating from '../src/pages/Trips/Rating';
import RatingRider from '../src/pages/Trips/RatingRider';
import Report from '../src/pages/Trips/Report';
import ReportRider from '../src/pages/Trips/ReportRider';
import LandingPage from '../src/pages/landingPage/landingPage';
import Login from "../src/pages/Login/Login";
import Signup from "../src/pages/Signup/SignUp";
import OtpVerification from "../src/pages/otpVerificationPage/otpVerificationScreen";
import ForgotPassword from "../src/pages/forgotPassword/forgotPassword";
import Splash from "../src/pages/Splash/splash";
import Journey from "../src/pages/ride/journey";
import Result from "../src/pages/ride/result";
import Feedback from "../src/pages/Profile/feedback"
import ContactUs from "../src/pages/Profile/contactUs"
import AboutUs from "../src/pages/Profile/aboutUs";
import UserProfile from "../src/pages/userProfile/UserProfile";
import BankAccount from "../src/pages/payment/BankAccount";
import CardDetails from "../src/pages/payment/CardDetails";
import BookingMap from "../src/pages/Map/BookingMap";
import DriverProfile from '../src/pages/Map/DriverProfile';
import GroupInfoDetails from '../src/pages/message/groupInfoDetails';
import ChangePassword from '../src/pages/Login/ChangePassword';


const Tripstack = createStackNavigator(
  {
    Trips: { screen: TripLists },
    Rating: { screen: Rating },
    Report: { screen: Report },
    RatingRider:{screen:RatingRider},
    ReportRider:{screen:ReportRider},
    UserProfile:{screen:UserProfile}
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
      headerMode: 'none',
    },
  }
);

const RideStack = createStackNavigator(
  {
    Ride: { screen: Ride },
    journey: { screen: Journey },
    result: { screen: Result },
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
      headerMode: 'none',
      initialRouteName:'Ride'
    },
  }
);
const DriveStack = createStackNavigator(
  {
    Drive: { screen: Drive },
    LocationAutoCompleteFrom: { screen: LocationAutoCompleteFrom },
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
      headerMode: 'none',
      initialRouteName: 'Drive'
    },
  }
);
const AccountStack = createStackNavigator(
  {
    Account: { screen: Account },
    Profile: { screen: Profile },
    EditProfile: { screen: EditProfile },
    Feedback: { screen: Feedback },
    ContactUs: { screen: ContactUs },
    AboutUs: { screen: AboutUs },
    BankAccount : { screen : BankAccount},
    CardDetails: { screen : CardDetails },
    BookingMap: { screen : BookingMap },
    DriverProfile : {screen : DriverProfile},
    GroupInfoDetails : {screen : GroupInfoDetails}  , 
    ChangePassword: { screen: ChangePassword }
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
      headerMode: 'none',
      initialRouteName: "Account"
    },
    
    
  },
  
);
const MessageStack = createStackNavigator(
  {
    Chatlist: { screen: Chatlist },
    ChatDetails: { screen: ChatDetails },
    GroupChatDetails:{screen: GroupChatDetails}
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
      headerMode: 'none',
    },
  }
);

const App = createBottomTabNavigator(
  {
    Trips: {
      screen: Tripstack,

      tabBarOnPress: ({ navigation }) => {
        navigation.navigate("Trips");
      }
    },
    // Ride: { screen: RideStack },
    Ride: {
      screen: RideStack,

      tabBarOnPress: ({ navigation }) => {
        if (navigation.state.index > 0) {
          // navigation.navigate("Ride");
          navigation.dispatch(navigateActionRide);
        } else {
          navigation.navigate("Ride");
        }

      }

    },
    Drive: {
      screen: DriveStack,
      navigationOptions: ({ navigation }) => ({
        tabBarOnPress: ({ navigation }) => {
          // navigation.navigate("Drive");
          if (navigation.state.index > 0) {
            // navigation.navigate("Ride");
            navigation.dispatch(navigateActionDrive);
          } else {
            navigation.navigate("Drive");
          }
        }
      })
    },
    Message: {
      screen: MessageStack,
      navigationOptions: ({ navigation }) => ({
        title : "Messages",
        tabBarOnPress: ({ navigation }) => {
          if (navigation.state.index > 0) {
            navigation.navigate("Chatlist");
          } else {
            navigation.navigate("Chatlist");
          }
        }
      })
    },
    Account: {
      screen: AccountStack,
      navigationOptions: ({ navigation }) => ({
        tabBarOnPress: ({ navigation }) => {
          if (navigation.state.index > 0) {            
            navigation.dispatch(navigateAction);
          } else {
            navigation.navigate("Account");
          }
        }
        
      })
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Trips') {
          iconName = `ios-timer`;
        } else if (routeName === 'Ride') {
          iconName = `ios-search`;
        } else if (routeName === 'Drive') {
          iconName = `ios-add-circle${focused ? '' : '-outline'}`;

        } else if (routeName === 'Account') {
          iconName = `ios-settings`;

        } else if (routeName === 'Message') {
          iconName = `ios-chatbubbles`;

        }
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#faa400',
      inactiveTintColor: '#007fe1',
      labelStyle: {
        fontSize: 15
      },
      tabStyle: {
        paddingTop: 5,

      },
    },
  }
);

const navigateAction = NavigationActions.navigate({
  routeName: 'Account',
  action: NavigationActions.navigate({ routeName: 'Account' }),
});

const navigateActionRide = NavigationActions.navigate({
  routeName: 'Ride',
  action: NavigationActions.navigate({ routeName: 'Ride' }),
});

const navigateActionDrive = NavigationActions.navigate({
  routeName: 'Drive',
  action: NavigationActions.navigate({ routeName: 'Drive' }),
});

const Loading = createStackNavigator({
  Splash: { screen: Splash }
},
  {
    headerMode: "none"
  })


const loginStack = createStackNavigator({
  LandingPage: { screen: LandingPage },
  Login: { screen: Login },
  Signup: { screen: Signup },
  OtpVerification: { screen: OtpVerification },
  ForgotPassword: { screen: ForgotPassword },  
},
  {
    //    initialRouteName: 'TripLists',
    //    headerShown: false,
    headerMode: 'none'
  }
);


export default createAppContainer(createSwitchNavigator({
  Loading,
  authStack: loginStack,
  mainStack: App,
},
 {
  initialRouteName: "Loading"
}));